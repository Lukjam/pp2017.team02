package pp2017.team02.shared;

/**
 * Erstellt das Monster Drache und legt die Monstertypischen Eigenschaften fest.
 *
 * @autor Kriehn, Lorenz, 7311063
 */
public class Drache extends Monster {
    /**
     * Konstruktor der Klasse.
     * Berechnet die Werte des Monster anhand der Tiefe des Levels.
     * @autor Kriehn, Lorenz, 7311063
     */
    public Drache(int xPos, int yPos,int[][] karte,int tiefe, boolean hatSchluessel){
        super(xPos,yPos,karte, tiefe, hatSchluessel);

        this.setAttReichweite(1);
        this.setBemerkenReichweite(10);
        this.setCdAngriff(500);
        this.setCdZug(500);
        this.setFliehWert(0.3f);
        this.setMaxLeben((int)(100 * Math.pow(1.5,tiefe-1)));
        this.setLeben(getMaxLeben());
        this.setMinTiefe(0);
        this.setMaxTiefe(Integer.MAX_VALUE);
        this.setVerteidigungsWert(1*tiefe);// TODO BALANCING
        this.setAngriffsWert(10 + tiefe * 2);
        this.setZustandAutomat(1);





    }


}
