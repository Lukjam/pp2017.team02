package pp2017.team02.shared;

/**
 * Erstellt das Monster Statue und legt die Monstertypischen Eigenschaften fest.
 * @autor Kriehn, Lorenz, 7311063
 */
public class Statue extends Monster{

    /**
     * Konstruktor der Klasse
     * Berechnet die Werte des Monster anhand der Tiefe des Levels.
     * @autor Kriehn, Lorenz, 7311063
     */
    public Statue(int xPos, int yPos,int[][] karte,int tiefe, boolean hatSchluessel){
        super(xPos,yPos,karte, tiefe, hatSchluessel);

        this.setAttReichweite(1);
        this.setBemerkenReichweite(5);
        this.setCdAngriff(500);
        this.setCdZug(500);
        this.setFliehWert(0f);
        this.setMaxLeben((int)(150 * Math.pow(1.5,tiefe-1)));
        this.setLeben(getMaxLeben());
        this.setMinTiefe(0); // Erscheint erst ab einer Tiefe von 2.
        this.setMaxTiefe(Integer.MAX_VALUE);
        this.setVerteidigungsWert(5*tiefe);// TODO BALANCING
        this.setAngriffsWert(2 + tiefe * 2);
        this.setZustandAutomat(4);




    }
}
