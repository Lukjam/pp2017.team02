package pp2017.team02.shared;

/**
 * @author <Mack, Jonas, 5975514>
 */

public class Boden extends SpielfeldElement {

    public Boden() {
        super.setTyp(BODEN);
        super.setBegehbar(true);
    }

}

