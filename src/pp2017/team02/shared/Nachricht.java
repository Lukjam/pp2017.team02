package pp2017.team02.shared;

import java.io.Serializable;

/**
 * Basisklasse Nachricht, von der alle anderen Nachrichten-Klassen erben.
 *
 * @author Tiebing, Alexander, 6018254
 * @author Jamann, Lukas, 7309791
 */
public class Nachricht implements Serializable{

    private static final long serialVersionUID = 1L;

    private int clientID;
    private int clientIDComm = -1;

    /**
     * Standard Konsturuktor für den client
     * @author Tiebing, Alexander, 6018254
     */
    public Nachricht(int clientID){
        this.clientID = clientID;
    }

    /**
     *Konstruktor für alle Nachrichten erschaffen.
     *
     *@author Jamann, Lukas, 7309791
     */
    public Nachricht(){}

    /**
     * Für SWV um der Comm den Client bescheid zu sagen
     * @author Jamann, Lukas, 7309791
     */
    public Nachricht(int clientID, int clientIDComm) {
        this.clientID = clientID;
        this.clientIDComm = clientIDComm;
    }

    /**
     * Nur clientIDComm Konstruktor, welcher selten benutzt wird. Nur Comm -> SWV
     * @author Jamann, Lukas, 7309791
     */
    public Nachricht(int clientIDComm, boolean ueberladen) {
        this.clientIDComm = clientIDComm;
    }


    /**
     * GETTER / SETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public int getClientID(){
        return this.clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    /**
     * GETTER / SETTER
     *
     * @author Jamann, Lukas, 7309791
     */
    public int getClientIDComm() {
        return clientIDComm;
    }

    public void setClientIDComm(int clientIDComm) {
        this.clientIDComm = clientIDComm;
    }
}
