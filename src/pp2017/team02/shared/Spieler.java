package pp2017.team02.shared;

import java.util.LinkedList;
import java.util.Random;

/**Verwaltet die Informationen über die Spielfigur.
 * @author Kriehn, Lorenz, 7311063
 * @author Tiebing, Alexander, 6018254
 */
public class Spieler extends Figur implements Cloneable{
    private String name;
    private LinkedList<Item> inventar = new LinkedList<Item>();
    private int clientID;
    private boolean hatSchluessel;
    private int mana;
    private Waffe waffe;
    private int staerke = 0;
    private int geschicklichkeit = 0;
    private int intelligenz = 0;
    private int stufe;
    private int erfahrung;
    private int skillPunkte = 0;
    private Ruestung ruestung;
    private Random rnd = new Random();
    private long letzterAngriff;
    private long letzterZug;
    private int cdAngriff;
    private int cdZug;
    private Level aktuellesLevel = new Level(1); //Todo: Ist das richtig?
    private int aktuelleKarte;    // Wert zwischen 0 und 3, der den aktuellen Raum anngibt, in dem der Spieler ist
    private int anzahlTraenke;
    private Spieler field;

    /**
     * Implementiert das Interface Cloneable.
     * @author Kriehn, Lorenz, 7311063
     */
    @Override
    public Object clone() throws  CloneNotSupportedException{
        return super.clone();
    }


    private Trank heilTrank;

    /**
     * Konstruktor fuer andere Spieler, die ganzen Attribute der anderen Spieler sind unwichtig, nur die Position
     * interessiert
     *
     * @author Tiebing, Alexander, 6018254
     */
    public Spieler(int clientID, String name, int xPos, int yPos){
        setClientID(clientID);
        setName(name);
        setxPos(xPos);
        setyPos(yPos);
    }

    /**
     * Konstruktor der Klasse, wird aufgerufen, wenn ein neuer Spieler einen Charakter erstellt.
     *
     * Level, Karte und Position werden in lokalisiereEingang() festgesetzt
     *
     * @author Kriehn, Lorenz, 7311063
     */
    public Spieler(String name, int clientID, int addStaerke, int addGeschicklichkeit, int addIntelligenz, Level level) {

        this.clientID = clientID;
        setName(name);

        rnd = new Random();
        setGeschicklichkeit(5 + addGeschicklichkeit);
        setStufe(1);
        setStaerke(5 + addStaerke);
        setIntelligenz(5 + addIntelligenz);
        setLeben(3 * getStaerke() + 2 * getStufe());
        setMana(2 * getIntelligenz() + getGeschicklichkeit());
        setAngriffsWert((int) ((getStaerke()) * (0.8 + rnd.nextInt(4))));
        setVerteidigungsWert(getGeschicklichkeit());

        setHatSchluessel(false);
        setErfahrung(0);
        setCdAngriff(500);
        setCdZug(500);
        setLetzterZug(System.currentTimeMillis());
        setLetzterAngriff(System.currentTimeMillis());
        setAktuellesLevel(level);
        lokalisiereEingang(getAktuellesLevel()); //TODO Level, Karte, Position stecken hier drin!
        setInventar(getInventar());
        this.ruestung = new Ruestung(1,1,1);
        this.waffe = new Waffe(1,1,1);
        fuegeItemHinzu(new Trank(1,1));
        fuegeItemHinzu(new Trank(1,1));


    }

    /** Ueberladen des Konstruktors um alte Spieler zu laden.
     * @author Kriehn, Lorenz, 7311063
     */ //TODO Geht das so??
    public Spieler( String name, int clientID, int staerke, int geschicklichkeit, int intelligenz, int stufe,
                   int erfahrung, int skillPunkte, Level aktuellesLevel, int aktuelleKarte,
                    int anzahlTraenke){

        this.name = name;
        this.clientID = clientID;
        this.staerke = staerke;
        this.geschicklichkeit = geschicklichkeit;
        this.intelligenz = intelligenz;
        this.stufe = stufe;
        this.erfahrung = erfahrung;
        this.skillPunkte = skillPunkte;
        this.aktuellesLevel = aktuellesLevel;
        this.aktuelleKarte = aktuelleKarte;
        lokalisiereEingang(getAktuellesLevel());
        this.anzahlTraenke = anzahlTraenke;

        for(int i = 1;i <= anzahlTraenke;i++){
            fuegeItemHinzu(getHeilTrank());
        }

        setHatSchluessel(false);
        setMana(2 * getIntelligenz() + getGeschicklichkeit());
        setLeben(3 * getStaerke() + 2 * getStufe());
        setCdAngriff(500);
        setCdZug(500);
        setLetzterZug(System.currentTimeMillis());
        setLetzterAngriff(System.currentTimeMillis());

    }


    /** Einfacher Weg um Erfahrungspunkte hinzuzufuegen.
     * @author Kriehn, Lorenz, 7311063
     */
    public void plusErfahrung(int erfahrung){
        setErfahrung(getErfahrung()+erfahrung);
    }

    /** Der Charakter stiegt eine Stufe auf, falls genuegend Erfahung vorhanden ist.
     *  Nach der Formel aus dem Multi User Dungeon Guide
     *  @author Kriehn, Lorenz, 7311063
     */
    private void stufenAufstieg(){
        int tempErfahrung;
        if(getErfahrung() >= 100 * Math.pow(1.2,getStufe())){
            setSkillPunkte(getSkillPunkte() + 5);
            tempErfahrung =(int)( 100 * Math.pow(1.2,getStufe() - getErfahrung()));
            setStufe(getStufe() + 1);
            setErfahrung(tempErfahrung);
            // Falls genug Erfahrung vorhanden ist, um mehr als eine Stufe aufzusteigen. Bei Cheats o.ae.
            stufenAufstieg();
        }
    }

    /** Die Methoden sollen fuer die Verteilung der Staerke Skillpunkte verwendet werden.
     * @author Kriehn, Lorenz, 7311063
     */
    public void addStaerke(){
        if (skillPunkte > 0){
            setStaerke(getStaerke() +1);
            skillPunkte--;
        }
    }
    /** Die Methoden sollen fuer die Verteilung der Intelligenz Skillpunkte verwendet werden.
     * @author Kriehn, Lorenz, 7311063
     */
    public void addIntelligenz(){
        if (skillPunkte > 0){
            setIntelligenz(getIntelligenz() +1);
            skillPunkte--;
        }
    }
    /** Die Methoden sollen fuer die Verteilung der Geschicklichkeits Skillpunkte verwendet werden.
     * @author Kriehn, Lorenz, 7311063
     */
    public void addGeschicklichkeit(){
        if (skillPunkte > 0){
            setGeschicklichkeit(getGeschicklichkeit() +1);
            skillPunkte--;
        }
    }

    /**
     * Prueft, ob der Spieler tot ist und wenn ja, entzieht im EP und setzt ihn an den Anfang des Levels
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void istTot(){
        if(getLeben() <= 0){
            setErfahrung((int) Math.floor(0.75*getErfahrung()));
            lokalisiereEingang(getAktuellesLevel());
            setLeben(3 * getStaerke() + 2 * getStufe());

        }
    }

    /**
     * Fuegt ein Item hinzu und legt Waffen und Ruestung an, falls diese bessere Werte haben als die alten.
     * TODO Sende nachricht falls eine neue Waffe/Ruestung angelegt wird.
     * @author Kriehn, Lorenz, 7311063
     */
    public void fuegeItemHinzu(Item item){
        inventar.add(item);
        if (item.getClass() == Trank.class){
            anzahlTraenke++;
        } else if (item.getClass() == Ruestung.class){
            Ruestung tempR;
            tempR = (Ruestung) item;

            if (tempR.getVerteidigungsWert() > ruestung.getVerteidigungsWert()){
                ruestung =(Ruestung) item;

            }
        } else if (item.getClass() == Waffe.class){
            Waffe tempW;
            tempW = (Waffe) item;
            if (tempW.getAngriffsWert() > waffe.getAngriffsWert()){
                waffe = (Waffe) tempW;
            }
        }
    }

    /**
     * Wenn ein Trank im Inventar ist, wird einer verwendet und gelöscht.
     * @author Kriehn, Lorenz, 7311063
     */
    public void verwendeTrank(){
        for (int i=0; i<inventar.size();i++) {
            if (inventar.get(i).getClass() == Trank.class){
                setLeben((int) Math.min(3*getStaerke()+2*getStufe(),getLeben()+0.25*(3*getStaerke()+2*getStufe())));
                inventar.remove(i);
                break;
            }
        }
    }

    /**
     * Legt ein Item an die aktuelle Position.
     *
     * @author Kriehn, Lorenz, 7311063
     */
    public Item droppeItem(Item item){
        for (int i=0;i<inventar.size();i++){
            if (inventar.get(i).getClass() == item.getClass()){
                inventar.get(i).setXpos(getxPos());
                inventar.get(i).setYpos(getyPos());
                if (item.getClass() == Trank.class)
                    anzahlTraenke--;
                return inventar.remove(i);
            }
        }
        return null;
    }

    /**
     * Geht alle 4 Karten eines Levels durch und sucht nach dem Eingang. Aktualisiert as Level des Spielers, sowie seine
     * aktuelle Karte und aktuelle Position
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void lokalisiereEingang(Level level){
        setAktuellesLevel(level);

        for(int n = 0;n < 4;n++){
            for(int i = 0;i < level.getKarten().get(n).getKarte().length;i++){
                for(int j = 0;j < level.getKarten().get(n).getKarte().length;j++){
                    if(level.getKarten().get(n).getEintrag(i,j).getTyp() == SpielfeldElement.EINGANG){

                        setAktuelleKarte(n);
                        setxPos(getAktuellesLevel().getEingang().getXpos());
                        setyPos(getAktuellesLevel().getEingang().getYpos());
                    }
                }
            }
        }
    }

    /**
     * Addiert den Wert lebensAenderung zum aktuellen Leben dazu
     *
     * @author Tiebing, Alexander, 6018254
     */
    @Override
    public void changeLeben(int lebensAenderung){
        System.out.println(getLeben()+lebensAenderung);
        if (lebensAenderung <= 0) {

            if (getVerteidigungsWert() < (-lebensAenderung)) {
                setLeben(getLeben() + getVerteidigungsWert() + lebensAenderung);
                istTot();
            }
        }
        if(lebensAenderung > 0){
            setLeben(Math.min(3 * getStaerke() + 2 * getStufe(),getLeben()+lebensAenderung));
        }
    }

    /**
     * Wenn der Spieler  einen Schluessel hat und diesen auf einer geschlossenen Tuer nutzt, dann oeffnet sich diese
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void oeffneTuer(){
        if(isHatSchluessel()){
            if(getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer1().isEnthaeltSpieler() &&
                    !getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer1().isOffen()){

                setHatSchluessel(false);
                getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer1().setOffen();

            }
            if(getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer2().isEnthaeltSpieler() &&
                    !getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer2().isOffen()) {

                setHatSchluessel(false);
                getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer2().setOffen();

            }
        }
    }

    /**
     * Wenn der Spieler auf Tuer 1 steht und diese offen ist, gelangt er in den EINEN Nachbarraum, und wenn er auf
     * Tuer 2 steht und diese offen ist, gelangt er in den ANDEREN Nachbarraum.
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void raumWechsel(){
        if(getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer1().isEnthaeltSpieler() &&
                getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer1().isOffen()){

            getAktuellesLevel().getKarten().get(getAktuelleKarte()).deleteClientID(getClientID());

            setAktuelleKarte(getAktuellesLevel().getKarten().indexOf(
                    getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer1().getNachbarraum()));

            getAktuellesLevel().getKarten().get(getAktuelleKarte()).addClientID(getClientID());

            setxPos(getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer1().getxPos());
            setxPos(getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer1().getyPos());

        }
        if(getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer2().isEnthaeltSpieler() &&
                getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer2().isOffen()){

            getAktuellesLevel().getKarten().get(getAktuelleKarte()).deleteClientID(getClientID());

            setAktuelleKarte(getAktuellesLevel().getKarten().indexOf(
                    getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer2().getNachbarraum()));

            getAktuellesLevel().getKarten().get(getAktuelleKarte()).addClientID(getClientID());

            setxPos(getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer2().getxPos());
            setxPos(getAktuellesLevel().getKarten().get(getAktuelleKarte()).getTuer2().getyPos());

        }
    }

    /**
     * falls
     */
    public void setKarteKorrektur(int kartenNr, int[] xy){
        setAktuelleKarte(kartenNr);
        getAktuellesLevel().getKarten().get(getAktuelleKarte()).addClientID(getClientID());
        setPosition(xy);
    }

    //Getter/Setter

    //<editor-fold desc="Getter/Setter">

    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public String getName() {
        return name;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setName(String name) {
        this.name = name;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public boolean isHatSchluessel() {
        return hatSchluessel;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setHatSchluessel(boolean hatSchluessel) {
        this.hatSchluessel = hatSchluessel;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public int getMana() {
        return mana;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setMana(int mana) {
        this.mana = mana;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public Item getWaffe() {
        return waffe;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setWaffe(Waffe waffe) {
        this.waffe = waffe;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public int getStaerke() {
        return staerke;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setStaerke(int staerke) {
        this.staerke = staerke;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public int getGeschicklichkeit() {
        return geschicklichkeit;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setGeschicklichkeit(int geschicklichkeit) {
        this.geschicklichkeit = geschicklichkeit;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public int getIntelligenz() {
        return intelligenz;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setIntelligenz(int intelligenz) {
        this.intelligenz = intelligenz;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public int getStufe() {
        return stufe;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setStufe(int stufe) {
        this.stufe = stufe;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public int getErfahrung() {
        return erfahrung;
    }
    /** Setter von erfahrung. Fragt gleichzeitig ab ob genuegend Erfahrung vohanden ist um eine Stufe aufzusteigen.
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setErfahrung(int erfahrung) {
        this.erfahrung = erfahrung;
        stufenAufstieg();
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public Item getRuestung() {
        return ruestung;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setRuestung(Ruestung ruestung) {
        this.ruestung = ruestung;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setClientID(int clientID) {
        this.clientID = clientID;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public int getClientID() {
        return clientID;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public int getSkillPunkte() {
        return skillPunkte;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setSkillPunkte(int skillPunkte) {
        this.skillPunkte = skillPunkte;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public LinkedList<Item> getInventar() {
        return inventar;
    }
    /** Getter und Setter
     * @autor Kriehn, Lorenz, 7311063
     */
    public void setInventar(LinkedList<Item> inventar) {
        this.inventar = inventar;
    }

    /**
     * Berechnet den Verteidigungswert des Spielers.
     * Betrachtet auch die Ruestung.
     * @author Kriehn, Lorenz, 7311063
     */
    @Override
    public int getVerteidigungsWert(){
        int i = 0;
        if (ruestung != null){
        i = ruestung.getVerteidigungsWert();
    }
        return getGeschicklichkeit() + i;
    }
    /**
     * Berechnet den Angriffswertswert des Spielers.
     * Betrachtet auch die Ruestung.
     * @author Kriehn, Lorenz, 7311063
     */
    @Override
    public int getAngriffsWert(){
    int i = 0;
        if (waffe != null){
        i = waffe.getAngriffsWert();
    }
        return 100;  //(int) ((i + getStaerke()) * (0.8 + rnd.nextInt(4)));
}


    /**
     * @author Tiebing, Alexander, 6018254
     */
    public Level getAktuellesLevel() {
        return aktuellesLevel;
    }

    public void setAktuellesLevel(Level aktuellesLevel) {
        this.aktuellesLevel = aktuellesLevel;
    }

    public int getAktuelleKarte() {
        return aktuelleKarte;
    }

    // Todo: Muss dann noch im Level verankert werde! also aus der Karte im Level die ClientID Löschen.

    public void setAktuelleKarte(int aktuelleKarte) {
        this.aktuelleKarte = aktuelleKarte;
    }

    public int getAnzahlTraenke() {
        return anzahlTraenke;
    }

    public void setAnzahlTraenke(int anzahlTraenke) {
        this.anzahlTraenke = anzahlTraenke;
    }

    public Trank getHeilTrank() {
        return heilTrank;
    }

    public void setHeilTrank(Trank heilTrank) {
        this.heilTrank = heilTrank;
    }





}


