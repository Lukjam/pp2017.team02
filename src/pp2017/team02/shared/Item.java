package pp2017.team02.shared;

import java.io.Serializable;
import java.util.Random;

/**
 * Datentyp Item: Vaterklasse von Trank, Schluessel,...
 *
 * @author Jonas Mack, 5975514
 *
 */

public class Item implements Serializable{
    private static final long serialVersionUID = 1L;

    private int xPos;
    private int yPos;
    private boolean aufnommen;
    private int itemID;
    Random r=new Random();

    public Item() {
        this.itemID = r.nextInt(900000)+100000;
    }

    public int getXpos() {
        return xPos;
    }

    public void setXpos(int xPos) {
        this.xPos = xPos;
    }

    public int getYpos() {
        return yPos;
    }

    public void setYpos(int yPos) {
        this.yPos = yPos;
    }

    public boolean isAufnommen() {
        return aufnommen;
    }

    public void setAufnommen(boolean aufnommen) {
        this.aufnommen = aufnommen;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }


}
