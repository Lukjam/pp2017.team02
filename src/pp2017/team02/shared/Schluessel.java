package pp2017.team02.shared;

/**
 *
 * @author <Mack, Jonas, 5975514>
 *
 */

public class Schluessel extends Item {

    private boolean sichtbar;

    public Schluessel(int x, int y) {
        super.setXpos(x);
        super.setYpos(y);
        this.sichtbar = false;
    }

    public Schluessel() {
    }

    public boolean isSichtbar() {
        return sichtbar;
    }

    public void setSichtbar(boolean offen) {
        this.sichtbar = offen;
    }










}
