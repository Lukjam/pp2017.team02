package pp2017.team02.shared;

import java.util.LinkedList;

/**
 * Klasse, die der Server nutzt, wenn er dem Client die aktuelle Highscoreliste als LinkedList durchgibt.
 * Jeder Eintrag der Liste ist ein String-Array mit den Eintraeen: Rang, Name, Levelnummer (des Levels, das erreicht wurde)
 *
 * @author Tiebing, Alexander, 6018254
 */
public class HighscoreNachricht extends Nachricht {

    private LinkedList<String[]> highscore;

    /**
     * Server schickt dem Client die Highscoreliste
     *
     * @author Tiebing, Alexander, 6018254
     */
    public HighscoreNachricht(int clientID, LinkedList<String[]> highscore) {
        super(clientID);
        this.highscore = highscore;
    }

    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public LinkedList<String[]> getHighscore() {
        return highscore;
    }
}

