package pp2017.team02.shared;

/**
 * Klasse des Monster Gargoyle.
 * Bewegt sich schneller als andere Monster.
 * @author Kriehn, Lorenz, 7311063
 */
public class Gargoyle extends Monster{
    public Gargoyle(int xPos, int yPos,int[][] karte,int tiefe, boolean hatSchluessel){
        super(xPos,yPos,karte, tiefe, hatSchluessel);

        this.setAttReichweite(1);
        this.setBemerkenReichweite(5);
        this.setCdAngriff(500);
        this.setCdZug(300);
        this.setFliehWert(0.4f);
        this.setMaxLeben((int)(100 * Math.pow(1.3,tiefe-1)));
        this.setLeben(getMaxLeben());
        this.setMinTiefe(0);
        this.setMaxTiefe(3);
        this.setVerteidigungsWert(1*tiefe);// TODO BALANCING
        this.setAngriffsWert( 3 + (int) (tiefe * 1.5));
        this.setZustandAutomat(1);


    }
}
