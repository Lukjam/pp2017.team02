package pp2017.team02.shared;

import java.util.LinkedList;

/**
 * Klasse zum Uebertragen von Daten bezueglich diverser Spielerattribute. Wird benutzt wenn Raum gewechselt wird,
 * Skillpunkte verteilt werden oder ein Heiltrank getrunken wird
 *
 * @author Tiebing, Alexander, 6018254
 */

public class SpielerNachricht extends Nachricht {

    private int xPos;
    private int yPos;
    private int aktuelleKarte = -1;
    private int alteKarte;
    private int staerke;
    private int intelligenz;
    private int geschicklichkeit;
    private int leben = -1;
    private LinkedList<Item> inventar;


    /**
     * Wenn der Spieler den Raum wechselt, wird dem Server die Nummer der Karte mitgeteilt, auf der sich der Spieler
     * nun beindet, sowie die alte Kartennummer und die aktuelle Position
     *
     * @author Tiebing, Alexander, 6018254
     */
    public SpielerNachricht(int clientID, int xPos, int yPos, int alteKarte, int aktuelleKarte){
        super(clientID);
        this.xPos = xPos;
        this.yPos = yPos;
        this.alteKarte = alteKarte;
        this.aktuelleKarte = aktuelleKarte;

    }

    /**
     * Wenn der Spieler seine Skillpunkte auf Staerke, Intelligenz oder Geschicklichkeit verteilt, wird dies hiermit
     * dem Server mitgeteilt. Alle Skillpunkte muessen auf einmal verteilt werden.
     *
     * @author Tiebing, Alexander, 6018254
     */
    public SpielerNachricht(int clientID, int staerke, int intelligenz, int geschicklichkeit){
        super(clientID);
        this.staerke = staerke;
        this.intelligenz = intelligenz;
        this.geschicklichkeit = geschicklichkeit;
    }


    /**
     * Wenn der Spieler einen Heiltrank getrunken hat, wird dies hiermit dem Server weitergegeben
     *
     * @author Tiebing, Alexander, 6018254
     */
    public SpielerNachricht(int clientID, LinkedList<Item> inventar, int leben) {
        super(clientID);
        this.inventar = inventar;
        this.leben = leben;
    }


    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public int getxPos() {
        return xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public int getAktuelleKarte() {
        return aktuelleKarte;
    }

    public int getStaerke() {
        return staerke;
    }

    public int getIntelligenz() {
        return intelligenz;
    }

    public int getGeschicklichkeit() {
        return geschicklichkeit;
    }

    public int getLeben() {
        return leben;
    }

    public LinkedList<Item> getInventar() {
        return inventar;
    }

    public int getAlteKarte() {
        return alteKarte;
    }
}