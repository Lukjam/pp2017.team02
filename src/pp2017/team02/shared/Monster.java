package pp2017.team02.shared;

import pp2017.team02.server.engine.KI;
import pp2017.team02.server.engine.WegPunkte;

import java.util.LinkedList;
import java.util.Random;

/**
 * Die Monsterklasse ist die Oberklasse der einzelnen Monster und enthaelt den endlichen Automaten, alle wichtigen
 * Attribute und Methoden.
 * @author Kriehn, Lorenz, 7311063
 */
public class Monster extends Figur {

    private Item beute;
    private int goldBeute;
    private int xpBeute;
    private int minTiefe;
    private int maxTiefe;
    private int zustandAutomat;
    private int attReichweite;
    private int tiefe;
    private LinkedList<WegPunkte> weg = new LinkedList<WegPunkte>();
    private KI kI;
    private LinkedList<Spieler> spieler;
    private int bemerkenReichweite;
    private float fliehWert; // Gibt in Dezimal an, ab wieviel % Leben das Monster flieht.
    private Spieler wurdeAngegriffenVon;
    private int monsterID;
    private boolean enthaeltSchluessel;
    private boolean sichtbar;
    private boolean geflohen = false; //Falls ein Monster einmal geflohen ist, kaempft es immer weiter.

    private Random rnd = new Random();

    /**
     * Nur für Testumgebung von Levelgenerator.
     * @author: Jonas Mack, 5975514
     */
    public Monster(int x, int y, boolean s1, boolean s2) {
        this.setxPos(x);
        this.setyPos(y);
        this.sichtbar = s1;
        this.enthaeltSchluessel = s2;
    }


    /**
     * erzeugt zufaellige 6-stellige Zufallszahl
     *
     * @author Kriehn, Lorenz, 7311063
     */
    public int erstelleMonsterID(){
        Random rand = new Random();

        int zahl = (int)(rand.nextDouble()*1000000L);
        monsterID = zahl;
        return monsterID;
    }


    /** Konstruktor der eine Monsterklasse erstellt.
     * @author Kriehn, Lorenz, 7311063
     *
     */
    public Monster(int yPos, int xPos, int[][] karte, int tiefe, boolean hatSchluessel){
        this.setxPos(xPos);
        this.setyPos(yPos);
        this.setTiefe(tiefe);
        berechneGoldBeute();
        berechneXPBeute();
        setMonsterID(erstelleMonsterID());
        this.setEnthaeltSchluessel(hatSchluessel);
        this.setLetzterZug(System.currentTimeMillis());
        this.setLetzterAngriff(System.currentTimeMillis());
        // Setzt die Beute des Monsters zufaellig. 1/2 Trank, 1/4 Ruestung, 1/4 Waffe
        int temp = rnd.nextInt(3);
        switch(temp){
            case 0 :
                this.setBeute(new Trank(1,1));
                break;
            case 1 :
                this.setBeute(new Trank(1,1));
                break;
            case 2 :
                // Setzt die Ruestung auf einen zufaelligen Wert zwischen 1 und der aktuellen Tiefe +1.
                this.setBeute(new Ruestung(1,1, rnd.nextInt(getTiefe()+1)));
                break;
            case 3 :
                this.setBeute(new Waffe(1,1, rnd.nextInt(getTiefe()+1)));
                break;
        }
        this.erstelleMonsterID();
        kI = new KI(karte);

        // TODO Konstruktor aufbauen. Fertig?



    }


    /** Bewegt das Monster und gibt die neue Position und verschiedene Informationen über das Monster an die
     *  Spielweltverwaltung zurück. Stellt den Automaten dar.
     *  Dabei gilt: [] gibt den Index an.
     *   [0] neue xPosition des Monsters
     *   [1] neue YPosition des Monsters
     *   [2] = 0, Monster lebt, = 1 Monster ist gestorben, getBeute(); getXP(); und an Spieler[6] senden.
     *   [3] hat erfolgreich angegriffen falls = 1, sonst 0
     *   [4] Schaden den das Monster gemacht hat. oder getAngriffsWert()
     *   [5] ClientID welcher Spieler angegriffen wurde.
     *   [6] ClientID des Spielers der XP bekommt.
     *
     *
     *   Die Zustaende des Automaten:
     *  '0' Passiv / Nicht aktiv / Nicht Sichtbar. Wird ueber setSichtbar gesetzt.
     *  '1' Patrouillieren
     *  '2' Angreifen
     *  '3' Fliehen
     *  '4' Warten: Wartet bis der Spieler in Angriffsreichweite ist.
     * @author Kriehn, Lorenz, 7311063
     */
    public int[] updateMonster(LinkedList<Spieler> inputSpieler){
        int[] tempXY = new int[8]; // TODO Noch 7 sonst 8.
        tempXY[0] = getxPos();
        tempXY[1] = getyPos();
        tempXY[7] = -1;
        tempXY[2] = 0;
        tempXY[3] = 0;
        tempXY[4] = 0;
        // Alle Spieler die im gleichen Raum sind.
        this.spieler = inputSpieler;

        // Monster tot?
        if (getLeben() <= 0){
            tempXY[2] = 1;
//            tempXY[6] = wurdeAngegriffenVon.getClientID();
        }


        for(int i = 0; i<spieler.size();i++){

            //Zustand: 3. Prüft ob das Monster flieht. Es hört auf zu fliehen, falls es geheilt wurde oder es weit genug weggelaufen ist.
            if ((Math.round(getLeben() / getMaxLeben()) <= getFliehWert() && zustandAutomat != 0 ) && !isGeflohen()){
                if ((zustandAutomat == 3 && wegIstLeer()) || isGeflohen()){
                    setZustandAutomat(1); // TODO Monstertesten auf 4 setzen.
                    setGeflohen(true);
                }else{
                    flieh();
                    setZustandAutomat(3);
                }
            }

            // Zustand: 2. Setzt den Automat in Angriffszustand, falls das Monster nicht im Passiv oder Fliehen zustand ist.
            if (spielerWirdGesehen(spieler.get(i).getxPos(),spieler.get(i).getyPos()) && zustandAutomat != 0 && zustandAutomat != 3){
                setZustandAutomat(2);
                weg = kI.bewegeVonZu(getxPos(),getyPos(),spieler.get(i).getxPos(),spieler.get(i).getyPos());
            }
            //Zustand: 1. Monster patrouiliiert
            if (zustandAutomat == 1 || !spielerWirdGesehen(spieler.get(i).getxPos(),spieler.get(i).getyPos()) && zustandAutomat != 0 && zustandAutomat != 3 && zustandAutomat != 4){

                if (wegIstLeer()){
                    Random rnd = new Random();
                    LinkedList<WegPunkte> tempWeg;
                    tempWeg = kI.alleNachbarn(kI.getKarte()[getxPos()][getyPos()]);
                    weg = new LinkedList<WegPunkte>();
                    weg.add(tempWeg.get(rnd.nextInt(tempWeg.size()))); // Setzt einen zufaelligen nächsten Wegpunkt der begehbar ist.

                }
            }

            //Monster greift den Spieler an falls dieser in Angriffsreichweite ist und das Monster im Angriffsmodus ist.
            if(attackiertSpieler(spieler.get(i).getxPos(),spieler.get(i).getyPos()) && zustandAutomat == 2){
                //spieler.get(i).setLeben(spieler.get(i).getLeben() - getAngriffsWert()); Führt eine Attacke aus
                setLetzterAngriff(System.currentTimeMillis());
                tempXY[3] = 1;
                tempXY[4] = getAngriffsWert();
                tempXY[5] = spieler.get(i).getClientID();

            }
        }// Bis hierhin fuer jeden Spieler.

        //System.out.println("Test");
        // Läuft den vorher festgelegten Weg ab, falls das Monster sich begewegen kann.
        // Mit temporaerem workaround, dass Monster nicht auf Spieler tritt. Wird spaeter in der Spielweltverwaltung erledigt.
        if (System.currentTimeMillis() - getLetzterZug() >= getCdZug() && !wegIstLeer() && zustandAutomat!= 0 && zustandAutomat!=4) {
            boolean stehaufspieler = false;
            for(int i = 0; i<spieler.size();i++) {
                if ((weg.peekLast().getxPos() == spieler.get(i).getxPos() && weg.peekLast().getyPos() == spieler.get(i).getyPos())) {
                    stehaufspieler = true;
                }
            }
            if (!stehaufspieler) {
                tempXY[0] = weg.peekLast().getxPos();
                tempXY[1] = weg.pollLast().getyPos();
                setLetzterZug(System.currentTimeMillis());
                setxPos(tempXY[0]);
                setyPos(tempXY[1]);
            }


        }
        tempXY [7] = getZustandAutomat();
        return tempXY;
    }


    /**
     * Schaut ob der Weg leer ist.
     * @author Kriehn, Lorenz, 7311063
     */
    public boolean wegIstLeer(){
        if (weg == null)
            return true;
        else if (weg.isEmpty()){
            return true;
        }
        return false;

    }
    /** Laesst das Monster an eine zufaellige begehbare Position fliehen.
     * @author Kriehn, Lorenz, 7311063
     */
    public void flieh(){
        int[] tempPos = new int[2];
        Random rnd = new Random();
        // Damit der erste Fluchpunkt nicht immer auf 0,0 zeigt.
        tempPos[0] = rnd.nextInt(kI.getKarte().length);
        tempPos[1] = rnd.nextInt(kI.getKarte()[0].length);
        int tempZaehler = 0;
        while(!kI.istBegehbar(kI.getKarte()[tempPos[0]][tempPos[1]].getObjekt()) || tempZaehler == 1000){
            tempPos[0] = rnd.nextInt(kI.getKarte().length);
            tempPos[1] = rnd.nextInt(kI.getKarte()[0].length);
            tempZaehler++;
        }
        if (wegIstLeer() || zustandAutomat != 3 ) weg = kI.bewegeVonZu(getxPos(),getyPos(),tempPos[0],tempPos[1]);




    }


    /**
     * Verarbeitet den Angriff durch einen Spieler.
     * @param spieler der angreift.
     * @author Kriehn, Lorenz, 7311063
     */
    public int angegriffenWerden(Spieler spieler){
        changeLeben(spieler.getAngriffsWert() - getVerteidigungsWert());
        wurdeAngegriffenVon = spieler;
        return spieler.getAngriffsWert() - getVerteidigungsWert();

    }



    /** Gibt aus ob das Monster angreift.
     *  @author Kriehn, Lorenz, 7311063
     */

    public boolean attackiertSpieler(int spielerX, int spielerY){
        return spielerIstInAttReichweite(spielerX, spielerY) && (System.currentTimeMillis() - getLetzterAngriff() >= getCdAngriff());
    }

    /** Boolean ob Spieler in (Angriffs-)Reichweite ist.
     *  @author Kriehn, Lorenz, 7311063
     */
    public boolean spielerIstInAttReichweite(int spielerX, int spielerY){

        return getAttReichweite() >= (Math.sqrt(Math.pow(getxPos() - spielerX, 2) + Math.pow(getyPos() - spielerY, 2)));

    }

    /** Gibt aus ob das Monster den Spieler bemerkt, also ob das Monster sich in bemerkenReichweite zum Spieler bewegen kann. Mit A*.
     *  @author Kriehn, Lorenz, 7311063
     */
    public boolean spielerWirdGesehen(int spielerX, int spielerY){

        LinkedList<WegPunkte> lLW = new LinkedList<>();
        lLW = kI.bewegeVonZu(getxPos(),getyPos(),spielerX,spielerY);
        if (!lLW.isEmpty())
        {return (lLW.getFirst().getgWert() <= getBemerkenReichweite());}
        return false;


    }

    /**
     * Berechnet die Goldbeute die das Monster bei sich hat, wenn es stirbt. Nach der Formel aus dem Multi User Dungeon Guide.
     * @author Kriehn, Lorenz, 7311063
     */
    public void berechneGoldBeute(){
        setGoldBeute((int) (5 * Math.pow(1.12,tiefe)));
    }

    /**
     * Berechnet die Erfahrungsbeute die das Monster gibt, wenn es stirbt. Nach der Formel aus dem Multi User Dungeon Guide.
     * @author Kriehn, Lorenz, 7311063
     */

    public void berechneXPBeute(){
        setXpBeute((int) (5 * Math.pow(1.12,tiefe)));
    }


    //<editor-fold desc="Getter/Setter">

    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public Spieler getWurdeAngegriffenVon() {
        return wurdeAngegriffenVon;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setWurdeAngegriffenVon(Spieler wurdeAngegriffenVon) {
        this.wurdeAngegriffenVon = wurdeAngegriffenVon;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public boolean isGeflohen() {
        return geflohen;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setGeflohen(boolean geflohen) {
        this.geflohen = geflohen;
    }

    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */

    public void setEnthaeltSchluessel(boolean b) {
        this.enthaeltSchluessel=b;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public boolean isEnthaeltSchluessel(){
        return enthaeltSchluessel;
    }
    /** Setter der automatisch den Automaten in den richtigen Zustand setzt.
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setSichtbar(boolean b) {
        if(b == false) setZustandAutomat(0);
        if (b== true) setZustandAutomat(3);
        this.sichtbar=b;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public boolean isSichtbar(){
        return sichtbar;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public Item getBeute() {
        return beute;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setBeute(Item beute) {
        this.beute = beute;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public int getTiefe() {
        return tiefe;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setTiefe(int tiefe) {
        this.tiefe = tiefe;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public int getGoldBeute() {
        return goldBeute;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setGoldBeute(int goldBeute) {
        this.goldBeute = goldBeute;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public int getXpBeute() {
        return xpBeute;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setXpBeute(int xpBeute) {
        this.xpBeute = xpBeute;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public int getMinTiefe() {
        return minTiefe;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setMinTiefe(int minTiefe) {
        this.minTiefe = minTiefe;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public int getMaxTiefe() {
        return maxTiefe;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setMaxTiefe(int maxTiefe) {
        this.maxTiefe = maxTiefe;
    }

    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public int getAttReichweite() {
        return attReichweite;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setAttReichweite(int attReichweite) {
        this.attReichweite = attReichweite;
    }

    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public int getZustandAutomat() {
        return zustandAutomat;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setZustandAutomat(int zustandAutomat) {
        this.zustandAutomat = zustandAutomat;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public int getBemerkenReichweite() {
        return bemerkenReichweite;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setBemerkenReichweite(int bemerkenReichweite) {
        this.bemerkenReichweite = bemerkenReichweite;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public float getFliehWert() {
        return fliehWert;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setFliehWert(float fliehWert) {
        this.fliehWert = fliehWert;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public int getMonsterID() {
        return monsterID;
    }
    /** Getter und Setter
     *  @autor Kriehn, Lorenz, 7311063
     */
    public void setMonsterID(int monsterID) {
        this.monsterID = monsterID;
    }

    public void voidSpieler() {
        this.spieler = null;
    }
    //</editor-fold>
}
