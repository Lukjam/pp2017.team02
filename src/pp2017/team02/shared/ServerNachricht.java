package pp2017.team02.shared;

/**
 * @author Jamann, Lukas, 7309791
 */

public class ServerNachricht extends Nachricht {

    /**
     * Zur Kommunizierung zwischen Comm und SWV.
     * @author Jamann, Lukas, 7309791
     */
    public ServerNachricht(int clientIDComm, boolean ueberladen) {
        super(clientIDComm, ueberladen);
    }
}
