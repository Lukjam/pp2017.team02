package pp2017.team02.shared;


import java.io.Serializable;
import java.util.LinkedList;

import pp2017.team02.shared.*;

/**
 * Datentyp, der einen Raum (Labyrinth) eines Levels darstellt
 *
 * @author <Mack, Jonas, 5975514>
 */

public class Karte implements Serializable{
    private static final long serialVersionUID = 1L;

    private SpielfeldElement[][] karte;
    private int[][] intKarte;
    private LinkedList<Monster> monsterListe;
    private LinkedList<Trank> trankListe;
    private Tuer tuer1, tuer2;
    private LinkedList<Integer> clientIDs = new LinkedList<Integer>();
    private int groesse;

    public Karte(){}


    /**
     * GETTER / SETTER
     */
    public Tuer getTuer1() {
        return tuer1;
    }

    public void setTuer1(Tuer tuer1) {
        this.tuer1 = tuer1;
    }

    public Tuer getTuer2() {
        return tuer2;
    }

    public void setTuer2(Tuer tuer2) {
        this.tuer2 = tuer2;
    }

    public Karte(int groesse) {
        this.karte = new SpielfeldElement[groesse][groesse];
        this.groesse = groesse;
        this.intKarte = new int[groesse][groesse];
    }

    public SpielfeldElement getEintrag(int x, int y) {
        return karte[y][x];
    }

    public void setEintrag(int x, int y, SpielfeldElement eintrag) {
        this.karte[y][x] = eintrag;
    }

    public LinkedList<Monster> getMonsterListe() {
        return monsterListe;
    }

    public void setMonsterListe(LinkedList<Monster> monsterListe) {
        this.monsterListe = monsterListe;
    }

    public LinkedList<Trank> getTrankListe() {
        return trankListe;
    }

    public void setTrankListe(LinkedList<Trank> trankListe) {
        this.trankListe = trankListe;
    }

    public SpielfeldElement[][] getKarte() {
        return karte;
    }

    public LinkedList<Integer> getClientIDs() {
        return clientIDs;
    }


    public void setClientIDs(LinkedList<Integer> clientIDs) {
        this.clientIDs = clientIDs;
    }

    /**
     * gibt eine int[][] Karte für die Monster aus.
     * @author Jamann, Lukas, 7309791
     */
    public int[][] getIntKarte() {
        for (int i = 0; i < groesse; i++){
            for (int j = 0; j < groesse; j++){
                intKarte[i][j] = getEintrag(i,j).getTyp();
            }
        }
        return intKarte;
    }

    /**
     * Fuegt clientID zur ClientIDliste hinzu
     */
    public void addClientID(int clientID) {
        clientIDs.add(clientID);
    }

    /**
     * Entfernt clientID aus der ClientIDliste
     */
    public void deleteClientID(int clientID) {
        clientIDs.remove(clientID);
    }

    /**
     * Fuegt Trank zur Trankliste hinzu
     */
    public void addTrank(Trank t){
        trankListe.add(t);
    }

    /**
     * Loescht Trank an Position i aus der Trankliste
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void loescheTrank(int i){
        trankListe.remove(i);
    }

    /**
     * Loescht Monster an Position i aus der Monsterliste
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void loescheMonster(int i){
        monsterListe.remove(i);
    }

}
