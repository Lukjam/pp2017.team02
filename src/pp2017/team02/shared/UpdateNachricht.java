package pp2017.team02.shared;

import java.util.LinkedList;

/**
 * Klasse zum Uebertragen der Daten bezogen auf die Monster, die sich auf der Karte bewegen
 *
 * @author Jamann, Lukas, 7309791
 * @author Tiebing, Alexander, 6018254
 */

public class UpdateNachricht extends Nachricht {

    /**
     * Erschaffung von Variablen Listen mit der ID und den neuen Positionen der Daten.
     *
     * @author Jamann, Lukas, 7309791
     */
    private LinkedList<Integer[]> xy;
    private LinkedList<Integer> monsterID;
    private LinkedList<int[]> monsterListe;

    public int getAktuelleKarte() {
        return aktuelleKarte;
    }

    private int aktuelleKarte;


    private Level level;


    /**
     * Wenn sich Monster updaten, schickt der Server hiermit direkt daie komplette Monsterliste der Karte neu
     *
     * @author Tiebing, Alexander, 6018254
     */
    public UpdateNachricht(int clientID, int clientIDComm, LinkedList<int[]> monsterListe, int aktuelleKarte){
        super(clientID,clientIDComm);
        this.monsterListe = monsterListe;
        this.aktuelleKarte = aktuelleKarte;
    }



    /**
     * Getter
     *
     * @author Jamann, Lukas, 7309791
     */
    public LinkedList<Integer[]> getXy() {
        return xy;
    }

    public LinkedList<Integer> getMonsterID() {
        return monsterID;
    }

    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public Level getLevel() {
        return level;
    }

    public LinkedList<int[]> getMonsterListe() {
        return monsterListe;
    }



}