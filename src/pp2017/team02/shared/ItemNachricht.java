package pp2017.team02.shared;

import java.util.LinkedList;

/**
 * Klasse wird benutzt, wenn der Client ein Item von der Karte aufnimmt, es ablegt bzw. benutzt
 *
 * @author Tiebing, Alexander, 6018254
 * @author Jamann, Lukas, 7309791
 */
public class ItemNachricht extends Nachricht {

    private boolean schluessel;
    private LinkedList<Item> inventar;
    private Level level;


    /**
     * Server schickt das aktualisierte Level zurueck an den Client
     *
     * @author Jamann, Lukas, 7309791
     */
    public ItemNachricht(int clientID, int clientIDComm, Level level){
        super(clientID, clientIDComm);
        this.level = level;
    }

    /**
     * Client schickt dem Server Nachricht, dass Schluessel benutzt wurde
     *
     * @author Tiebing, Alexander, 6018254
     */
    public ItemNachricht(int clientID, boolean schluessel, Level level){
        super(clientID);
        this.schluessel = schluessel;
        this.level = level;
    }

    /**
     * Client schickt Server Nachricht, dass Trank aufgenommen bzw. gedroppt wurde
     * Es werden das Inventar des Spielers, sowie das aktualisierte Level weitergegeben
     *
     * @author Tiebing, Alexander, 6018254
     */
    public ItemNachricht(int clientID, LinkedList<Item> inventar, Level level){
        super(clientID);
        this.inventar = inventar;
        this.level = level;
    }


    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public boolean isSchluessel() {
        return schluessel;
    }

    public LinkedList<Item> getInventar() {
        return inventar;
    }

    public Level getLevel() {
        return level;
    }


}
