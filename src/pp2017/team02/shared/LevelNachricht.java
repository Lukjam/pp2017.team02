package pp2017.team02.shared;

/**
 * Klasse fuer das Uebertragen der Daten, die benoetigt werden, wenn der Spieler das naechste Level erreicht
 *
 * @author Tiebing, Alexander, 6018254
 * @author Jamann, Lukas, 7309791
 */

public class LevelNachricht extends Nachricht {


    private Spieler spieler;
    private int levelNr;


    /**
     * Wenn Spieler das Level wechselt, wird dem Server die Levelnummer des Levels mitgeteilt, welches gerade verlassen
     * wurde
     *
     * @author Tiebing, Alexander, 6018254
     */
    public LevelNachricht(int clientID, int levelNr) {
        super(clientID);
        this.levelNr = levelNr;
    }

    /**
     * Wenn Spieler Level gewechselt hat, schickt der Server dem Client hier den aktualisierten Spieler. Das Level ist
     * im Spieler gespeichet
     *
     * @author Jamann, Lukas, 7309791
     */
    public LevelNachricht(int clientID, int clientIDComm, Spieler spieler) {
        super(clientID, clientIDComm);
        this.spieler = spieler;
    }

    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public Spieler getSpieler() {
        return spieler;
    }

    public int getLevelNr() {
        return levelNr;
    }
}