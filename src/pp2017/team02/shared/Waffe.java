package pp2017.team02.shared;

/**
 * Beschreibt die Klasse Waffe.
 * Ein Spieler kann eine Waffe tragen.
 * @author Kriehn, Lorenz, 7311063
 */
public class Waffe extends Item{
    private int angriffsWert;

    /**
     * Konstruktor der Klasse.
     * @author Kriehn, Lorenz, 7311063
     */
    public Waffe(int x, int y, int angriffsWert){
        this.setXpos(x);
        this.setYpos(y);
        this.setAngriffsWert(angriffsWert);


    }

    public int getAngriffsWert() {
        return angriffsWert;
    }

    public void setAngriffsWert(int angriffsWert) {
        this.angriffsWert = angriffsWert;
    }
}
