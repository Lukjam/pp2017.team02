package pp2017.team02.shared;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Datentyp, der ein Level repraesentiert
 *
 * @author <Mack, Jonas, 5975514>
 *
 */

public class Level implements Serializable{

    private LinkedList<Karte> karten = new LinkedList<Karte>();
    private Eingang eingang;
    private Ausgang ausgang;
    private int levelnr;
    private static final long serialVersionUID = 1L;

    public Level (int levelnr) {
        this.levelnr = levelnr;
    }

    public LinkedList<Karte> getKarten() {
        return karten;
    }
    public void addKarte(int index, Karte karte) {
        this.karten.add(index, karte);
    }
    public Eingang getEingang() {
        return eingang;
    }
    public void setEingang(Eingang eingang) {
        this.eingang = eingang;
    }
    public Ausgang getAusgang() {
        return ausgang;
    }
    public void setAusgang(Ausgang ausgang) {
        this.ausgang = ausgang;
    }
    public int getLevelnr() {
        return levelnr;
    }

    public void setLevelnr(int levelnr) {
        this.levelnr = levelnr;
    }




}