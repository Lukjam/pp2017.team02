package pp2017.team02.shared;


/**
 * Klasse zur Uebertragung der Spieler, die im selben Raum sind, und der, die nicht im selben Raum sind
 *
 * @author Tiebing, Alexander, 6018254
 */

public class MitspielerNachricht extends Nachricht {

    private Spieler nichtimRaum;
    private Spieler istimRaum;


    /**
     * Schickt dem Client die Spieler, die im selben Raum sind
     *
     * @author Tiebing, Alexander, 6018254
     */
    public MitspielerNachricht(int clientID, int clientIDComm, Spieler istimRaum) {
        super(clientID, clientIDComm);
        this.istimRaum = istimRaum;
    }

    /**
     * Schickt dem Client die Spieler, die nicht im selben Raum wie der Client sind
     *
     * @author Tiebing, Alexander, 6018254
     */
    public MitspielerNachricht(int clientID, int clientIDComm, Spieler nichtimRaum, boolean überladung) {
        super(clientID, clientIDComm);
        this.nichtimRaum = nichtimRaum;
    }

    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public Spieler getNichtimRaum() {
        return nichtimRaum;
    }

    public Spieler getIstimRaum() {
        return istimRaum;
    }

}
