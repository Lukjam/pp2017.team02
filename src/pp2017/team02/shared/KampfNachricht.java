package pp2017.team02.shared;


/**
 * Klasse fuer das Uebertragen der Daten, die im Kampf abgefragt werden
 *
 * @author Tiebing, Alexander, 6018254
 * @author Jamann, Lukas, 7309791
 */

public class KampfNachricht extends Nachricht{

    private int schaden = -1;
    private int monsterID;
    private int erfahrung = -1;
    private int monsterLeben = -1;
    private Item item;
    private boolean schluessel =  false;


    /**
     * Wenn der Spieler ein Monster angreift, wird hiermit dem Server seine clientID durchgegeben,
     * sowie die ID des Monsters, welches angegriffen wurde
     *
     * @author Tiebing, Alexander, 6018254
     */
    public KampfNachricht(int clientID, int monsterID, boolean ueberladung){
        super(clientID);
        this.monsterID = monsterID;
    }


    /**
     * Zum Uebertragen des Schadens, den das Monster dem Spieler gemacht hat
     *
     * @author Jamann, Lukas, 7309791
     */
    public KampfNachricht(int clientID, int clientIDComm, int schaden) {
        super(clientID, clientIDComm);
        this.schaden = schaden;
    }

    /**
     * Zum Uebertragen des Lebens des Monsters, welches vom Spieler angegriffen wurde
     *
     * @author Jamann, Lukas, 7309791
     */
    public KampfNachricht(int clientID, int clientIDComm, int monsterID, int monsterLeben) {
        super(clientID, clientIDComm);
        this.monsterID = monsterID;
        this.monsterLeben = monsterLeben;
    }

    /**
     * Zum Uebertragen des getoeteten Monsters. Der Client bekommt Erfahrunspunkte und gegebenenfalls einen Schluessel
     * vom Monster. Das Monster droppt moeglicherweise ein Item.
     *
     * @author Jamann, Lukas, 7309791
     */
    public KampfNachricht(int clientID, int clientIDComm, int monsterID, int erfahrung, Item item, boolean schluessel) {
        super(clientID, clientIDComm);
        this.monsterID = monsterID;
        this.erfahrung = erfahrung;
        this.item = item;
        this.schluessel = schluessel;

    }


    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public int getSchaden() {
        return schaden;
    }

    public int getMonsterID() {
        return monsterID;
    }

    public int getErfahrung() {
        return erfahrung;
    }

    public int getMonsterLeben() {
        return monsterLeben;
    }

    public Item getItem() {
        return item;
    }

    public boolean isSchluessel() {
        return schluessel;
    }


}
