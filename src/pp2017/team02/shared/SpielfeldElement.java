package pp2017.team02.shared;

import java.io.Serializable;

/**
 * Datenstruktur, die ein Feld der Spielkarte darstellt
 *
 * @author Jonas Mack, 5975514
 *
 */
public class SpielfeldElement implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     * 9 Konstanten, welche den Typ und den Inhalt eines SpielfeldElements repraesentieren
     *
     * @author Jonas, 5975514
     *
     */
    // Typ
    public static final int RAND = 0;
    public static final int WAND = 1;
    public static final int BODEN = 2;
    public static final int TUER = 3; //Tuer in einen Nachbarraum
    public static final int EINGANG = 4; //Leiter aus vorherigem Level
    public static final int AUSGANG = 5; //Leiter ins naechste Level

    /**
     * 5 Attribute, die Informationen ueber das SpielfeldElement liefern
     *
     * @author Jonas, 5975514
     *
     */
    private int typ;
    private boolean enthaeltSchluessel = false;
    private boolean enthaeltTrank = false;
    private boolean enthaeltSpieler = false;
    private boolean enthaeltMonster = false;
    private boolean begehbar = false;

    /**
     * Getter und Setter Methoden fuer die 5 Attribute
     *
     * @author Jonas, 5975514
     *
     */
    public int getTyp() {
        return typ;
    }

    public void setTyp(int typ) {
        this.typ = typ;
    }

    public boolean isEnthaeltSchluessel() {
        return enthaeltSchluessel;
    }

    public void setEnthaeltSchluessel(boolean enthaeltSchluessel) {
        this.enthaeltSchluessel = enthaeltSchluessel;
    }

    public boolean isEnthaeltTrank() {
        return enthaeltTrank;
    }

    public void setEnthaeltTrank(boolean enthaeltTrank) {
        this.enthaeltTrank = enthaeltTrank;
    }

    public boolean isEnthaeltSpieler() {
        return enthaeltSpieler;
    }

    public void setEnthaeltSpieler(boolean enthaeltSpieler) {
        this.enthaeltSpieler = enthaeltSpieler;
    }

    public boolean isEnthaeltMonster() {
        return enthaeltMonster;
    }

    public void setEnthaeltMonster(boolean enthaeltMonster) {
        this.enthaeltMonster = enthaeltMonster;
    }

    public boolean isBegehbar() {
        return begehbar;
    }

    public void setBegehbar(boolean begehbar) {
        this.begehbar = begehbar;
    }


    /**
     * Methode, die ueberprueft ob ein Feld ein Element vom Typ Rand oder Wand ist
     *
     * @author Jonas Mack, 5975514
     */

    public boolean nichtBegehbar(){
        if (typ==RAND || typ == WAND) {
            return true;
        }
        return false;
    }
}

