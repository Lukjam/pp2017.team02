package pp2017.team02.shared;

import java.io.Serializable;

/**
 * Grundklasse aus der alle Figuren abgeleitet werden.
 *
 * @author Kriehn, Lorenz, 7311063
 */
public class Figur implements Serializable {
    private int leben;
    private int maxLeben;
    private int angriffsWert;
    private int xPos,yPos;
    private int karte[][];
    private int verteidigungsWert;
    private long letzterAngriff;
    private long letzterZug;
    private int cdAngriff;
    private int cdZug;

    private static final long serialVersionUID = 34L;



    //TODO Bewegungsmethoden schon in CE vorhanden, hier ueberhaupt noetig?
/*   public void oben(){
 *      yPos--;
 *  }
 *   public void unten(){
 *       yPos++;
 *   }
 *   public void links(){
 *       xPos--;
 *   }
 *   public void rechts(){
 *       xPos++;
 *   }
*/
    // Addiert die eingabe zum aktuellen Leben. TODO sollte jetzt eigentlich klappen

    /**
     * Lebensaenderung fuer die Figur.
     * @author Kriehn, Lorenz, 7311063
     */
    public void changeLeben(int lebensAenderung){
        System.out.println(getLeben() + lebensAenderung+"Monsterleben");
        if (lebensAenderung <= 0)
            setLeben(Math.max(0,getLeben()+lebensAenderung));
        System.out.println(getLeben()+lebensAenderung);
        if (lebensAenderung > 0){
            setLeben(Math.min(getMaxLeben(),getLeben()+lebensAenderung));
        }
    }

    // Getter/Setter

    public int[][] getKarte() {
        return karte;
    }

    public void setKarte(int[][] karte) {
        this.karte = karte;
    }

    public int getLeben() {
        return leben;
    }

    public void setLeben(int leben) {
        this.leben = leben;
    }

    public int getMaxLeben() {
        return maxLeben;
    }

    public void setMaxLeben(int maxLeben) {
        this.maxLeben = maxLeben;
    }

    public int getAngriffsWert() {
        return angriffsWert;
    }

    public void setAngriffsWert(int angriffsWert) {
        this.angriffsWert = angriffsWert;
    }


    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int[] getPosition(){
        int[] tempPos = new int[2];
        tempPos[0] = getxPos();
        tempPos[1] = getyPos();
        return tempPos;
    }

    public void setPosition(int[] tempPos){
        setxPos(tempPos[0]);
        setyPos(tempPos[1]);
    }

    public int getVerteidigungsWert() {
        return verteidigungsWert;
    }

    public void setVerteidigungsWert(int verteidigungsWert) {
        this.verteidigungsWert = verteidigungsWert;
    }

    public long getLetzterAngriff() {
        return letzterAngriff;
    }

    public void setLetzterAngriff(long letzterAngriff) {
        this.letzterAngriff = letzterAngriff;
    }

    public long getLetzterZug() {
        return letzterZug;
    }

    public void setLetzterZug(long letzterZug) {
        this.letzterZug = letzterZug;
    }

    public int getCdAngriff() {
        return cdAngriff;
    }

    public void setCdAngriff(int cdAngriff) {
        this.cdAngriff = cdAngriff;
    }

    public int getCdZug() {
        return cdZug;
    }

    public void setCdZug(int cdZug) {
        this.cdZug = cdZug;
    }
}
