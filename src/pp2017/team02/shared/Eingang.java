package pp2017.team02.shared;


/**
 * Datentyp, der den Startpunkt des Spielers für das Level repraesentiert
 *
 * @author <Mack, Jonas, 5975514>
 */
public class Eingang extends SpielfeldElement {

    private Karte karte;
    private int xpos;
    private int ypos;
    private int kartenNummer; //Speichert auf welcher Karte der Eingang liegt

    public Eingang(Karte karte, int x, int y) {
        super.setTyp(EINGANG);
        super.setBegehbar(true);
        this.karte = karte;
        this.xpos = x;
        this.ypos = y;
    }

    public Eingang(){
        super.setTyp(EINGANG);
    }

    public Karte getKarte() {
        return karte;
    }

    public void setKarte(Karte karte) {
        this.karte = karte;
    }

    public int getXpos() {
        return xpos;
    }

    public void setXpos(int xpos) {
        this.xpos = xpos;
    }

    public int getYpos() {
        return ypos;
    }

    public void setYpos(int ypos) {
        this.ypos = ypos;
    }

    public int getKartenNummer() {
        return kartenNummer;
    }

    public void setKartenNummer(int kartenNummer) {
        this.kartenNummer = kartenNummer;
    }


}
