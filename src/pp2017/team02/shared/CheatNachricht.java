package pp2017.team02.shared;

/**
 * Klasse fuer die Nachrichten, die dem Client die Auswirkungen der eingegebenen Cheats mitteilt
 *
 * @author Tiebing, Alexander, 6018254
 * @author Jamann, Lukas, 7309791
 */
public class CheatNachricht extends Nachricht {

    private int leben;
    private boolean schluessel;

    /**
     * Server sagt Client, dass der Cheat, der ihm wieder volles Leben gibt, erfolgreich ausgefuehrt wurde
     *
     * @author Jamann, Lukas, 7309791
     */
    public CheatNachricht(int clientID, int clientIDComm, int leben) {
        super(clientID, clientIDComm);
        this.leben = leben;
    }

    /**
     * Server sagt Client, dass der Cheat, der ihm den Schluessel gibt, erfolgreich ausgefuehrt wurde
     *
     * @author Tiebing, Alexander, 6018254
     */
    public CheatNachricht(int clientID, int clientIDComm, boolean schluessel) {
        super(clientID, clientIDComm);
        this.schluessel = schluessel;
    }

    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public int getLeben() {
        return leben;
    }

    public boolean isSchluessel() {
        return schluessel;
    }
}

