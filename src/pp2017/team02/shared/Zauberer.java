package pp2017.team02.shared;

/**
 * Klasse des Monsters Zauberer.
 * @author Kriehn, Lorenz, 7311063
 */

public class Zauberer extends Monster{

    /**
     * Konstruktor der Klasse.
     * @author Kriehn, Lorenz, 7311ß63
     */
    public Zauberer(int xPos, int yPos,int[][] karte,int tiefe, boolean hatSchluessel){
        super(xPos,yPos,karte, tiefe, hatSchluessel);

        this.setAttReichweite(1);
        this.setBemerkenReichweite(5);
        this.setCdAngriff(500);
        this.setCdZug(700);
        this.setFliehWert(0.4f);
        this.setMaxLeben((int)(150 * Math.pow(1.5,tiefe-1)));
        this.setLeben(getMaxLeben());
        this.setMinTiefe(0);
        this.setMaxTiefe(Integer.MAX_VALUE);
        this.setVerteidigungsWert(1*tiefe);
        this.setAngriffsWert( 3 + (int) (tiefe * 1.5));
        this.setZustandAutomat(1);


    }
}
