package pp2017.team02.shared;

import java.util.LinkedList;

/**
 * Klasse zum Uebertragen der Daten, die fuer den Login benoetigt werden
 *
 * @author Tiebing, Alexander, 6018254
 * @author Jamann, Lukas, 7309791
 */

public class LoginNachricht extends Nachricht {

    private int sessionID;
    private String benutzerName;
    private String passwort;
    private Spieler spieler;
    private boolean loginErfolgreich;
    private boolean passwortFalsch;
    private boolean neuesKonto;
    private LinkedList<Integer> skill;  // 1. Eintrag Staerke, 2. Eintrag Geschicklichkeit, 3. Eintrag Intelligenz


    /**
     * Wenn der Client sich einloggen will, schickt er dem Server den eingegebenen Namen und das Passwort
     *
     * @author Tiebing, Alexander, 6018254
     */
    public LoginNachricht(String benutzerName,String passwort, int sessionID){
        this.benutzerName = benutzerName;
        this.passwort = passwort;
        this.sessionID = sessionID;
    }

    /**
     * Server sagt Client, dass der Login erfolgreich war, weist ihm eine ClientID zu und schickt ihm den Spieler
     *
     * @author Jamann, Lukas, 7309791
     */
    public LoginNachricht(int clientID, int clientIDComm, int sessionID, String benutzerName, String passwort, Spieler spieler, boolean loginErfolgreich) {
        super(clientID, clientIDComm);
        this.sessionID = sessionID;
        this.benutzerName = benutzerName;
        this.passwort = passwort;
        try {
            this.spieler = (Spieler) spieler.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        this.loginErfolgreich = loginErfolgreich;
    }

    /**
     * Server sagt Client, dass das eingegebene Passwort nicht zum Benutzernamen passt
     *
     * @author Jamann, Lukas, 7309791
     */

    public LoginNachricht(int clientID, int clientIDComm, int sessionID, String benutzerName, String passwort, boolean passwortFalsch) {
        super(clientID, clientIDComm);
        this.sessionID = sessionID;
        this.benutzerName = benutzerName;
        this.passwort = passwort;
        this.passwortFalsch = passwortFalsch;
    }

    /**
     * Server sagt Client, dass ein neues Konto erfolgreich angelegt wurde, und dass jetzt Skillpunkte verteilt werden
     * muessen
     *
     * @author Jamann, Lukas, 7309791
     */

    public LoginNachricht(int clientID, int clientIDComm, int sessionID, String benutzerName, String passwort, boolean neuesKonto, boolean überladen) {
        super(clientID, clientIDComm);
        this.sessionID = sessionID;
        this.benutzerName = benutzerName;
        this.passwort = passwort;
        this.neuesKonto = neuesKonto;
    }

    /**
     * Client uebergibt Server die Skillpunktverteilung in der Reihenfolge: 1. Eintrag Staerke,
     * 2. Eintrag Geschicklichkeit, 3. Eintrag Intelligenz
     *
     * @author Jamann, Lukas, 7309791
     */
    public LoginNachricht(int sessionID, String benutzerName, String passwort, boolean neuesKonto, LinkedList<Integer> skill) {
        this.sessionID = sessionID;
        this.benutzerName = benutzerName;
        this.passwort = passwort;
        this.neuesKonto = neuesKonto;
        this.skill = skill;
    }

    /**
     * GETTER / SETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public String getBenutzerName() {
        return benutzerName;
    }

    public String getPasswort() {
        return passwort;
    }

    public Spieler getSpieler() {
        return spieler;
    }

    public int getSessionID() {
        return sessionID;
    }

    public boolean isLoginErfolgreich() {
        return loginErfolgreich;
    }

    public void setLoginErfolgreich(boolean loginErfolgreich) {
        this.loginErfolgreich = loginErfolgreich;
    }

    public boolean isPasswortFalsch() {
        return passwortFalsch;
    }

    public void setPasswortFalsch(boolean passwortFalsch) {
        this.passwortFalsch = passwortFalsch;
    }

    public boolean isNeuesKonto() {
        return neuesKonto;
    }

    public void setNeuesKonto(boolean neuesKonto) {
        this.neuesKonto = neuesKonto;
    }

    public LinkedList<Integer> getSkill() {
        return skill;
    }
}
