package pp2017.team02.shared;

/**
 * Datentyp, der für den Ausgang(=Eingang ins naechste Level) benoetigt wird
 *
 * @author <Mack, Jonas, 5975514>
 */
public class Ausgang extends SpielfeldElement {

    private Karte karte;
    private int xpos;
    private int ypos;
    private int kartenNummer;

    public Ausgang(Karte karte, int x, int y) {
        super.setTyp(AUSGANG);
        super.setBegehbar(true);
        this.karte = karte;
        this.xpos = x;
        this.ypos = y;
    }

    public Karte getKarte() {
        return karte;
    }

    public void setKarte(Karte karte) {
        this.karte = karte;
    }

    public int getXpos() {
        return xpos;
    }

    public void setXpos(int xpos) {
        this.xpos = xpos;
    }

    public int getYpos() {
        return ypos;
    }

    public void setYpos(int ypos) {
        this.ypos = ypos;
    }

    public int getKartenNummer() {
        return kartenNummer;
    }

    public void setKartenNummer(int kartenNummer) {
        this.kartenNummer = kartenNummer;
    }

}