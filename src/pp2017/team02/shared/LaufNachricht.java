package pp2017.team02.shared;

/**
 * Klasse fuer das Uebertragen der Daten, die beim Bewegen des Spielers benoetigt werden
 *
 * @author Tiebing, Alexander, 6018254
 */

public class LaufNachricht extends Nachricht{

    private int xPosNeu;
    private int yPosNeu;


    /**
     * Zum Uebertragen der neuen Position des Spielers an den Server
     *
     * @author Tiebing, Alexander, 6018254
     */
    public LaufNachricht(int clientID, int xPosNeu, int yPosNeu) {

        super(clientID);
        this.xPosNeu = xPosNeu;
        this.yPosNeu = yPosNeu;
    }

    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public int getxPosNeu() {
        return xPosNeu;
    }

    public int getyPosNeu() {
        return yPosNeu;
    }
}