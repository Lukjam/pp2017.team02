package pp2017.team02.shared;

/**
 * Beschreibt alle Ruestungen die der Spieler anziehen kann.
 * @author Kriehn, Lorenz, 7311063
 */
public class Ruestung extends Item{
    private  int verteidigungsWert;

    /**
     * Erstellt eine Ruestung mit zufaelligem Verteidigungswert.
     * @author Kriehn, Lorenz, 7311063
     *
     */
    public Ruestung(int x, int y, int verteidigungsWert){
        this.setXpos(x);
        this.setYpos(y);
        this.setVerteidigungsWert(verteidigungsWert);

    }

    public int getVerteidigungsWert() {
        return verteidigungsWert;
    }

    public void setVerteidigungsWert(int verteidigungsWert) {
        this.verteidigungsWert = verteidigungsWert;
    }
}
