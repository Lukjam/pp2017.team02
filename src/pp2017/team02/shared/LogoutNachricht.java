package pp2017.team02.shared;

/**
 * Klasse zur Uebertragung der Daten, die gebraucht werden, wenn ein Spieler sich ausloggt
 *
 * @author Tiebing, Alexander, 6018254
 */

public class LogoutNachricht extends Nachricht {

    private String benutzername;

    /**
     * Konstruktor wird benutzt, wenn ein Spieler sich ausloggt, und dies dem Server mitteilt
     *
     * @author Tiebing, Alexander, 6018254
     */
    public LogoutNachricht(int clientID){
        super(clientID);
    }

    /**
     * TODO wofuer wird dieser Konstruktor gebraucht?
     *
     * @author Tiebing, Alexander, 6018254
     */
    public LogoutNachricht(int clientID, String benutzername) {
        super(clientID);
        this.benutzername = benutzername;
    }

    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public String getBenutzername() {
        return benutzername;
    }
}
