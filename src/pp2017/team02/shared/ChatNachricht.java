package pp2017.team02.shared;

/**
 * Klasse fuer Chatnachrichten, die der Client an den Server schickt, und der Server an alle Clients schickt.
 * Cheats werden ebenfalls als Chatnachrichten dem Server geschickt
 *
 * @author Tiebing, Alexander, 6018254
 */
public class ChatNachricht extends Nachricht{

    private String text;

    /**
     * Chatnachricht von Client an Server
     *
     * @author Tiebing, Alexander, 6018254
     */
    public ChatNachricht(int clientID, String text){
        super(clientID);
        this.text = text;
    }

    /**
     * GETTER
     *
     * @author Tiebing, Alexander, 6018254
     */
    public String getText() {
        return text;
    }
}
