package pp2017.team02.shared;

/**
 *
 * @author <Mack, Jonas, 5975514>
 *
 */

public class Tuer extends SpielfeldElement {

    private Karte nachbarraum;
    private boolean offen;
    private int xPos;
    private int yPos;

    public Tuer(Karte karte, int xPos, int yPos) {
        this.nachbarraum = karte;
        super.setTyp(TUER);
        super.setBegehbar(true);
        this.offen=false;
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public Karte getNachbarraum() {
        return nachbarraum;
    }

    public void setOffen(){
        this.offen=true;
    }

    public boolean isOffen() {
        return offen;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }



}
