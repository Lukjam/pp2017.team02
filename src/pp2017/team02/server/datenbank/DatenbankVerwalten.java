package pp2017.team02.server.datenbank;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import pp2017.team02.server.map.LevelGenerator;
import pp2017.team02.shared.Level;
import pp2017.team02.shared.Spieler;

/**
 * Verwaltet eine bestehende Datenbank
 *
 * @author sqlitetutorial.net
 * @author <Mack, Jonas, 5975514>
 *
 */
public class DatenbankVerwalten {

    private Spieler spieler;
    private LevelGenerator generator = new LevelGenerator();
    private Random r = new Random();

    // Benoetigte Attribute im Spieler
    private int clientID, staerke, geschicklichkeit, intelligenz, stufe, erfahrung, skillPunkte, levelnr, anzahlTraenke;


    /**
     * Verbindet mit der bestehenden Datenbank Datenbank.db
     *
     * @author http://www.sqlitetutorial.net/sqlite-java/select/
     *
     *
     */
    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:" + "Datenbank.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    /**
     * Legt einen neuen Spieler in der Datenbank an. Danach wird ein Objekt vom Typ Spieler mit clientID,
     * spielername, und den Startkoordinaten des ersten Levels zurueckgegeben.
     * Ist der Spielername, welcher in der Datenbank angelegt werden soll schon vergeben, wird keine Bearbeitung
     * der Datenbank vorgenommen und es wird ein mit null initialisierter Spieler zurueckgegeben
     *
     * @author <Mack, Jonas, 5975514>
     */

    public Spieler SpielerAnlegen(String spielername, String passwort, int st, int ge, int in) {
        // Um spieler zu erstellen, wird x und y Position vom Eingang des ersten Levels benötigt
        int xPos = generator.erzeugeLevel(1).getEingang().getXpos();
        int yPos = generator.erzeugeLevel(1).getEingang().getYpos();

        String sql = "INSERT INTO Spielerdaten(clientID, spielername, passwort, level, intelligenz, staerke, geschicklichkeit, erfahrung, stufe, anzahlTraenke, skillPunkte) VALUES(?,?,?,?,?,?,?,?,?,?,?)";

        // Initialisiert Werte der Datenbank
        int clientID = r.nextInt(900000)+100000;
        spieler = new Spieler(spielername, clientID, st,ge,in, generator.level);
        int level = 1;
        int intelligenz = spieler.getIntelligenz();
        int staerke = spieler.getStaerke();
        int geschicklichkeit = spieler.getGeschicklichkeit();
        int erfahrung = 0;
        int stufe = 1;
        int anzahlTraenke = 0;
        int skillPunkte = 0;

        // Schreibt obige sowie die uebergebenen Werte in die Datenbank, falls der Spielername noch nicht vorhanden ist
        try (Connection conn = this.connect();

             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, clientID);
            pstmt.setString(2, spielername);
            pstmt.setString(3, passwort);
            pstmt.setInt(4, level);
            pstmt.setInt(5, intelligenz);
            pstmt.setInt(6, staerke);
            pstmt.setInt(7, geschicklichkeit);
            pstmt.setInt(8, erfahrung);
            pstmt.setInt(9, stufe);
            pstmt.setInt(10, anzahlTraenke);
            pstmt.setInt(11, skillPunkte);
            pstmt.executeUpdate();




        } catch (SQLException e) {
            System.out.println(e.getMessage());

            // Falls Spielername schon vorhanden, gib leeren Spieler zurueck
            spieler = null;
        }

        return spieler;

    }




    /**
     * Login-Methode, welche die uebergebene clientID in der Datenbank sucht und ueberpreuft,
     * ob das uebermittelte Passwort korrekt ist und ggf. den Spieler zurueckgibt
     * Ist die Kombination aus clientID und passwort noch nicht in der Datenbank enthalten
     * gibt die Methode einen leeren Spieler zurueck
     *
     * @author <Mack, Jonas, 5975514>
     */

    public Spieler Login(String n, String pw) {

        String sql = "SELECT clientID, spielername, passwort, level, intelligenz, staerke, geschicklichkeit, erfahrung, stufe, anzahlTraenke, skillPunkte FROM Spielerdaten";

        boolean spielerEnthalten = false;

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while(rs.next() && !spielerEnthalten) {
                // Pruefe, ob die clientID enthalten ist
                String name = rs.getString("spielername");

                if (name.equals(n)) {
                    spielerEnthalten = true;

                    // Falls clientID in Datenbank enthalten, pruefe noch, ob das uebergebene Passwort korrekt ist
                    String passwort = rs.getString("passwort");

                    if (passwort.equals(pw)) {

                        // Falls Spieler gefunden und Passwort korrekt, lade alle Eintraege aus der Datenbank
                        int clientID = rs.getInt("clientID");
                        int levelnr = rs.getInt("level");
                        int intelligenz = rs.getInt("intelligenz");
                        int staerke = rs.getInt("staerke");
                        int geschicklichkeit = rs.getInt("geschicklichkeit");
                        int erfahrung = rs.getInt("erfahrung");
                        int stufe = rs.getInt("stufe");
                        int anzahlTraenke = rs.getInt("anzahlTraenke");
                        int skillPunkte = rs.getInt("skillPunkte");

                        // Erzeuge das Level, in dem sich Spieler laut Datenbank befindet
                        Level level = generator.erzeugeLevel(levelnr);
                        // Lade Kartennummer, in dem der Eingang des Levels liegt
                        int kartenNummer = level.getEingang().getKartenNummer();

                        // Uebergebe Eintraege aus der Datenbank, level und kartennummer an spieler
                        spieler = new Spieler(n, clientID, staerke, geschicklichkeit, intelligenz, stufe, erfahrung, skillPunkte, level, kartenNummer, anzahlTraenke);

                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());

            // Falls Login fehlgeschlagen gib leeren Spieler zurueck
            spieler = null;

        }

        return spieler;


    }


    /**
     * Logout-Methode, welche den Spieler in der Datenbank sucht, und die Datenbank daraufhin aktualisiert
     *
     * @author <Mack, Jonas, 5975514>
     */

    public void Logout(Spieler spieler) {
        // Lade Daten aus Spieler
        clientID = spieler.getClientID();
        levelnr = spieler.getAktuellesLevel().getLevelnr();
        intelligenz = spieler.getIntelligenz();
        staerke = spieler.getStaerke();
        geschicklichkeit = spieler.getGeschicklichkeit();
        erfahrung = spieler.getErfahrung();
        stufe = spieler.getStufe();
        anzahlTraenke = spieler.getInventar().size();
        skillPunkte = spieler.getSkillPunkte();

        String sql = "UPDATE Spielerdaten SET level = ?, "
                + "intelligenz = ?, "
                + "staerke = ?, "
                + "geschicklichkeit = ?, "
                + "erfahrung = ?, "
                + "stufe = ?, "
                + "anzahlTraenke = ?, "
                + "skillPunkte = ? "
                + "WHERE clientID = ?";

        // Falls Spieler in Datenbank enhalten, update der Datenbank
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, levelnr);
            pstmt.setInt(2, intelligenz);
            pstmt.setInt(3, staerke);
            pstmt.setInt(4, geschicklichkeit);
            pstmt.setInt(5, erfahrung);
            pstmt.setInt(6, stufe);
            pstmt.setInt(7, anzahlTraenke);
            pstmt.setInt(8, skillPunkte);
            pstmt.setInt(9, clientID);
            pstmt.executeUpdate();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Update-Methode, welche nach clientID sucht, die Datenbankeintraege
     * aktualisiert und anschliessend den Spieler in einem Level hoeher zurueckgibt
     *
     * @author <Mack, Jonas, 5975514>
     */

    public Spieler Update(Spieler spieler) {

        Spieler rueckgabeSpieler;

        // Lade Daten aus Spieler
        clientID = spieler.getClientID();
        String name = spieler.getName();
        levelnr = spieler.getAktuellesLevel().getLevelnr();
        intelligenz = spieler.getIntelligenz();
        staerke = spieler.getStaerke();
        geschicklichkeit = spieler.getGeschicklichkeit();
        erfahrung = spieler.getErfahrung();
        stufe = spieler.getStufe();
        anzahlTraenke = spieler.getInventar().size();
        skillPunkte = spieler.getSkillPunkte();

        String sql = "UPDATE Spielerdaten SET level = ?, "
                + "intelligenz = ?, "
                + "staerke = ?, "
                + "geschicklichkeit = ?, "
                + "erfahrung = ?, "
                + "stufe = ?, "
                + "anzahlTraenke = ?, "
                + "skillPunkte = ? "
                + "WHERE clientID = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, levelnr+1);
            pstmt.setInt(2, intelligenz);
            pstmt.setInt(3, staerke);
            pstmt.setInt(4, geschicklichkeit);
            pstmt.setInt(5, erfahrung);
            pstmt.setInt(6, stufe);
            pstmt.setInt(7, anzahlTraenke);
            pstmt.setInt(8, skillPunkte);
            pstmt.setInt(9, clientID);
            pstmt.executeUpdate();

            // Setze rueckgabeSpieler mit alten Werten, wobei das Level um eins erhöht wird
            Level aktuellesLevel = generator.erzeugeLevel(levelnr+1);
            int aktuelleKarte = aktuellesLevel.getEingang().getKartenNummer();
            rueckgabeSpieler = new Spieler(name, clientID, staerke, geschicklichkeit, intelligenz, stufe, erfahrung, skillPunkte, aktuellesLevel, aktuelleKarte, anzahlTraenke);
        } catch(SQLException e) {
            System.out.println(e.getMessage());

            // Falls Spieler aus irgendeinem Grund nicht in Datenbank sein sollte setze ihn auf Null
            rueckgabeSpieler = null;
        }
        return rueckgabeSpieler;
    }
    public boolean getSpieler(String spieler) {
        String sql = "SELECT spielername FROM Spielerdaten";

        boolean gefunden = false;

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while(rs.next() && gefunden == false) {
                String spielername = rs.getString("spielername");

                if(spielername.equals(spieler)) {
                    gefunden = true;
                }
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

        return gefunden;
    }

    public boolean getSpielerPasswort(String spieler, String pw) {
        String sql = "SELECT spielername, passwort FROM Spielerdaten";

        boolean gefunden = false;

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while(rs.next() && gefunden == false) {
                String spielername = rs.getString("spielername");
                String passwort = rs.getString("passwort");
                if(spielername.equals(spieler) && passwort.equals(pw)) {
                    gefunden = true;
                }
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

        return gefunden;
    }
}