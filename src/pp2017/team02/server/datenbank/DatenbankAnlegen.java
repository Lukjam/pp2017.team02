package pp2017.team02.server.datenbank;

import java.sql.*;

/**
 * Erzeugt eine Datenbank
 * Wenn Datenbank bereits vorhanden ist, wird diese Klasse nicht mehr benoetigt!
 *
 * @author http://www.sqlitetutorial.net/sqlite-java/select/
 * @author <Mack, Jonas, 5975514>
 *
 */

public class DatenbankAnlegen {

    public static void main(String[] args) {
        createNewDatabase("Datenbank.db");
        createNewTable("Datenbank.db");
    }


    public static void createNewDatabase(String fileName) {

        String url = "jdbc:sqlite:" + fileName;

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createNewTable(String fileName) {
        // SQLite connection string
        String url = "jdbc:sqlite:" + fileName;

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Spielerdaten ("
                + " clientID integer NOT NULL,"
                + "	spielername text PRIMARY KEY,"
                + "	passwort text NOT NULL,"
                + "	level integer NOT NULL,"
                + " intelligenz integer NOT NULL,"
                + " staerke integer NOT NULL,"
                + " geschicklichkeit integer NOT NULL,"
                + " erfahrung integer NOT NULL,"
                + " stufe integer NOT NULL,"
                + " anzahlTraenke integer NOT NULL,"
                + " skillPunkte integer NOT NULL"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }





}
