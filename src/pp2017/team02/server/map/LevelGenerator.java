package pp2017.team02.server.map;

import pp2017.team02.shared.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

/**
 * LevelGenerator erzeugt mithilfe des FloodFill-Algorithmus zufaellig die zu einem Level gehoerenden Raeume. Die Tueren,
 * Monster, Traenke, Ein- & Ausgang werden zufaellig in den Raeumen platziert.
 *
 * @author <Mack, Jonas, 5975514>
 *
 */


public class LevelGenerator {

    /**
     * 9 Konstanten, welche den Typ eines SpielfeldElements repraesentieren
     *
     * @author <Mack, Jonas, 5975514>
     *
     */
    // Typ
    public static final int RAND = 0;
    public static final int WAND = 1;
    public static final int BODEN = 2;
    public static final int TUER = 3; //Tuer in einen Nachbarraum
    public static final int EINGANG = 4; //Leiter aus vorherigem Level
    public static final int AUSGANG = 5; //Leiter ins naechste Level

    public final int GROESSE = 35; // Groesse der Karten/Raeume

    public Level level;

    private Random r;


    /**
     * Methode, die bei Aufruf mit dem Uebergabeparameter level das entsprechende Level erzeugt. Ein Level setzt
     * sich dabei aus 4 Raeumen (Karten) zusammen, in welchen jedes Feld zunaechst als Wand gesetzt wird. Daraufhin
     * erzeugt der FloodFill-Algorithmus ein Labyrinth, es werden Raeume, Luecken, Tueren, Ein- & Ausgang für den
     * Eingang ins Level sowie für den Ausgang (=Eintritt ins naechste Level) gesetzt.
     * Schliesslich werden in die begehbaren Felder Monster und Traenke zufaellig verteilt.
     *
     * @author <Mack, Jonas, 5975514>
     */

    public Level erzeugeLevel(int levelnr){

        level = new Level(levelnr);

        // Initialisiere level mit Rand und Wand
        initialisiereLevel(GROESSE);

        // Erzeuge Labyrinth
        for(int i=0; i<=3; i++) {
            r = new Random(levelnr*10+i);
            floodfill(r.nextInt((GROESSE-1)/2)*2+1,r.nextInt((GROESSE-1)/2)*2+1, level.getKarten().get(i));
            erzeugeLuecken(2, level.getKarten().get(i)); // 6
        }

        // Erzeuge in den Räumen 3 und 4 Flaechen aus Boden
        r = new Random(levelnr*10+2);
        erzeugeFlaechen(8,3,6,level.getKarten().get(2)); //8,3,6
        r = new Random(levelnr*10+3);
        erzeugeFlaechen(4,5,8,level.getKarten().get(3)); //4,5,8

        // Setze in jeden Raum zwei Tueren, in jeweils einen anderen Raum
        r = new Random(levelnr*10);
        setzeTueren(level.getKarten().get(0), level.getKarten().get(1), level.getKarten().get(2));
        r = new Random(levelnr*10+1);
        setzeTueren(level.getKarten().get(1), level.getKarten().get(0), level.getKarten().get(3));
        r = new Random(levelnr*10+2);
        setzeTueren(level.getKarten().get(2), level.getKarten().get(3), level.getKarten().get(0));
        r = new Random(levelnr*10+3);
        setzeTueren(level.getKarten().get(3), level.getKarten().get(2), level.getKarten().get(1));

        r = new Random(levelnr);
        //Entscheide zufaellig in welchen Raum der Eingang gesetzt wird
        int eingang = getZufallszahl(0,3);

        // Entscheide zufaellig in welchen Raum der Augang gesetzt wird
        int ausgang = eingang;
        while(ausgang==eingang) {
            ausgang = getZufallszahl(0,3);
        }

        setzeEingang(level.getKarten().get(eingang));
        level.getEingang().setKartenNummer(eingang);

        setzeAusgang(level.getKarten().get(ausgang));
        level.getAusgang().setKartenNummer(ausgang);

        // Erzeuge in den 4 Raeumen Monster und Traenke
        for(int i=0; i<4; i++) {
            r = new Random(levelnr*10+i);
            erzeugeMonster((int) (2*Math.log(levelnr)+5), level, i);
            erzeugeTraenke((int) (2*Math.log(levelnr)+5), level, i);
        }

        return level;
    }


    /**
     * Methode, welche die 4 Raeume eines Levels erstellt und alle Felder eines Raumes als Wand initialisiert
     *
     * @author <Mack, Jonas, 5975514>
     *
     */

    public void initialisiereLevel(int groesse) {

        Karte karte0 = new Karte(groesse);
        Karte karte1 = new Karte(groesse);
        Karte karte2 = new Karte(groesse);
        Karte karte3 = new Karte(groesse);

        // Setze die Randfelder auf Rand
        for(int i=0; i<groesse; i++) {
            karte0.setEintrag(0,i, new Rand());
            karte1.setEintrag(0,i, new Rand());
            karte2.setEintrag(0,i, new Rand());
            karte3.setEintrag(0,i, new Rand());
        }
        for(int i=0; i<groesse; i++) {
            karte0.setEintrag(i, 0, new Rand());
            karte1.setEintrag(i, 0, new Rand());
            karte2.setEintrag(i, 0, new Rand());
            karte3.setEintrag(i, 0, new Rand());
        }
        for(int i=0; i<groesse; i++) {
            karte0.setEintrag(groesse-1,i, new Rand());
            karte1.setEintrag(groesse-1,i, new Rand());
            karte2.setEintrag(groesse-1,i, new Rand());
            karte3.setEintrag(groesse-1,i, new Rand());
        }
        for(int i=0; i<groesse; i++) {
            karte0.setEintrag(i,groesse-1,new Rand());
            karte1.setEintrag(i,groesse-1,new Rand());
            karte2.setEintrag(i,groesse-1,new Rand());
            karte3.setEintrag(i,groesse-1,new Rand());
        }

        // Setze alle inneren Felder aller Karten auf Wand
        for (int x=1; x<groesse-1; x++) {
            for (int y=1; y<groesse-1; y++) {
                karte0.setEintrag(x,y,new Wand());
                karte1.setEintrag(x,y,new Wand());
                karte2.setEintrag(x,y,new Wand());
                karte3.setEintrag(x,y,new Wand());
            }
        }

        // Fuege Karten zum Level hinzu
        level.addKarte(0, karte0);
        level.addKarte(1, karte1);
        level.addKarte(2, karte2);
        level.addKarte(3, karte3);
    }





    /**
     * Baut in den initialisierten Karten zufällig erzeugte Labyrinthe
     *
     * @author <Mack, Jonas, 5975514>
     *
     */
    public void floodfill(int x, int y, Karte karte) {

        // Startpunkt des Algorithmus ist Boden
        karte.setEintrag(x, y, new Boden());

        // Waehle eine zufaellige Richtung
        Integer[] Richtungen = {0,1,2,3};
        Collections.shuffle(Arrays.asList(Richtungen), r);

        for (int Richtung : Richtungen) {

            // Richtung = 0 entspricht links
            if(Richtung == 0 && (x-2)>0 && karte.getEintrag(x-1,y).getTyp()==WAND && karte.getEintrag(x-2,y).getTyp()==WAND ) {
                karte.setEintrag(x-1,y,new Boden());
                floodfill(x-2,y,karte );
            }
            // Richtung = 1 entspricht unten
            else if(Richtung == 1 && (y+2)<GROESSE && karte.getEintrag(x,y+1).getTyp()==WAND && karte.getEintrag(x,y+2).getTyp()==WAND ) {
                karte.setEintrag(x,y+1,new Boden());
                floodfill(x,y+2,karte );
            }
            // Richtung = 2 entspricht rechts
            else if(Richtung == 2 && (x+2)<GROESSE && karte.getEintrag(x+1,y).getTyp()==WAND && karte.getEintrag(x+2,y).getTyp()==WAND ) {
                karte.setEintrag(x+1,y,new Boden());
                floodfill(x+2,y,karte );
            }
            // Richtung = 3 entspricht oben
            else if(Richtung == 3 && (y-2)>0 && karte.getEintrag(x,y-1).getTyp()==WAND && karte.getEintrag(x,y-2).getTyp()==WAND ) {
                karte.setEintrag(x,y-1,new Boden());
                floodfill(x,y-2,karte );
            }
        }
    }

    /**
     *
     * Methode, welche freie Flaechen (Rechtecke) in die Labyrinthe schlaegt
     *
     * @author <Mack, Jonas, 5975514>
     */

    public void erzeugeFlaechen(int anzahl, int groesseMin, int groesseMax, Karte karte) {

        // Laenge, Breite sowie untere Ecke der zu erzeugenden Flaeche (werden zufaellig gezogen)
        int laenge;
        int breite;
        int x;
        int y;

        // Pruefe ob groesseMax zulaessig
        if (groesseMax<GROESSE-2) {

            // Schleife, die anzahl viele Flaechen erzeugt
            for(int k=1; k<=anzahl;k++) {
                laenge = getZufallszahl(groesseMin, groesseMax);
                breite = getZufallszahl(groesseMin, groesseMax);
                x = getZufallszahl(1,GROESSE-2-breite);
                y = getZufallszahl(1,GROESSE-2-laenge);

                for (int i=y; i<=laenge+y-1;i++) {
                    for (int j=x; j<=breite+x-1;j++) {
                        karte.setEintrag(i,j,new Boden());
                    }
                }
            }
        }

    }


    /**
     * Erzeugt in einem vorgegebenem Labyrinth eine vorgegebene Anzahl an Luecken, um im Labyrinth neue Wege zu erzeugen
     *
     * @author <Mack, Jonas, 5975514>
     */


    public void erzeugeLuecken(int anzahl, Karte karte) {

        boolean lueckeErzeugt = false;

        for(int i=1; i<=anzahl;i++) {

            while(lueckeErzeugt==false) {
                int x = getZufallszahl(1,GROESSE-2);
                int y = getZufallszahl(1,GROESSE-2);
                // Prueft, ob Feld drueber und drunter oder links und rechts auch Boden sind
                if (!karte.getEintrag(x,y).isBegehbar()
                        && !karte.getEintrag(x+1,y).isBegehbar()
                        && !karte.getEintrag(x-1,y).isBegehbar()
                        && karte.getEintrag(x,y+1).getTyp()==BODEN
                        && karte.getEintrag(x,y-1).getTyp()==BODEN
                        ) {
                    karte.setEintrag(x,y,new Boden());
                    lueckeErzeugt = true;
                } else if (!karte.getEintrag(x,y).isBegehbar()
                        && !karte.getEintrag(x,y+1).isBegehbar()
                        && !karte.getEintrag(x,y-1).isBegehbar()
                        && karte.getEintrag(x+1,y).getTyp()==BODEN
                        && karte.getEintrag(x-1,y).getTyp()==BODEN
                        ) {
                    karte.setEintrag(x,y,new Boden());
                    lueckeErzeugt = true;
                }
            }

            lueckeErzeugt = false;
        }
    }


    /**
     * Setzt 2 Tueren in den uebergebenen Raum. Dabei wird eine Tuer nur an den Rand der Karte
     * gesetzt und es wird darauf geachtet, dass nicht beide Tueren an gleicher Stelle sind. Wo
     * eine Tuer am Rand gesetzt wird, wird zufaellig gezogen
     *
     * @author <Mack, Jonas, 5975514>
     */


    public void setzeTueren(Karte karte, Karte nachbar1, Karte nachbar2) {

        int anzahl = 4*GROESSE-5; //Anzahl der Randfelder
        int x;
        int y;

        //Setze Tuer 1:
        int randfeld1 = getZufallszahl(0, anzahl);
        // Ordne der Zufallszahl ein Element vom Rand zu
        if (randfeld1<=GROESSE-1) {
            x=0;
            y=randfeld1;
        } else if (randfeld1<=2*GROESSE-3) {
            x=randfeld1-GROESSE+1;
            y=GROESSE-1;
        } else if(randfeld1<=3*GROESSE-3) {
            x=GROESSE-1;
            y=randfeld1-2*GROESSE+2;
        } else {
            x=randfeld1-3*GROESSE+3;
            y=0;
        }
        karte.setEintrag(x,y,new Tuer(nachbar1, x, y));
        karte.setTuer1(new Tuer(nachbar1, x, y));

        //Setze Tuer 2:
        int randfeld2;
        // Stelle sicher, dass sich nicht beide Türen an gleicher Stelle befinden
        do{
            randfeld2 = getZufallszahl(0,anzahl);
        } while (randfeld1==randfeld2);
        // Ordne der Zufallszahl ein Element vom Rand zu
        if (randfeld2<=GROESSE-1) {
            x=0;
            y=randfeld2;
        } else if (randfeld2<=2*GROESSE-3) {
            x=randfeld2-GROESSE+1;
            y=GROESSE-1;
        } else if(randfeld2<=3*GROESSE-3) {
            x=GROESSE-1;
            y=randfeld2-2*GROESSE+2;
        } else {
            x=randfeld2-3*GROESSE+3;
            y=0;
        }
        karte.setEintrag(x,y,new Tuer(nachbar2,x,y));
        karte.setTuer2(new Tuer(nachbar2,x,y));

    }


    /**
     * Setzt in die uebergebene Karte einen Eingang (Startpunkt) fuer Spieler
     *
     * @author <Mack, Jonas, 5975514>
     */

    public void setzeEingang(Karte karte) {

        boolean gesetzt = false;
        while(gesetzt == false) {
            int x = getZufallszahl(1,GROESSE-2);
            int y = getZufallszahl(1,GROESSE-2);
            if(karte.getEintrag(x,y).getTyp()==BODEN && zaehleNachbarn(x,y,karte)==1) {
                // Markiere in Objekt Level, wo der Eingang gesetzt wurde
                level.setEingang(new Eingang(karte,x,y) );
                karte.setEintrag(x,y,new Eingang(karte,x,y));
                gesetzt=true;
            }
        }
    }


    /**
     * Setzt in die uebergebene Karte einen Ausgang (= Eingang ins naechste Level) fuer Spieler
     *
     * @author <Mack, Jonas, 5975514>
     */

    public void setzeAusgang(Karte karte) {

        boolean gesetzt = false;
        while(gesetzt == false) {
            int x = getZufallszahl(1,GROESSE-2);
            int y = getZufallszahl(1,GROESSE-2);
            if(karte.getEintrag(x,y).getTyp()==BODEN && zaehleNachbarn(x,y,karte)==1) {
                level.setAusgang(new Ausgang(karte,x,y));
                karte.setEintrag(x,y,new Ausgang(karte, x,y));
                gesetzt=true;
            }
        }
    }


    /**
     * Erzeugt in einer vorgegebenen Karte eine vorgegebene Anzahl an sichtbaren und unsichtbaren Monstern und ordnet einem Monster
     * einen Schlüssel zu
     *
     * @author <Mack, Jonas, 5975514>
     */

    public void erzeugeMonster(int anzahl, Level level, int i) {

        LinkedList<Monster> monsterListe = new LinkedList<Monster>();
        Random rnd = new Random();


        boolean gesetzt = false;
        int[][] karte = level.getKarten().get(i).getIntKarte();

        // Monster, die vor Aufnahme des Schluessels sichtbar sind
        for(int j=0; j<=anzahl-1; j++) {
            while(gesetzt==false) {
                int x = getZufallszahl(1, GROESSE-2);
                int y = getZufallszahl(1, GROESSE-2);
                if(level.getKarten().get(i).getEintrag(y,x).getTyp()==BODEN) {
                    int tndInt = rnd.nextInt(2);
                    if (tndInt == 0){
                        monsterListe.add(j, (Monster) new Drache(x,y, karte, level.getLevelnr(), false));
                    }else{
                        monsterListe.add(j, (Monster) new Statue(x,y, karte, level.getLevelnr(), false));
                    }

                    gesetzt = true;
                }
            }
            gesetzt = false;
        }

        // Weise einem Monster zufaellig den Schluessel zu
        int z=getZufallszahl(0,anzahl-1);
        monsterListe.get(z).setEnthaeltSchluessel(true);


        level.getKarten().get(i).setMonsterListe(monsterListe);
    }


    /**
     * erzeugt in einer vorgegegeben Karte eine vorgegebene Anzahl an Traenken
     *
     * @author <Mack, Jonas, 5975514>
     */

    public void erzeugeTraenke(int anzahl, Level level, int i) {

        LinkedList<Trank> trankListe = new LinkedList<Trank>();

        boolean gesetzt = false;

        for(int j=0; j<=anzahl-1; j++) {
            while(gesetzt==false) {
                int x = getZufallszahl(1, GROESSE-2);
                int y = getZufallszahl(1, GROESSE-2);
                if(level.getKarten().get(i).getEintrag(y,x).getTyp()==BODEN) {
                    trankListe.add(j, new Trank(x,y));
                    gesetzt = true;
                }
            }
            gesetzt = false;
        }

        level.getKarten().get(i).setTrankListe(trankListe);
    }



    /**
     * Erzeugt eine ganzzahlige Zufallszahl zwischen min und max (inklusive)
     *
     * @author Jonas Mack, 5975514
     */

    public int getZufallszahl(int min, int max) {
        int zahl = ((r.nextInt(max-min+1)) + min);
        return zahl;
    }


    /**
     * Zaehle die Anzahl der begehbaren Felder links, rechts, oben und unten des uebergebenen Feldes
     *
     * @author Jonas Mack, 5975514
     */

    public int zaehleNachbarn(int x, int y, Karte karte) {

        int anzahl = 0;

        if(karte.getEintrag(x+1,y).isBegehbar()){
            anzahl++;
        }
        if(karte.getEintrag(x-1,y).isBegehbar()){
            anzahl++;
        }
        if(karte.getEintrag(x,y+1).isBegehbar()){
            anzahl++;
        }
        if(karte.getEintrag(x,y-1).isBegehbar()){
            anzahl++;
        }

        return anzahl;
    }






}
