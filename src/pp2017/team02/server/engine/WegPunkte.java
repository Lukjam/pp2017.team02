package pp2017.team02.server.engine;
// TODO sehr wahrscheinlich in packet shared einbauen.

import java.io.Serializable;

/**
 * Klasse um die Knoten des A-Stern Algorithmus zu erstellen.
 * @author Kriehn, Lorenz, 7311063
 */
public class WegPunkte extends WegUeber implements Comparable<WegPunkte>{
    private int xPos;
    private int yPos;
    private int objekt;
    private double fWert;
    private int gWert;
    private int[] vorgaenger = new int[2]; // Koordinate des Vorgängers.


    /**
     * Konstruktor der Klasse
     * @author Kriehn, Lorenz, 7311063
     */
    public WegPunkte(int xPos, int yPos, int objekt){
        setxPos(xPos);
        setyPos(yPos);
        setObjekt(objekt);


    }

    public void setVorgaenger(int xPos, int yPos){
        vorgaenger[0] = xPos;
        vorgaenger[1] = yPos;
    }



    public int[] getVorgaenger() {
        return vorgaenger;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int getObjekt() {
        return objekt;
    }

    public void setObjekt(int objekt) {
        this.objekt = objekt;
    }

    public int getgWert() {
        return gWert;
    }

    public void setgWert(int gWert) {
        this.gWert = gWert;
    }

    public double getfWert() {
        return fWert;
    }

    public void setfWert(double fWert) {
        this.fWert = fWert;
    }


    /** Implementiert die natuerliche Ordnung nach dem fWert fuer WegPunkte.
     * Wird fuer die Prioritaetswarteschlange benötigt.
     * @author Kriehn, Lorenz, 7311063
    */
    @Override
    public int compareTo(WegPunkte wegPunkt) {
        return (int) (this.getfWert() - wegPunkt.getfWert());
    }
}
