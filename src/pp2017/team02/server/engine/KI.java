package pp2017.team02.server.engine;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Set;

/** Beinhaltet die KI in form von dem A*-Algorithmus. Eigene Implementierung.
 * @author Kriehn, Lorenz, 7311063
 */

public class KI implements Serializable{
    private static final long serialVersionUID = 1L;
    private WegPunkte[][] karte;
    private PriorityQueue<WegPunkte> offeneWegPunkte;
    private Set<WegPunkte> geschlosseneWegPunkte;
    private WegPunkte aktuellerWegPunkt;
    private WegPunkte zielPunkt;
    private LinkedList<WegPunkte> vorgaenger;
    private WegPunkte startPunkt;

    /**
     * Konstruktor um die Klasse auzurufen.
     * @author Kriehn, Lorenz, 7311063
     */
    public KI(int[][] inputkarte){
        karte = verarbeiteKarte(inputkarte);
    }

    /**
     * Verarbeitet die eingegeben Karte
     * @author Kriehn, Lorenz, 7311063
     */
    public WegPunkte[][] verarbeiteKarte(int[][] inputKarte){
        WegPunkte[][] tempKarte = new WegPunkte[inputKarte.length][inputKarte[0].length];
        for(int i=0; i<tempKarte.length;i++){
            for(int j=0; j<tempKarte[0].length; j++){

                tempKarte[i][j] = erstelleWegPunkte(i,j,inputKarte[i][j]); // war i j
            }
        }
        return tempKarte;
    }


    // Erstellt eine Karte zum Testen, nur 1 und 2 als Wert.
    /* Ausgeklammert, da verarbeiteKarte(int[][]) den Job übernimmt und die Methode auch nur zum Testen benutzt wird.
    public WegPunkte[][] testKarte(){
        WegPunkte[][] tempKarte = new WegPunkte[2000][2000];
        for(int i=0; i<tempKarte.length;i++){
            for(int j=0; j<tempKarte[0].length; j++){

                tempKarte[i][j] = erstelleWegPunkte(i,j,2);
            }
        }
        // Testkarte mit Hindernissen.
        tempKarte[5][0].setObjekt(1);
        tempKarte[5][1].setObjekt(1);
        tempKarte[4][1].setObjekt(1);
        for(int k=0; k<40;k++){
            tempKarte[k][0].setObjekt(1);
        }

        for(int k=0; k<40;k++){
            tempKarte[42][k].setObjekt(1);
        }
        return tempKarte;
    }
    */

    /** Erstellt eine LinkedList mit allen Nachbarn. Achtet dabei auch auf Raender der Karte.
     * @author Kriehn, Lorenz, 7311063
     */
    public LinkedList<WegPunkte> alleNachbarn(WegPunkte wegPunkt){
        LinkedList<WegPunkte> listePunkte = new LinkedList<WegPunkte>();

        // Testet ob ueber den Rand gelaufen wurde.
        if(wegPunkt.getyPos() -1 >= 0) {
            // Nimmt oberen Knoten hinzu.
            if (istBegehbar(karte[wegPunkt.getxPos()][wegPunkt.getyPos() - 1].getObjekt())) {
                listePunkte.add(karte[wegPunkt.getxPos()][wegPunkt.getyPos() -1]);

            }

        }
        // Testet ob rechts über den Rand gelaufen wurde.
        if(wegPunkt.getxPos() +1 < karte.length){
            // Nimmt den rechten Knoten hinzu.
            if (istBegehbar(karte[wegPunkt.getxPos()+1][wegPunkt.getyPos()].getObjekt())){
                listePunkte.add(karte[wegPunkt.getxPos()+1][wegPunkt.getyPos()]);
            }
        }

        // Testet unteren Knoten
        if(wegPunkt.getyPos() +1 < karte[0].length){
            // Nimmt unteren Knoten hinzu.
            if (istBegehbar(karte[wegPunkt.getxPos()][wegPunkt.getyPos() +1].getObjekt())){
                listePunkte.add(karte[wegPunkt.getxPos()][wegPunkt.getyPos() +1]);
            }
        }

        // Testet links
        if (wegPunkt.getxPos() -1 >= 0){
            // Nimmt den linken Knoten hinzu.
            if (istBegehbar(karte[wegPunkt.getxPos() -1][wegPunkt.getyPos()].getObjekt())){
                listePunkte.add(karte[wegPunkt.getxPos() -1][wegPunkt.getyPos()]);
            }
        }
        return listePunkte;
    }

    /** Erstellt den endgueltigen Weg der dann ausgegeben wird.
     * @author Kriehn, Lorenz, 7311063
     */
    public LinkedList<WegPunkte> baueVorgaenger(WegPunkte zielPunkt){
        vorgaenger = new LinkedList<WegPunkte>();
        int i=0;
        WegPunkte tempVorgaenger = zielPunkt;
        vorgaenger.add(zielPunkt);
        while (!((tempVorgaenger.getVorgaenger()[0] == startPunkt.getxPos()) && (tempVorgaenger.getVorgaenger()[1] == startPunkt.getyPos())))
        {
            vorgaenger.add(karte[tempVorgaenger.getVorgaenger()[0]][tempVorgaenger.getVorgaenger()[1]]);
            tempVorgaenger = karte[tempVorgaenger.getVorgaenger()[0]][tempVorgaenger.getVorgaenger()[1]];
            //System.out.println("Ich bin ein loop "+tempVorgaenger.getVorgaenger()[0]+" "+tempVorgaenger.getVorgaenger()[1]);

        }

        return vorgaenger;
    }

    /** Erstellt einen Knoten um die Karte besser verarbeiten zukoennen.
     * @author Kriehn, Lorenz, 7311063
     */
    public WegPunkte erstelleWegPunkte(int xPos, int yPos, int objekt){
        return new WegPunkte(xPos,yPos,objekt);
    }

    /** Berechnet den direkten Abstand zweier Knoten. Mit ((x2-x1)^2+(y2-y1)^2)^(1/2). Wichtig zum Abstandschätzen. (Euklidischer Abstand)
     * @author Kriehn, Lorenz, 7311063
     */
    public double berechneHWert(WegPunkte wegPunkte1, WegPunkte zielPunkt){
        return Math.sqrt(Math.pow((zielPunkt.getxPos()-wegPunkte1.getxPos()),2) + Math.pow((zielPunkt.getyPos()-wegPunkte1.getyPos()),2));
    }

    /** Fügt den aktualisierten Knoten wieder in die Karte ein.
     * @author Kriehn, Lorenz, 7311063
     */
    public void updateKarte(WegPunkte wegPunkte){
        karte[wegPunkte.getxPos()][wegPunkte.getyPos()] = wegPunkte;
    }

    /** Fragt ab ob ein Feld Begehbar ist. Also ob Feld == Boden.
     * @author Kriehn, Lorenz, 7311063
     */
    public boolean istBegehbar(int feld){
        //Kann später erweitert werden, falls andere Flaechen begehbar sein sollen.
        if (!(feld == 0 || feld == 1 )) return true;
        return false;
    }

    /** Berechnet den kuerzesten Weg mit Hilfe des A*-Algorithmus. Die Rueckgabe erfolgt in einer LinkedList<Wegpunkte>
     * @author Kriehn, Lorenz, 7311063
     */

    public LinkedList<WegPunkte> bewegeVonZu(int xStart, int yStart, int xEnde, int yEnde){
        for (int i = 0;i<karte.length;i++){
            for (int j = 0;j<karte[0].length;j++){
                karte[j][i].setgWert(0);
                karte[j][i].setfWert(0);
            }
        }
        //karte = testKarte();
        //karte = karteinput;
        zielPunkt = karte[xEnde][yEnde];
        startPunkt = karte[xStart][yStart];
        geschlosseneWegPunkte = new HashSet<WegPunkte>();


        // Erstellt PriorityQueue und fuegt den Startknoten hinzu.
        offeneWegPunkte= new PriorityQueue<WegPunkte>();
        offeneWegPunkte.add(karte[xStart][yStart]);

        do{
            aktuellerWegPunkt = offeneWegPunkte.poll();
            if (aktuellerWegPunkt == zielPunkt){
                return baueVorgaenger(zielPunkt);
            }

            geschlosseneWegPunkte.add(aktuellerWegPunkt);
            erweiterSet(aktuellerWegPunkt);
        }while(!offeneWegPunkte.isEmpty());
        return new LinkedList<WegPunkte>();

    }

    /**
     * Implementier die Methode erweiterSet aus dem A* Algorithmus. Geht alle Nachbarn des Knotens durch.
     * @author Kriehn, Lorenz, 7311063
     */
    public void erweiterSet(WegPunkte wegPunkt){
        LinkedList<WegPunkte> tempNachbarn;
        WegPunkte tempKnoten;
        int tempGWert;
        double tempFWert;
        tempNachbarn = alleNachbarn(wegPunkt);
        while(!tempNachbarn.isEmpty()){
            tempKnoten = tempNachbarn.pop();
            // Prueft ob der Knoten im geschlossenen Set ist.

            if (geschlosseneWegPunkte.contains(tempKnoten)){

                continue;
            }

            tempGWert = wegPunkt.getgWert() + 1; // TODO gWert? Bei uns immer 1? oder Abhängig von untergrund. (Weiß monster welcher untergrund gut ist?)

            if (offeneWegPunkte.contains(tempKnoten) && tempGWert >= wegPunkt.getgWert() ){
                continue;
            }
            tempKnoten.setVorgaenger(wegPunkt.getxPos(),wegPunkt.getyPos());  // Setzt den Vorgaengerzeiger auf den Vorgaenger.
            tempKnoten.setgWert(tempGWert);
            tempFWert = tempGWert + berechneHWert(wegPunkt,zielPunkt);

            if (offeneWegPunkte.contains(tempKnoten)){
                tempKnoten.setfWert(tempFWert);
                updateKarte(tempKnoten);
            }else {
                tempKnoten.setfWert(tempFWert);
                offeneWegPunkte.add(tempKnoten);
                updateKarte(tempKnoten);
            }
        }
    }

    /**
     * Getter fuer die Karte.
     * @author Kriehn, Lorenz, 7311063
     */
    public WegPunkte[][] getKarte() {
        return karte;
    }

    /**
     * Setter fuer die Karte.
     * @author Kriehn, Lorenz, 7311063
     */
    public void setKarte(WegPunkte[][] karte) {
        this.karte = karte;
    }
}
