package pp2017.team02.server.engine;

import pp2017.team02.server.datenbank.DatenbankVerwalten;
import pp2017.team02.server.map.LevelGenerator;
import pp2017.team02.shared.*;

import java.util.LinkedList;

public class SWV {
    private LinkedList<Spieler> sf;
    private LinkedList<ChatNachricht> chat;
    private LinkedList<int[]> clientSessionIDComm;
    private LinkedList<Object> inqueue;
    public LinkedList<Object> outqueue;
    private DatenbankVerwalten db;
    private LevelGenerator levelGenerator;
    private LinkedList<String[]> highscore;
    private Object o;

    /**
     * Konstruktor der Spielweltverwaltung
     *
     * @author Jamann, Lukas 7309791
     */
    public SWV() {
        db = new DatenbankVerwalten();
        sf = new LinkedList<Spieler>();
        chat = new LinkedList<ChatNachricht>();
        clientSessionIDComm = new LinkedList<int[]>();
        inqueue = new LinkedList<>();
        outqueue = new LinkedList<>();
        levelGenerator = new LevelGenerator();
        highscore = new LinkedList<>();
        o = null;
    }

    /**
     * Methode, damit die Inqueue bzw. das Postfach für die Comm bearbeitet wird.
     * Sie wird so lange aufgerufen, bis die Inqueue leer ist.
     *
     * @author Jamann, Lukas 7309791
     */
    public void behandelNachricht() {
        while (!inqueue.isEmpty()) {
            recive(inqueue.getFirst());
            inqueue.removeFirst();
        }

    }

    /**
     * Verteilung der Verschieden Objekte in die handle Klassen
     *
     * @author Jamann, Lukas 7309791
     */
    public void recive(Object o) {
        if (o.getClass() == ChatNachricht.class) {
            outqueue.add(handleNachricht((ChatNachricht) o));
        } else if (o.getClass() == LoginNachricht.class) {
            handleLogin((LoginNachricht) o);
        } else if (o.getClass() == LogoutNachricht.class) {
            handleLogout((LogoutNachricht) o);
        } else if (o.getClass() == KampfNachricht.class) {
            handleKampf((KampfNachricht) o);
        } else if (o.getClass() == LaufNachricht.class) {
            handleLaufNachricht((LaufNachricht) o);
        } else if (o.getClass() == LevelNachricht.class) {
            handleLevel((LevelNachricht) o);
        } else if (o.getClass() == SpielerNachricht.class) {
            handleSpieler((SpielerNachricht) o);
        } else if (o.getClass() == ItemNachricht.class) {
            handleItem((ItemNachricht) o);
        } else if (o.getClass() == ServerNachricht.class) {
            handleServer((ServerNachricht) o);
        }
    }

    /**
     * Hier wird ein Verbindungsabbruch eines Clients abgehandelt.
     * Er wird dann aus den entsprechenden Listen geschrieben.
     * Die ServerNachricht ist nur für die Kommunikation zwischen Comm und SWV da.
     *
     * @author Jamann, Lukas 7309791
     */
    private void handleServer(ServerNachricht o) {
        int clientIDindex = searchclientID(o.getClientIDComm());
        if (clientIDindex != -1) {
            int spielerindex = searchSpieler(clientSessionIDComm.get(clientIDindex)[2]);
            if (spielerindex != -1) {
                db.Logout(sf.get(spielerindex));
                String name = sf.get(spielerindex).getName();
                int level = sf.get(spielerindex).getAktuellesLevel().getLevelnr();
                sf.remove(spielerindex);
                int idcp = searchclientID(o.getClientIDComm());
                clientSessionIDComm.remove(idcp);
                String[] tmp = new String[]{name, Integer.toString(level)};
                highscore.add(tmp);
                sortiereHighscore();
                outqueue.add(new HighscoreNachricht(-7, highscore));
                outqueue.add(new LogoutNachricht(-7, name));

            }
        }
    }

    /**
     * HIer wird eine ItemNAchricht abgehandelt und dem entsprechend Verarbeitet.
     *
     * @author Jamann, Lukas 7309791
     */
    private void handleItem(ItemNachricht itemNachricht) {
        int spielerindex = searchSpieler(itemNachricht.getClientID());
        if (spielerindex != -1) {
            if (itemNachricht.isSchluessel()) {
                sf.get(spielerindex).oeffneTuer();
                LinkedList<Integer> tmp = sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).getClientIDs();
                if (tmp.size() > 1) {
                    for (int i = 0; i < tmp.size(); i++) {
                        if (tmp.get(i) != itemNachricht.getClientID()) {
                            outqueue.add(new ItemNachricht(tmp.get(i), clientSessionIDComm.get(searchclientIDComm(tmp.get(i)))[0], sf.get(spielerindex).getAktuellesLevel()));
                        }
                    }
                }
            } else {
                sf.get(spielerindex).setInventar(itemNachricht.getInventar());
                sf.get(spielerindex).setAktuellesLevel(itemNachricht.getLevel());
                LinkedList<Integer> tmp = sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).getClientIDs();
                if (tmp.size() > 1) {
                    for (int i = 0; i < tmp.size(); i++) {
                        if (tmp.get(i) != itemNachricht.getClientID()) {
                            outqueue.add(new ItemNachricht(tmp.get(i), clientSessionIDComm.get(searchclientIDComm(tmp.get(i)))[0], sf.get(spielerindex).getAktuellesLevel()));
                        }
                    }
                }
            }
        }
    }

    /**
     * Eine Chat NAchricht wird abgerufen und überprüf, ob diese ein Cheat ist, oder ob es eine ChatNachricht ist.
     * Es wird dementsprechend gehandelt.
     *
     * @author Jamann, Lukas 7309791
     */
    public Object handleNachricht(ChatNachricht cn) {
        String tmp = cn.getText();
        if (tmp.startsWith("/")) {
            int cheat = searchSpieler(cn.getClientID());
            if (tmp.equals("/leben") && cheat != -1) {
                sf.get(cheat).setLeben(sf.get(cheat).getMaxLeben());
                CheatNachricht chatn = new CheatNachricht(cn.getClientID(), clientSessionIDComm.get(searchclientID(cn.getClientID()))[0], sf.get(cheat).getMaxLeben());
                return chatn;

            } else if (tmp.equals("/highscore")) {
                return new HighscoreNachricht(-7, highscore);
            } else if (tmp.equals("/schluessel")) {
                sf.get(cheat).setHatSchluessel(true);
                CheatNachricht chatn = new CheatNachricht(cn.getClientID(), clientSessionIDComm.get(searchclientID(cn.getClientID()))[0], true);
                return chatn;
            }
        }
        chat.add(cn);
        ChatNachricht chat = new ChatNachricht(-7, cn.getText());
        System.out.print(cn.getText());
        return chat;


    }

    /**
     * Die handleLogin Methode stellt die Verbindung zur Datenbank da. Dem Client wir nach entsprechender Prozedur ein
     * Spieler geschickt, welcher auch zugleich entweder in die Datenbank geschrieben oder aus der Datenbank heraus erstellt
     * wird.
     * Desweiteren werden die clientIDComm, welche für die Comms wichtig sind, mit den Unique clientIDs verbunden.
     */
    @SuppressWarnings("Duplicates")
    public void handleLogin(LoginNachricht li) {
        int[] tmpints = new int[3];
        if (!li.isLoginErfolgreich() && !li.isNeuesKonto() && !li.isPasswortFalsch()) {
            if (db.getSpieler(li.getBenutzerName())) {
                //Login erfolgreich, es gibt den Username
                if (db.getSpielerPasswort(li.getBenutzerName(), li.getPasswort())) {
                    //Login korrekt, Passwort und Benutzername stimmen überein.
                    Spieler tmp1 = db.Login(li.getBenutzerName(), li.getPasswort());
                    sf.add(tmp1);
                    LinkedList<Integer> tmplist = new LinkedList<Integer>();
                    tmplist.add(tmp1.getClientID());
                    for (int i = 0; i < sf.size() - 1; i++) {
                        if (sf.get(i).getAktuelleKarte() == sf.getLast().getAktuelleKarte() && tmp1.getClientID() != sf.get(i).getClientID()) {
                            tmplist.add(sf.get(i).getClientID());
                        }
                    }
                    for (int i = 0; i < tmplist.size(); i++) {
                        int spielerindex = searchSpieler(tmplist.get(i));
                        sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).setClientIDs(tmplist);
                    }
                    if (tmplist.size() != 1) {
                        Spieler tmpsp = sf.getLast();
                        int tmpspind = searchSpieler(tmpsp.getClientID());
                        for (int i = 0; i < tmplist.size(); i++) {
                            if (tmpsp.getClientID() != tmplist.get(i)) {
                                int tmpid = tmplist.get(i);
                                for (int j = 0; j < tmplist.size(); j++) {
                                    if (tmpid != tmplist.get(j)) {
                                        outqueue.add(new MitspielerNachricht(tmpid, clientSessionIDComm.get(searchclientIDComm(tmplist.get(j)))[0], new Spieler(tmplist.get(j), sf.get(searchSpieler(tmplist.get(j))).getName(), sf.get(searchSpieler(tmplist.get(j))).getxPos(), sf.get(searchSpieler(tmplist.get(j))).getyPos())));
                                    }
                                }
                            }
                        }
                    }
                    int tmppos = searchclientID(li.getClientIDComm());
                    if (tmppos == -1) {
                        tmpints[0] = li.getClientIDComm();
                        tmpints[1] = li.getSessionID();
                        tmpints[2] = sf.getLast().getClientID();
                        clientSessionIDComm.add(tmpints);
                    } else {
                        if (clientSessionIDComm.get(tmppos)[2] == -1) {
                            clientSessionIDComm.get(tmppos)[2] = sf.getLast().getClientID();
                        }
                    }
                    //
                    tmppos = searchclientID(li.getClientIDComm());
                    Spieler tmp = db.Login(li.getBenutzerName(), li.getPasswort());
                    outqueue.add(new LoginNachricht(tmp.getClientID(), li.getClientIDComm(), li.getSessionID(), li.getBenutzerName(), li.getPasswort(), tmp, true));
                    for (int j = 0; j < tmplist.size(); j++) {
                        if (tmp.getClientID() != tmplist.get(j)) {
                            outqueue.add(new MitspielerNachricht(tmp.getClientID(), clientSessionIDComm.get(searchclientIDComm(tmplist.get(j)))[0], new Spieler(tmplist.get(j), sf.get(searchSpieler(tmplist.get(j))).getName(), sf.get(searchSpieler(tmplist.get(j))).getxPos(), sf.get(searchSpieler(tmplist.get(j))).getyPos())));
                        }
                    }
                } else {
                    //Fall: Passwort Falsch
                    int tmppos = searchclientID(li.getClientIDComm());
                    if (tmppos == -1) {
                        tmpints[0] = li.getClientIDComm();
                        tmpints[1] = li.getSessionID();
                        tmpints[2] = -1;
                        clientSessionIDComm.add(tmpints);
                    }
                    outqueue.add(new LoginNachricht(-1, li.getClientIDComm(), li.getSessionID(), li.getBenutzerName(), li.getPasswort(), true));

                }
            } else {
                //Neues Konto soll angelegt werden mit den Benutzername und Passwort

                int tmppos = searchclientID(li.getClientIDComm());
                if (tmppos == -1) {
                    tmpints[0] = li.getClientIDComm();
                    tmpints[1] = li.getSessionID();
                    tmpints[2] = -1;
                    clientSessionIDComm.add(tmpints);
                }
                outqueue.add(new LoginNachricht(-1, li.getClientIDComm(), li.getSessionID(), li.getBenutzerName(), li.getPasswort(), true, true));
            }
        } else if (li.isNeuesKonto()) {
            //Neuer Spieler wird mit den Skills angelegt.
            if (!li.getSkill().isEmpty()) {

                int tmppos = searchclientID(li.getClientIDComm());
                Spieler tmp = db.SpielerAnlegen(li.getBenutzerName(), li.getPasswort(), li.getSkill().get(0), li.getSkill().get(1), li.getSkill().get(2));
                if (tmp != null) {
                    LinkedList<Integer> tmplist = new LinkedList<Integer>();
                    sf.add(tmp);
                    tmplist.add(tmp.getClientID());
                    for (int i = 0; i < sf.size(); i++) {
                        if (sf.get(i).getAktuelleKarte() == sf.get(searchSpieler(tmp.getClientID())).getAktuelleKarte() && tmp.getClientID() != sf.get(i).getClientID()) {
                            tmplist.add(sf.get(i).getClientID());
                        }
                    }
                    for (int i = 0; i < tmplist.size(); i++) {
                        int spielerindex = searchSpieler(tmplist.get(i));
                        sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).setClientIDs(tmplist);
                    }
                    if (tmplist.size() != 1) {
                        for (int i = 0; i < tmplist.size(); i++) {
                            if (tmp.getClientID() != tmplist.get(i)) {
                                int tmpid = tmplist.get(i);
                                outqueue.add(new MitspielerNachricht(tmpid, clientSessionIDComm.get(searchclientIDComm(tmpid))[0], new Spieler(tmp.getClientID(), tmp.getName(), tmp.getxPos(), tmp.getyPos())));
                            }
                        }
                    }
                    int spielerindex = searchSpieler(tmp.getClientID());
                    clientSessionIDComm.get(tmppos)[2] = tmp.getClientID();
                    o = new LoginNachricht(sf.get(spielerindex).getClientID(), li.getClientIDComm(), li.getSessionID(), li.getBenutzerName(), li.getPasswort(), tmp /*sf.get(spielerindex)*/, true);
                    outqueue.add(o);
                    for (int j = 0; j < tmplist.size(); j++) {
                        if (tmp.getClientID() != tmplist.get(j)) {
                            outqueue.add(new MitspielerNachricht(tmp.getClientID(), clientSessionIDComm.get(searchclientIDComm(tmp.getClientID()))[0], new Spieler(tmplist.get(j), sf.get(searchSpieler(tmplist.get(j))).getName(), sf.get(searchSpieler(tmplist.get(j))).getxPos(), sf.get(searchSpieler(tmplist.get(j))).getyPos())));
                        }
                    }
                }
            }

        }
    }

    /**
     * Diese Methode erfüllt den Zweck eines LogOuts, welches vom Client gewollt ist.
     *
     * @author Jamann, Lukas 7309791
     */
    private void handleLogout(LogoutNachricht logoutNachricht) {
        int spielerindex = searchSpieler(logoutNachricht.getClientID());
        if (spielerindex != -1) {
            db.Logout(sf.get(spielerindex));
            String name = sf.get(spielerindex).getName();
            int level = sf.get(spielerindex).getAktuellesLevel().getLevelnr();
            sf.remove(spielerindex);
            int idcp = searchclientIDComm(logoutNachricht.getClientID());
            clientSessionIDComm.remove(idcp);
            String[] tmp = new String[]{name, Integer.toString(level)};
            highscore.add(tmp);
            sortiereHighscore();
            outqueue.add(new HighscoreNachricht(-7, highscore));
            outqueue.add(new ChatNachricht(-7, name));
        }
    }

    /**
     * Dauerhafte Update-Methode für die Monster.
     * Der Fall für meherer Spieler in einer
     *
     * @author Jamann, Lukas 7309791
     */
    @SuppressWarnings("Duplicates")
    public void updateMonster() {
        LinkedList<Integer> abgehackt = new LinkedList<Integer>();

        if (!sf.isEmpty()) {
            for (int a = 0; a < sf.size(); a++) {
                int i = sf.get(a).getAktuelleKarte();
                LinkedList<int[]> sendLink = new LinkedList<int[]>();
                if (sf.get(a).getAktuellesLevel().getKarten().get(i).getClientIDs().size() == 1) {
                    //Fall für nur einen Client in der Karte.
                    LinkedList<Spieler> spielers = new LinkedList<Spieler>();
                    spielers.add(sf.get(a));
                    for (int j = 0; j < sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().size(); j++) {
                        int[] tmp;
                        tmp = sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).updateMonster(sf);
                        int temp = searchSpieler(sf.get(a).getClientID()); //searchSpieler(tmp[5]);
                        //Monster hat Spieler angegriffen
                        if (tmp[2] == 0 && tmp[3] == 1) {
                            if (temp != -1) {
                                sf.get(temp).changeLeben(-tmp[4]);
                                outqueue.add(new KampfNachricht(sf.get(temp).getClientID(), clientSessionIDComm.get(searchclientIDComm(sf.get(temp).getClientID()))[0], tmp[4]));
                            }

                            //Monster ist gestorben. Loot wird verteilt.
                        } else if (tmp[2] == 0 && tmp[3] == 0) {
                            sendLink.add(new int[]{sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getMonsterID(), tmp[0], tmp[1]});
                        }else if (tmp[7] == 4) {
                            sendLink.add(new int[]{sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getMonsterID(), tmp[0], tmp[1]});
                        } else if (tmp[2] == 1) {
                            if (temp != -1) {
                                Item trank = sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getBeute();
                                int xp = sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getXpBeute();
                                int mid = sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getMonsterID();
                                boolean hs = sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).isEnthaeltSchluessel();
                                //sf.get(a).getAktuellesLevel().getKarten().get(i).addTrank(trank);
                                sf.get(temp).setErfahrung(xp);
                                sf.get(temp).setHatSchluessel(hs);
                                outqueue.add(new KampfNachricht(sf.get(temp).getClientID(), clientSessionIDComm.get(searchclientIDComm(sf.get(temp).getClientID()))[0], mid, xp, trank, hs));
                                sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().remove(j);
                            }
                        }

                    }
                    outqueue.add(new UpdateNachricht(sf.get(a).getClientID(), clientSessionIDComm.get(searchclientIDComm(sf.get(a).getClientID()))[0], sendLink, sf.get(a).getAktuelleKarte()));

                } else {
                    //Fall für mehrer Spieler in einer Karte.
                    if (!abgehackt.contains(sf.get(a).getClientID())) {
                        abgehackt.add(sf.get(a).getClientID());

                        for (int j = 0; j < sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().size(); j++) {
                            int[] tmp;
                            tmp = sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).updateMonster(sf);
                            int temp = searchSpieler(sf.get(a).getClientID());
                            //Monster hat Spieler angegriffen
                            if (tmp[2] == 0 && tmp[3] == 1) {
                                if (temp != -1) {
                                    sf.get(temp).changeLeben(-tmp[4]);
                                    outqueue.add(new KampfNachricht(sf.get(temp).getClientID(), clientSessionIDComm.get(searchclientIDComm(sf.get(temp).getClientID()))[0], tmp[4]));
                                }
                                //Monster ist gestorben. Loot wird verteilt.
                            } else if (tmp[2] == 0 && tmp[3] == 0) {
                                sendLink.add(new int[]{sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getMonsterID(), tmp[0], tmp[1]});
                            }else if (tmp[7] == 4) {
                                sendLink.add(new int[]{sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getMonsterID(), tmp[0], tmp[1]});
                            } else if (tmp[2] == 1) {
                                if (temp != -1) {
                                    Item trank = (Trank) sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getBeute();
                                    int xp = sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getXpBeute();
                                    int mid = sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getMonsterID();
                                    boolean hs = sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).isEnthaeltSchluessel();
                                    //sf.get(a).getAktuellesLevel().getKarten().get(i).addTrank(trank);
                                    sf.get(temp).setErfahrung(xp);
                                    sf.get(temp).setHatSchluessel(hs);
                                    outqueue.add(new KampfNachricht(sf.get(temp).getClientID(), clientSessionIDComm.get(searchclientIDComm(sf.get(temp).getClientID()))[0], mid, xp, trank, hs));
                                    sf.get(a).getAktuellesLevel().getKarten().get(i).getMonsterListe().remove(j);
                                }
                            }
                        }
                        outqueue.add(new UpdateNachricht(sf.get(a).getClientID(), clientSessionIDComm.get(searchclientIDComm(sf.get(a).getClientID()))[0], sendLink, sf.get(a).getAktuelleKarte()));
                        for (int id = 0; id < sf.get(a).getAktuellesLevel().getKarten().get(i).getClientIDs().size(); id++) {
                            int idc = sf.get(id).getClientID();
                            if (!abgehackt.contains(idc)) {
                                outqueue.add(new UpdateNachricht(idc, clientSessionIDComm.get(searchclientIDComm(idc))[0], sendLink, sf.get(id).getAktuelleKarte()));
                                abgehackt.add(sf.get(id).getClientID());
                            }
                        }

                    }
                }
            }

        }
    }


    /**
     * Wenn der Spieler ein Monster angreift, wird der Schaden dem dem entsprechendem Monster zugerechnet.
     *
     * @author Jamann, Lukas 7309791
     */
    public void handleKampf(KampfNachricht kn) {
        int spielerindex = searchSpieler(kn.getClientID());
        if (spielerindex != -1) {
            int[] km = searchMonster(spielerindex, kn.getMonsterID());
            if (km[0] != -1 && km[1] != -1) {
                sf.get(spielerindex).getAktuellesLevel().getKarten().get(km[0]).getMonsterListe().get(km[1]).changeLeben(-sf.get(spielerindex).getAngriffsWert());
                outqueue.add(new KampfNachricht(kn.getClientID(), kn.getClientIDComm(), kn.getMonsterID(), sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).getMonsterListe().get(km[1]).getLeben()));
            }
        }
    }

    /**
     * Setzt die Position des entsprechendem Spielers. Wenn mehrere in der Karte sind, wird ihnene die Position mitgeteilt.
     *
     * @author Jamann, Lukas 7309791
     */
    private void handleLaufNachricht(LaufNachricht ln) {
        int spielerindex = searchSpieler(ln.getClientID());
        if (spielerindex != -1) {
            if (sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).getClientIDs().size() == 1) {
                sf.get(spielerindex).setPosition(new int[]{ln.getxPosNeu(), ln.getyPosNeu()});
            } else {
                for (int id = 0; id < sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).getClientIDs().size(); id++) {
                    if (sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).getClientIDs().get(id) != ln.getClientID()) {
                        int tempid = sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).getClientIDs().get(id);
                        outqueue.add(new MitspielerNachricht(tempid, ln.getClientIDComm(), new Spieler(ln.getClientID(), sf.get(spielerindex).getName(), ln.getxPosNeu(), ln.getyPosNeu())));

                    }
                }

            }
        }
    }

    /**
     * Generieren und senden eines neuem Levels zu dem Client.
     *
     * @author Jamann, Lukas 7309791
     */
    private void handleLevel(LevelNachricht ln) {
        int spielerindex = searchSpieler(ln.getClientID());
        if (spielerindex != -1) {
            sf.get(spielerindex).lokalisiereEingang(levelGenerator.erzeugeLevel(ln.getLevelNr()));
            outqueue.add(new LevelNachricht(ln.getClientID(), ln.getClientIDComm(), sf.get(spielerindex)));
        }
    }

    /**
     * Behandelt die Fälle, Tränke zu sich nehmen, Raumwechsel, SkillPunktverteilung.
     *
     * @author Jamann, Lukas 7309791
     */
    private void handleSpieler(SpielerNachricht sn) {
        int spielerindex = searchSpieler(sn.getClientID());
        if (spielerindex != -1) {
            if (sn.getAktuelleKarte() != -1) {
                LinkedList tmp = sf.get(spielerindex).getAktuellesLevel().getKarten().get(sf.get(spielerindex).getAktuelleKarte()).getClientIDs();
                //Fall für Raumwechsel, falls noch andere Spieler im Raum sind.
                if (tmp.size() != 1) {
                    for (int i = 0; i < tmp.size(); i++) {
                        int tmpid = searchSpieler((Integer) tmp.get(i));
                        if (sn.getClientID() != sf.get(tmpid).getClientID()) {
                            sf.get(tmpid).getAktuellesLevel().getKarten().get(sf.get(tmpid).getAktuelleKarte()).deleteClientID(sn.getClientID());
                            outqueue.add(new MitspielerNachricht(sf.get(tmpid).getClientID(), clientSessionIDComm.get(searchclientIDComm(sf.get(tmpid).getClientID()))[0], new Spieler(sn.getClientID(), sf.get(spielerindex).getName(), sf.get(spielerindex).getxPos(), sf.get(spielerindex).getyPos()), true));
                        }
                    }
                    sf.get(spielerindex).raumWechsel();
                    if (sf.get(spielerindex).getAktuelleKarte() != sn.getAktuelleKarte()) {
                        sf.get(spielerindex).setKarteKorrektur(sn.getAktuelleKarte(), new int[]{sn.getxPos(), sn.getyPos()});
                    }
                } else {
                    //Kein anderer Spieler im Raum
                    sf.get(spielerindex).raumWechsel();
                    if (sf.get(spielerindex).getAktuelleKarte() != sn.getAktuelleKarte()) {
                        sf.get(spielerindex).setKarteKorrektur(sn.getAktuelleKarte(), new int[]{sn.getxPos(), sn.getyPos()});
                    }
                }
                // Trank Nutzung
            } else if (sn.getLeben() != -1) {
                sf.get(spielerindex).setLeben(sn.getLeben());
                sf.get(spielerindex).setInventar(sn.getInventar());
                //Skillpunkte Verteilung
            } else {

                sf.get(spielerindex).setStaerke(sn.getStaerke());
                sf.get(spielerindex).setIntelligenz(sn.getIntelligenz());
                sf.get(spielerindex).setGeschicklichkeit(sn.getGeschicklichkeit());
                sf.get(spielerindex).setSkillPunkte(0);
            }
        }
    }

    /**
     * Methode zur Suche einer Position eines Spielers in der LinkedList sf.
     * Bei nicht finden, wird eine -1 return.
     *
     * @author Jamann, Lukas 7309791
     */

    public int searchSpieler(int id) {
        if (sf.isEmpty()) {
            return -1;
        }
        for (int i = 0; i < sf.size(); i++)
            if (sf.get(i) != null && sf.get(i).getClientID() == id) {
                return i;
            }
        return -1;
    }

    /**
     * @author Jamann, Lukas 7309791
     */
    public int[] searchMonster(int id, int mid) {
        if (sf.isEmpty()) {
            return new int[]{-1, -1};
        }
        int indexs = id;
        if (indexs != -1) {
            int i = sf.get(indexs).getAktuelleKarte();
            int msize = sf.get(indexs).getAktuellesLevel().getKarten().get(i).getMonsterListe().size();
            for (int j = 0; j < msize; j++) {
                if ((int) sf.get(indexs).getAktuellesLevel().getKarten().get(i).getMonsterListe().get(j).getMonsterID() == mid) {
                    return new int[]{i, j};
                }
            }
        }


        return new int[]{-1, -1};
    }

    /**
     * @author Jamann, Lukas 7309791
     */
    public boolean searchObjectinList(LinkedList linkedList, Object o) {
        if (!linkedList.isEmpty()) {
            for (int i = 0; i < linkedList.size(); i++) {
                if (linkedList.get(i) == o) return true;
            }
        }
        return false;
    }

    /**
     * @author Jamann, Lukas 7309791
     */
    public void Inbox(Object o) {
        this.inqueue.add(o);
        behandelNachricht();
    }


    /**
     * @author Jamann, Lukas 7309791
     */
    public Object Outbox() {
        return outqueue.getFirst();
    }

    /**
     * @author Jamann, Lukas 7309791
     */
    public int objectClientID() {
        Nachricht n = (Nachricht) outqueue.pollFirst();
        return n.getClientIDComm();
    }

    /**
     * @author Jamann, Lukas 7309791
     */
    public int searchclientID(int idComm) {
        if (!clientSessionIDComm.isEmpty()) {
            for (int i = 0; i < clientSessionIDComm.size(); i++) {
                if (clientSessionIDComm.get(i)[0] == idComm) return i;
            }
        }
        return -1;
    }

    /**
     * Sucht in der clien
     * @author Jamann, Lukas 7309791
     */
    public int searchclientIDComm(int id) {
        if (!clientSessionIDComm.isEmpty()) {
            for (int i = 0; i < clientSessionIDComm.size(); i++) {
                if (clientSessionIDComm.get(i)[2] == id) return i;
            }
        }
        return -1;
    }

    /**
     * Methode zum Sortieren de Highscores
     * @author Jamann, Lukas 7309791
     */
    public void sortiereHighscore() {
        for (int i = 0; i < highscore.size() - 1; i++) {
            for (int j = i + 1; j < highscore.size(); j++) {
                if (Integer.parseInt(highscore.get(i)[1]) < Integer.parseInt(highscore.get(j)[1])) {
                    String[] tmp = highscore.get(i);
                    String[] tmp2 = highscore.get(j);
                    highscore.remove(i);
                    highscore.remove(j);
                    highscore.add(i, tmp2);
                    highscore.add(j, tmp);


                }
            }
        }
    }

}
