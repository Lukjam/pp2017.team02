package pp2017.team02.server.comm;

/**
 * Created by Saarinen on 02.07.2017.
 */

import pp2017.team02.server.engine.SWV;
import pp2017.team02.shared.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * akzeptiert bis zu 10 Clients
 * ueberprueft regelmaessig, ob Lebenszeichen vom Client verschickt wordern
 * @author Saarinen, Susanna, 7312362
 */

public class CommServer extends Thread {
    private int eindeuditeId = 0;
    private ArrayList<SpielClient> spielClients;
    private int port;
    private boolean weiter;
    private HashMap<Integer, Date>letztesLebenszeichen;
    private SWV sEngine;


    public CommServer(int port, SWV sEngine){
        try{
            this.port = port;
            this.sEngine = sEngine;
            this.spielClients = new ArrayList();
            this.letztesLebenszeichen = new HashMap();
            run();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * startet die Serververbindung und wartet auf Clients
     * laesst bis zu 10 Clientverbindungen zu
     * @author Saarinen, Susanna, 7312362
     */

    public void run() {
        System.out.println("Versuche Server zu starten");
        this.weiter = true;

        try {
            ServerSocket serverSocket = new ServerSocket(this.port);
            (new CommServer.ClientChecker()).start();
            (new horcheNachServer()).start();
            (new handleMonster()).start();


            SpielClient tc;
            while (this.weiter) {
                System.out.println("Server wartet auf Clients an Port " + this.port);
                Socket socket = serverSocket.accept();
                if (!this.weiter) {
                    break;
                }

                if (this.spielClients.size() < 11) {
                    tc = new CommServer.SpielClient(socket);
                    System.out.println("Neuer Client!");
                    this.spielClients.add(tc);
                    tc.start();

                }

                //sEngine.behandelNachricht();
                try {
                    sleep(10L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            //alle Clients trennen, wenn die Serververbindung gestoppt wird
            try {
                serverSocket.close();

                for (int i = 0; i < this.spielClients.size(); ++i) {
                    tc = this.spielClients.get(i);

                    try {
                        tc.input.close();
                        tc.output.close();
                        tc.socket.close();
                    } catch (IOException e) {
                        e.fillInStackTrace();
                    }
                    System.out.println("Alle Clients werden vom Server entfernt");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * entfernt einen Client anhand seiner ID
     * sychronized, da mehrere Threads diese Methode aufrufen koennen
     * @author Saarinen, Susanna, 7312362
     */

    synchronized void entferne(int id){
        try{
            for(int i=0; i<this.spielClients.size(); i++){
                SpielClient ct = this.spielClients.get(i);

                if(ct.clientID == id){
                    System.out.println("Client mit der [ID]: " + id + "wird entfernt");
                    this.spielClients.remove(i);
                    return;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * sendet Nachricht an alle Clients
     * @author Saarinen, Susanna, 7312362
     */

    public synchronized void nachrichtAnAlle(Object msg){
        try{
            //System.out.println("Nachricht an alle versendet");

            for(int i=0; i<this.spielClients.size(); i++){
                SpielClient ct = this.spielClients.get(i);

                if(!ct.sendeNachricht(msg)){
                    System.out.println("Spieler " + (this.spielClients.get(i).clientID + " ist nicht erreichbar"));
                    this.spielClients.remove(i);
                }



            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * sendet Nachricht an einen bestimmten Client ueber seine ID
     * falls Client nicht erreichbar ist, wird dieser entfernt
     *@author Saarinen, Susanna, 7312362
     */

    public synchronized void sendeNachrichtanID(Object msg, int id){
        //System.out.println("SendeNachrichtanid");
        try{
            for(int i= this.spielClients.size()-1; i>=0; i--){
                SpielClient ct = this.spielClients.get(i);
                if((this.spielClients.get(i)).clientID == id && !ct.sendeNachricht(msg)){
                    System.out.println("Spieler " + id + "ist nicht erreichbar");
                    this.spielClients.remove(i);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /*public ArrayList<SpielClient> getSpielClients(){
        return this.spielClients;
    }*/

    /**
     * kontrolliert, ob die Clients Lebenszeichen schicken
     * wurde zu lange keins geschickt, wird dieser Client entfernt
     * @author Saarinen, Susanna, 7312362
     */

    public class ClientChecker extends Thread {
        public ClientChecker() {
        }

        public void run() {
            try {
                while(true) {
                    for(int i = 0; i < CommServer.this.spielClients.size(); ++i) {
                        if((new Date()).getTime() - ((Date)CommServer.this.letztesLebenszeichen.get(Integer.valueOf(((CommServer.SpielClient)CommServer.this.spielClients.get(i)).clientID))).getTime() > 1000000L) {
                            System.out.println("Spieler " + CommServer.this.spielClients.get(i).clientID + " zu lange inaktiv.");
                            CommServer.this.entferne((CommServer.this.spielClients.get(i)).clientID);
                        } else {}
                    }

                    Thread.sleep(100L);
                }
            } catch (Exception var2) {
                var2.printStackTrace();
            }
        }
    }

    /**
     * wartet dauerhaft auf Nachrichten vom Server
     * versendet diese an alle oder einzelne Spieler
     * @author Saarinen, Susanna, 7312362
     */
    public class handleMonster extends Thread{

        public void run(){
            while(true){
                sEngine.updateMonster();
                System.out.println("Monster ist weg");

            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            }
        }
    }

    public class horcheNachServer extends Thread{

        public void run(){
            while(true) {

                if (!sEngine.outqueue.isEmpty()) {
                    Object msg = sEngine.Outbox();
                    int id = sEngine.objectClientID();
                    System.out.println("horchenachserver");
                    if (id == -1) {
                        nachrichtAnAlle(msg);
                        System.out.println("horchenachserver");
                    } else {
                        sendeNachrichtanID(msg, id);
                    }

                }try {
                    Thread.sleep(50L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        }

    }

    /**
     * oeffnet fuer jeden Client, der sich verbindet, einen Thread
     * vergibt clientID
     * @author Saarinen, Susanna, 7312362
     */

    public class SpielClient extends Thread{
        private Socket socket;
        private ObjectInputStream input;
        private ObjectOutputStream output;
        int clientID;

        SpielClient(Socket socket){
            CommServer.this.eindeuditeId = CommServer.this.eindeuditeId + 1;
            this.clientID = CommServer.this.eindeuditeId;
            this.socket = socket;
            System.out.println("Thread versucht ObjectInput/ ObjectOutputStream zu erzeugen");

            try{
                this.output = new ObjectOutputStream(socket.getOutputStream());
                this.input = new ObjectInputStream(socket.getInputStream());
                CommServer.this.letztesLebenszeichen.put(Integer.valueOf(this.clientID), new Date());
                //this.sendeNachricht(new LoginNachricht());
            }catch (IOException e){
                e.printStackTrace();
                System.out.println("1");
                return;
            }catch (Exception e){
                e.printStackTrace();
                System.out.println("2");
            }
        }

        /**
         * behandelt Nachrichten je nach Nachrichtenart
         * @author Saarinen, Susanna, 7312362
         */

        public void run(){
            boolean weiter = true;

            while(weiter) {
                try {
                    Nachricht rawMessage = (Nachricht) this.input.readObject();
                    rawMessage.setClientIDComm(this.clientID);
                    System.out.println("Nachricht empfangen von Spieler " + this.clientID );
                    if (rawMessage instanceof LogoutNachricht) {
                        CommServer.this.entferne(this.clientID);
                        System.out.println("Client wurde entfernt");
                        weiter = false;
                        schliessen();

                        break;
                    }if (rawMessage instanceof Lebenszeichen){
                        CommServer.this.letztesLebenszeichen.put(Integer.valueOf(this.clientID), new Date());
                    }
                    else{


                        CommServer.this.sEngine.Inbox(rawMessage);
                        //System.out.println("Lukas");
                    }
                }catch (IOException e){

                    e.printStackTrace();
                    System.out.println("3");
                    break;
                }catch (ClassNotFoundException e){
                    e.printStackTrace();
                    System.out.println("4");
                    break;
                }catch (Exception e){
                    e.printStackTrace();
                    System.out.println("5");
                }
            }

            CommServer.this.entferne(this.clientID);
            System.out.println("6");
            CommServer.this.sEngine.Inbox(new ServerNachricht(clientID, true));
            this.schliessen();
        }

        /**
         * schliesst Input-, Outputstreams und Socket
         * @author Saarinen, Susanna, 7312362
         */

        private void schliessen(){
            try{
                if(this.output != null){
                    this.output.close();
                }
            }catch (Exception e){
                System.out.println("7");
                ;
            }
            try{
                if(this.input != null){
                    this.input.close();
                }
            }catch (Exception e){
                System.out.println("8");
                ;
            }
            try{
                if(this.socket != null){
                    this.socket.close();
                }
            }catch (Exception e){
                System.out.println("9");
                ;
            }
        }

        /**
         * Methode zum Versenden jeglicher Nachrichten
         *@author Saarinen, Susanna, 7312362
         */

        public boolean sendeNachricht(Object msg){
            //System.out.println("Versuche zu senden.");
            if(!this.socket.isConnected()){
                this.schliessen();
                return false;
            }else{
                try{
                    if (msg.getClass() == LoginNachricht.class){
                        LoginNachricht tmplmsg = (LoginNachricht) msg;
                        if (tmplmsg.getSpieler() != null)
                        System.out.println(tmplmsg.getSpieler().getName());
                    }
                    this.output.writeObject(msg);
                    this.output.flush();
                    this.output.reset();
                }catch (IOException e){
                    System.out.println("10");
                    ;
                }
                return true;
            }
        }

        /*public int getMeineID(){
            return this.clientID;
        }*/
    }






}


