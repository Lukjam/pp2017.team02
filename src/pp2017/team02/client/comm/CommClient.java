package pp2017.team02.client.comm;

import pp2017.team02.client.engine.ClientEngine;
import pp2017.team02.server.comm.CommServer;
import pp2017.team02.shared.Lebenszeichen;
import pp2017.team02.shared.LogoutNachricht;
import pp2017.team02.shared.Nachricht;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import static java.net.InetAddress.getLoopbackAddress;


/**
 * Client wird mit dem Server verbunden
 * jegliche Art von Nachrichten wird ueber diese Klasse verschickt
 * sendet Lebenszeichen damit der Server weiss, dass der Client noch da ist
 * @author Saarinen, Susanna, 7312362
 *

 */
public class CommClient {
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private Socket socket;
    private InetAddress serverIp;
    private String server;
    private int port;
    private ClientEngine cEngine;
    public boolean nochDa;


    public CommClient(String server, int port, ClientEngine cEngine) {
        try {
            this.cEngine = cEngine;
            this.server = server;
            this.port = port;
            this.serverIp = getLoopbackAddress(); //TODO
            verbinden();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * diese Methode oeffnet die Verbindung vom Client zum Server
     * @author Saarinen, Susanna, 7312362
     */

    public boolean verbinden() {
        try {
            this.socket = new Socket(this.serverIp.getHostAddress(), this.port);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        try {
            this.ois = new ObjectInputStream(this.socket.getInputStream());
            this.oos = new ObjectOutputStream(this.socket.getOutputStream());
        } catch (IOException e) {
            return false;
        }
        (new empfangeVomServer()).start();
        (new horcheNachClient()).start();
        (new CommClient.sendeLebenszeichen()).start();
        this.nochDa = true;
        return true;
    }

    /**
     * diese Methode trennt den Client vom Server
     * Input-, Outputstream und Socket werden geschlossen
     * @author Saarinen, Susanna, 7312362
     */

    public void trennen() {
        try {
            if (this.oos != null) {
                this.oos.close();
                System.out.println("OutputStream wurde geschlossen");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (this.ois != null) {
                this.ois.close();
                System.out.println("InputStream wurde geschlossen");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (this.socket != null) {
                this.socket.close();
                System.out.println("Socket wurde geschlossen");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * diese Methode sendet Nachrichten jeder Art vom Client an den Server
     * @author Saarinen, Susanna, 7312362
     */

    public synchronized void sendeNachricht(Object msg) {
        try {
            this.oos.writeObject(msg);
            this.oos.flush();
            System.out.println("Nachricht wurde versendet");
        } catch (IOException e) {
            System.out.println("Der Server ist nicht mehr erreichbar. Nachricht wurde nicht versendet");
            this.lebenszeichenStop();
        }
    }

    /**
     * diese Methode sorgt dafuer, dass keine Lebenszeichen mehr gesendet werde
     * @author Saarinen, Susanna, 7312362
     */

    private void lebenszeichenStop() {
        this.nochDa = false;
    }

    /**
     * diese Methode wartet auf Nachrichten vom Client
     * versendet diese durch Aufruf von sendeNachricht()
     * @author Saarinen, Susanna, 7312362
     */

    class horcheNachClient extends Thread{

        public void run(){
            while(true){

            if(!(cEngine.getSendeListe().isEmpty())){
                Object msg = cEngine.getFirstSendeListe();
                System.out.println("LAAL");
                sendeNachricht(msg);
            }try{
                    Thread.sleep(10L);
                }catch(Exception e){
                    e.printStackTrace();
                }


            }
        }
    }

    /**
     * dieser Thread empfaengt dauerhaft Nachrichten vom Server
     * @author Saarinen, Susanna, 7312362
     */

    class empfangeVomServer extends Thread {
        empfangeVomServer() {
        }

        public void run() {
            while (true) {
                try {
                    Nachricht rawNachr = (Nachricht) CommClient.this.ois.readObject();
                    System.out.println(rawNachr.getClientID());
                    if (!(rawNachr instanceof LogoutNachricht)) {
                        if (rawNachr != null) {
                            System.out.println("Client hat die Nachricht empfangen");
                            cEngine.empfangeNachricht(rawNachr);
                        } else {
                            CommClient.this.trennen();
                        }
                    }

                } catch (EOFException e) {
                    System.out.println("Daten vom Server werden nicht mehr richtig gelesen");
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                    break;

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    continue;
                }

            }
        }
    }

    /**
     * dieser Thread sendet dauerhaft Lebenszeichen
     * @author Saarinen, Susanna, 7312362
     */

    public class sendeLebenszeichen extends Thread {


        public void run() {
            try {
                while (nochDa) {
                    sendeNachricht(new Lebenszeichen());
                    Thread.sleep(10000L);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


