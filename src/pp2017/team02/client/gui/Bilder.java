package pp2017.team02.client.gui;

/**
 * Created by Carina on 15.07.2017.
 */

/**
 * Diese Klasse gibt die Bilder zurueck
 * @author Carina Broehl, 7310398
 */

import java.awt.*;

public class Bilder {

    private Image img;

    public Image getImg() {
        return img;
    }

    public Bilder(Image img) {
        this.img = img;
    }
}
