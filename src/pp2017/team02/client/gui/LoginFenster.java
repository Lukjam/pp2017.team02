package pp2017.team02.client.gui;

/**
 * @author Broehl, Carina, 7310398
 */

import pp2017.team02.client.engine.ClientEngine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/* Startfenster zur Anmeldung*/

public class LoginFenster {

    Verschluesselung verschluesselung = new Verschluesselung();

    public static JFrame start;
    private JPanel login;
    private JLabel benutzername, passwort, hintergrund, spielname;
    public JTextField benutzernameEingabe;
    public JPasswordField passwortEingabe;
    private JButton bestaetigen, beenden; //neuesKonto
    private Font schrift1, schrift2;


    private ClientEngine cE;
    public SpielFenster sF;

    public LoginFenster(ClientEngine cEl) {

        this.cE = cEl;

        start = new JFrame();
        start.toFront();
        start.setResizable(false);
        start.setSize(1100, 830);
        start.setLocationRelativeTo(null);
        start.setUndecorated(true);
        start.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Hintergrundbild des Fensters laden
        hintergrund = new JLabel(new ImageIcon("img//Hintergrund.png"));
        hintergrund.setLayout(new FlowLayout());
        start.add(hintergrund);

        //Schiftarten setzen
        schrift1 = new Font("Calibri", Font.BOLD, 50);
        schrift2 = new Font("Calibri", Font.BOLD, 70);

        //Als Ueberschrift wird der Spielname angezeigt
        spielname = new JLabel("Blond in Aegypten");
        spielname.setFont(schrift2);
        spielname.setForeground(Color.BLACK);

        //Feld zur Eingabe des Benuzernamens mit Beschreibung darüber
        benutzername = new JLabel("Benutzername:");
        benutzername.setLabelFor(benutzernameEingabe);
        benutzername.setForeground(Color.BLACK);
        benutzername.setFont(schrift1);
        benutzernameEingabe = new JTextField(10);
        benutzernameEingabe.setFont(schrift1);
        benutzernameEingabe.setForeground(Color.BLACK);
        benutzernameEingabe.setBackground(Color.WHITE);
        benutzernameEingabe.setOpaque(true);

        //Feld zur Eingabe des Passworts mit Beschreibung darüber
        passwort = new JLabel("Passwort:");
        passwort.setLabelFor(passwortEingabe);
        passwort.setFont(schrift1);
        passwort.setForeground(Color.BLACK);
        passwortEingabe = new JPasswordField(6);
        passwortEingabe.setFont(schrift1);
        passwortEingabe.setForeground(Color.BLACK);
        passwortEingabe.setBackground(Color.WHITE);
        passwortEingabe.setOpaque(true);

        //Button zur Ausführung des Login
        bestaetigen = new JButton("Anmelden");
        bestaetigen.setPreferredSize(new Dimension(200,50));
        bestaetigen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String benu = benutzernameEingabe.getText();
                //Passwort verschluesseln
                @SuppressWarnings("deprecation")
                String pas = verschluesselung.rotK(passwortEingabe.getText(),2);

                //Anmeldedaten an ClientEngine schicken
                boolean a = cE.login(benu, pas);

                if(!a){
                    //es wurden nicht sowohl Benutzername als auch Passwort eingegeben
                    System.out.println("Benutzername und Passwort duerfen nicht leer sein");
                }
                else{
                    if (cE.getLoginFall()==2){
                        //Anmeldung erflogreich, Spielfenster wird geoeffnet
                        cE.setLoginTaste(true);
                        return;

                    }else if(cE.getLoginFall() == 1){
                        //Benutzername oder Passwort falsch
                        System.out.println("Benutzername oder Passwort falsch");

                    }else if(cE.getLoginFall() == 3){
                        //neues Konto wurde erstellt und das Skillpunktefenster wird geoeffnet
                        System.out.println("Neues Konto erstellt");
                        cE.setLoginTaste(true);
                        return;
                    }
                }
            }
        });

        //Fenster fuer Kontoerstellung oeffnen, jetzt anders gehandhabt
        /*neuesKonto = new JButton("Neuer Spieler");
        neuesKonto.setPreferredSize(new Dimension(200,50));
        neuesKonto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

               new NeuesKonto(cE);
            }
        });*/

        //Button, um das Spiel zu beenden
        beenden = new JButton("Beenden");
        beenden.setPreferredSize(new Dimension(200,50));
        beenden.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                System.exit(0);
            }
        });

        login = new JPanel();
        login.setOpaque(false);
        login.setLayout(new GridBagLayout());

        // Komponenten positionieren mit GridBag auf Panel

        GridBagConstraints gridbag = new GridBagConstraints();
        gridbag.insets = new Insets(5,5,5,5);

        gridbag.gridx = 0;

        gridbag.gridy++;
        login.add(spielname, gridbag);

        gridbag.gridy++;
        login.add(benutzername, gridbag);

        gridbag.gridy++;
        login.add(benutzernameEingabe, gridbag);

        gridbag.gridx--;
        gridbag.gridy++;
        login.add(passwort, gridbag);

        gridbag.gridx++;
        gridbag.gridy++;
        login.add(passwortEingabe, gridbag);

        gridbag.gridy++;
        login.add(bestaetigen,gridbag);

        gridbag.gridy++;
        gridbag.gridy++;
        //login.add(neuesKonto,gridbag);

        //gridbag.gridy++;
        login.add(beenden,gridbag);


        hintergrund.add(login);
        start.getRootPane().setDefaultButton(bestaetigen);
        start.setVisible(true);
    }

    //Fenster schließen

    public void dispose() {
        // TODO Auto-generated method stub
        start.dispose();
    }

}



