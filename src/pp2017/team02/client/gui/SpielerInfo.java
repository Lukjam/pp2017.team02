package pp2017.team02.client.gui;

import pp2017.team02.client.engine.ClientEngine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Fenster, das ueber Menuezeile geoeffnet werden kann und Auskunft ueber den Spieler gibt, sowie die
 * @author Carina Broehl, 7310398
 */
public class SpielerInfo {


    public static JFrame skill;
    private JPanel punkte;
    private JLabel hintergrund, spielerInfo, staerke, geschicklichtkeit, intelligenz, stufe, erfahrung, skillpunkte;
    private JTextField staerkeEingabe, geschickEingabe, intellEingabe;
    private Font schrift1, schrift2;
    private JButton bestaetigen;

    private ClientEngine cE;

    public SpielerInfo(ClientEngine cE) {

        this.cE = cE;

        skill = new JFrame();
        skill.setUndecorated(true);
        skill.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        skill.setSize(1100, 830);
        skill.setResizable(false);
        skill.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        skill.setLocationRelativeTo(null);

        hintergrund = new JLabel(new ImageIcon("img//hintergrund.png"));
        skill.add(hintergrund);
        hintergrund.setLayout(new FlowLayout());

        schrift1 = new Font("Calibri", Font.BOLD, 50);
        schrift2 = new Font("Calibri", Font.BOLD, 70);

        spielerInfo = new JLabel("Spielerinfo " + cE.getSpieler().getName());
        spielerInfo.setFont(schrift2);
        spielerInfo.setForeground(Color.BLACK);

        stufe = new JLabel("Spielerstufe: " + cE.getSpieler().getStufe());
        stufe.setForeground(Color.BLACK);
        stufe.setFont(schrift1);

        erfahrung = new JLabel("Erfahrungspunkte: " + cE.getSpieler().getErfahrung() + " / " + 100 * Math.pow(1.2,cE.getSpieler().getStufe()));
        erfahrung.setForeground(Color.BLACK);
        erfahrung.setFont(schrift1);

        skillpunkte = new JLabel("Skillpunkte: " + cE.getSpieler().getSkillPunkte());
        skillpunkte.setForeground(Color.BLACK);
        skillpunkte.setFont(schrift1);

        staerke = new JLabel("Staerke: " + cE.getSpieler().getStaerke());
        staerke.setLabelFor(staerkeEingabe);
        staerke.setForeground(Color.BLACK);
        staerke.setFont(schrift1);

        staerkeEingabe = new JTextField(2);
        staerkeEingabe.setFont(schrift1);
        staerkeEingabe.setForeground(Color.BLACK);
        staerkeEingabe.setBackground(Color.WHITE);
        staerkeEingabe.setOpaque(true);

        geschicklichtkeit = new JLabel("Geschicklichkeit: " + cE.getSpieler().getGeschicklichkeit());
        geschicklichtkeit.setLabelFor(geschickEingabe);
        geschicklichtkeit.setForeground(Color.BLACK);
        geschicklichtkeit.setFont(schrift1);

        geschickEingabe = new JTextField(2);
        geschickEingabe.setFont(schrift1);
        geschickEingabe.setForeground(Color.BLACK);
        geschickEingabe.setBackground(Color.WHITE);
        geschickEingabe.setOpaque(true);

        intelligenz = new JLabel("Intelligenz: " + cE.getSpieler().getIntelligenz());
        intelligenz.setLabelFor(intellEingabe);
        intelligenz.setForeground(Color.BLACK);
        intelligenz.setFont(schrift1);

        intellEingabe = new JTextField(2);
        intellEingabe.setFont(schrift1);
        intellEingabe.setForeground(Color.BLACK);
        intellEingabe.setBackground(Color.WHITE);
        intellEingabe.setOpaque(true);

        if(cE.getSpieler().getSkillPunkte() == 0) skill.dispose();

        else {
            bestaetigen = new JButton("Speichern");
            bestaetigen.setPreferredSize(new Dimension(200, 50));
            bestaetigen.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    String staerke1 = staerkeEingabe.getText();
                    Integer i = Integer.parseInt(staerke1);

                    String geschicklichkeit1 = geschickEingabe.getText();
                    Integer j = Integer.parseInt(geschicklichkeit1);

                    String intelligenz1 = intellEingabe.getText();
                    Integer k = Integer.parseInt(intelligenz1);

                    boolean tmp = SpielerInfo.this.cE.skillPktVerteilung(i, k, j);

                    if (tmp) {
                        SpielerInfo.this.cE.setSkillpunktFertigTaste(true);
                        skill.dispose();
                        return;
                    }
                    else {
                        System.out.println("Verteilung erneut eingeben");
                    }

                }
            });
        }

        punkte = new JPanel();
        punkte.setLayout(new GridBagLayout());
        punkte.setOpaque(false);

        GridBagConstraints gridbag3 = new GridBagConstraints();
        gridbag3.insets = new Insets(5,5,5,5);

        //Komponenten positionieren mit GridBag

        gridbag3.gridx = 0;

        gridbag3.gridy++;
        punkte.add(spielerInfo, gridbag3);

        gridbag3.gridy++;
        punkte.add(stufe, gridbag3);

        gridbag3.gridy++;
        punkte.add(erfahrung, gridbag3);

        gridbag3.gridy++;
        punkte.add(skillpunkte, gridbag3);

        gridbag3.gridy++;
        punkte.add(staerke, gridbag3);

        gridbag3.gridy++;
        punkte.add(staerkeEingabe, gridbag3);

        gridbag3.gridx--;
        gridbag3.gridy++;
        punkte.add(intelligenz, gridbag3);

        gridbag3.gridx++;
        gridbag3.gridy++;
        punkte.add(intellEingabe, gridbag3);

        gridbag3.gridx--;
        gridbag3.gridy++;
        punkte.add(geschicklichtkeit, gridbag3);

        gridbag3.gridx++;
        gridbag3.gridy++;
        punkte.add(geschickEingabe, gridbag3);

        gridbag3.gridy++;
        punkte.add(bestaetigen,gridbag3);

        hintergrund.add(punkte);
        skill.getRootPane().setDefaultButton(bestaetigen);
        skill.setVisible(true);
    }
}
