package pp2017.team02.client.gui;

import pp2017.team02.client.engine.ClientEngine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;

/**
 * Created by Carina on 16.07.2017.
 */


public class SpielFenster extends JFrame implements KeyListener{

    private final int KARTENBREITE = 35; // TODO AENDERUNG
    private final int KARTENHOEHE = 35;
    private final int SICHTGROESSE = 13; //Groesse des sichtbaren Ausschnitts

    private final Dimension dim = this.getToolkit().getScreenSize();
    private final int BILDSCHIRMHOEHE = dim.height;
    private final int BILDSCHIRMBREITE = dim.width;

    private final int SPIELFENSTERBREITE = (int) (BILDSCHIRMBREITE * 0.5);
    private final int SPIELFELDBREITE = (int) (SPIELFENSTERBREITE * 0.8);
    private final int SPIELFELDHOEHE = SPIELFELDBREITE;
    private final int ANORDNUNGBREITE = SPIELFENSTERBREITE - SPIELFELDBREITE;
    private final int SPIELFELDBILDGROESSE = (int) (SPIELFELDBREITE/35);
    private final int MINIMAPBILDGROESSE = (int) (ANORDNUNGBREITE/KARTENBREITE);
    private final int MINIMAPHOEHE = MINIMAPBILDGROESSE * KARTENHOEHE;
    private final int STATUSLEISTENHOEHE = 5* SPIELFELDBILDGROESSE;
    private final int MENUELEISTENHOEHE = SPIELFELDBILDGROESSE;

    public SpielFlaeche spielFlaeche;
    public MenueLeiste menueLeiste;
    public StatusLeiste statusLeiste;
    public Highscore highscore;
    public Anordnung anordnung;
    private ClientEngine cE;

    public JPanel spiel;

    public SpielFenster(ClientEngine cEn){

        this.cE = cEn;
        this.setUndecorated(true);
        this.setResizable(false);
        this.setLayout(new BorderLayout());
        this.setVisible(true);
        this.addKeyListener(this);

        spiel = new JPanel(new BorderLayout(1, 1));
        spiel.setPreferredSize(new Dimension(SPIELFELDBREITE, SPIELFELDHOEHE+STATUSLEISTENHOEHE));
        spiel.setVisible(true);

        this.spielFlaeche = new SpielFlaeche(KARTENBREITE, KARTENHOEHE, SPIELFELDBILDGROESSE, cE);
        //this.spielFlaeche.zeichnen(cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()), cE.getSpieler());
        spielFlaeche.setPreferredSize(new Dimension(SPIELFELDBREITE, SPIELFELDHOEHE));
        spielFlaeche.setVisible(true);

        this.menueLeiste = new MenueLeiste(this, cE);
        menueLeiste.setPreferredSize(new Dimension(SPIELFELDBREITE+ANORDNUNGBREITE, MENUELEISTENHOEHE));
        menueLeiste.setVisible(true);

        this.anordnung = new Anordnung(ANORDNUNGBREITE, SPIELFELDHOEHE+STATUSLEISTENHOEHE, MINIMAPHOEHE,
                MINIMAPBILDGROESSE, this.cE);
        anordnung.setPreferredSize(new Dimension(ANORDNUNGBREITE, SPIELFELDHOEHE + STATUSLEISTENHOEHE));
        anordnung.setVisible(true);
        //anordnung.miniMap.zeichnen(cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()), cE.getSpieler());

        this.statusLeiste = new StatusLeiste(this.cE);
        statusLeiste.setPreferredSize(new Dimension(SPIELFELDBREITE, STATUSLEISTENHOEHE));
        statusLeiste.setVisible(true);

        spiel.add(spielFlaeche, BorderLayout.NORTH);
        spiel.add(statusLeiste, BorderLayout.SOUTH);

        this.add(spiel, BorderLayout.CENTER);
        this.add(menueLeiste, BorderLayout.NORTH);
        this.add(anordnung, BorderLayout.EAST);

        this.pack();
        this.repaint();
        this.requestFocus();
        this.setLocation((int) ((dim.getWidth()-this.getWidth())/2),(int) ((dim.getHeight()-this.getHeight())/2));
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                int option = JOptionPane.showConfirmDialog(null,"Spiel speichern?");
                if(option == JOptionPane.YES_OPTION)
                    JOptionPane.showMessageDialog(null, "Spiel gespeichert!");
                System.exit(0);
                if(option == JOptionPane.NO_OPTION)
                    JOptionPane.showMessageDialog(null, "Spiel nicht gespeichert!");
                System.exit(0);
            }
        });
    }

    public void highscoreAnzeigen(LinkedList<String[]> highscoreListe){
        highscore = new Highscore(highscoreListe);
        this.spiel.setVisible(false);
        this.add(highscore, BorderLayout.CENTER);
        this.highscore.setVisible(true);
    }

    public void steuerungAnzeigen(){
        this.remove(spielFlaeche);
        this.requestFocus();
        this.pack();
    }

    @Override
    public void keyTyped(KeyEvent e){
        // TODO Auto-generated method stub
    }

    @Override
    public void keyPressed(KeyEvent e){
        if(e.getKeyCode() == KeyEvent.VK_P){
            cE.nutzeHeilrank();
        }
        else if(e.getKeyCode() == KeyEvent.VK_H){
            cE.droppeTrank();
        }
        else if(e.getKeyCode() == KeyEvent.VK_SPACE){
            cE.sammleItem();
            cE.oeffneTuer();
            cE.neuesLevel();
        }
        else if(e.getKeyCode() == KeyEvent.VK_W){
            cE.laufeHoch();
        }
        else if(e.getKeyCode() == KeyEvent.VK_S){
            cE.laufeRunter();
        }
        else if(e.getKeyCode() == KeyEvent.VK_D){
            cE.laufeRechts();
        }
        else if(e.getKeyCode() == KeyEvent.VK_A){
            cE.laufeLinks();
        }
        else if(e.getKeyCode() == KeyEvent.VK_O){
            cE.angriff();
        }
        spielFlaeche.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e){
        // TODO Auto-generated method stub
    }
}
