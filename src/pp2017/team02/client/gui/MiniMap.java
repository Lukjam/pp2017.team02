package pp2017.team02.client.gui;

/**
 * Created by Carina on 26.06.2017.
 */

/**
 * @author Broehl, Carina, 7310398
 */

import pp2017.team02.shared.Karte;
import pp2017.team02.shared.Spieler;
import pp2017.team02.shared.SpielfeldElement;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Minimap implementieren, also eine kleine Ansicht der Karte, welche
 * ganzheitlich sichtbar ist, keine Monster, Schluessel und Traenke
 */

public class MiniMap extends JPanel {

    private Bilder [][] hintergrund; //untere Schicht für Hintergrundbilder
    private Bilder [][] figuren; //obere Schicht, auf der Figuren angezeigt werden

    private final int KARTENBREITE;
    private final int KARTENHOEHE;

    private final int BILDGROESSE;

    private Image boden = null;
    private Image spieler = null;
    private Image wand = null;
    private Image tuer = null;
    private Image tueroffen = null;
    private Image strudel = null;

    //Konstruktor MiniMap bekommt Groesse uebergeben und erzeugt neue Schichten
    public MiniMap(int KARTENBREITE, int KARTENHOEHE, int BILDGROESSE) {

        this.KARTENBREITE = KARTENBREITE;
        this.KARTENHOEHE = KARTENHOEHE;
        this.BILDGROESSE = BILDGROESSE;

        hintergrund = new Bilder[KARTENHOEHE][KARTENBREITE];
        figuren = new Bilder[KARTENHOEHE][KARTENBREITE];
        bilderLaden();

    }

    //Bilder werden geladen
    private void bilderLaden(){

        try{
            boden = ImageIO.read(new File("img//bodenA.png"));
            spieler = ImageIO.read(new File("img//princess.png"));
            wand = ImageIO.read(new File("img//wandA.png"));
            tuer = ImageIO.read(new File("img//tuer.png"));
            tueroffen = ImageIO.read(new File("img//tueroffen.png"));
            strudel = ImageIO.read(new File ("img//strudel.png"));
        }
        catch (IOException e){
            System.err.println("Ein Bild konnte nicht geladen werden.");
        }
    }

    //Alle Schichten werden auf Null zurueckgesetzt
    public void schichtenZuruecksetzen() {

        for(int zeile = 0; zeile < KARTENHOEHE; zeile++){
            for(int spalte = 0; spalte < KARTENBREITE; spalte++){
                hintergrund[zeile][spalte] = null;
                figuren[zeile][spalte] = null;
            }
        }
    }

    /**
     * Zahlen in der Karte werden ausgelesen und abhaengig von den Schichten werden neue Bildelemente erzeugt
     * diese koennen spaeter mit den wirklichen Bilddateien bestueckt werden
     * @param karte
     * @param spielerx
     */
    public void zeichnen(Karte karte, Spieler spielerx) {

        this.schichtenZuruecksetzen();

        figuren[spielerx.getyPos()][spielerx.getxPos()] = new Bilder(spieler);

        for(int zeile = 0; zeile < KARTENHOEHE; zeile ++){
            for(int spalte = 0; spalte < KARTENBREITE; spalte ++){
                switch (karte.getEintrag(spalte, zeile).getTyp()){
                    case SpielfeldElement.WAND:
                        hintergrund[zeile][spalte] = new Bilder(wand);
                        break;
                    case SpielfeldElement.BODEN:
                        hintergrund[zeile][spalte] = new Bilder(boden);
                        break;
                    case SpielfeldElement.TUER:
                        hintergrund[zeile][spalte] = new Bilder(strudel);
                        break;
                    case SpielfeldElement.EINGANG:
                        hintergrund[zeile][spalte] = new Bilder(tueroffen);
                        break;
                    case SpielfeldElement.AUSGANG:
                        hintergrund[zeile][spalte] = new Bilder(tuer);
                        break;
                    default:
                        hintergrund[zeile][spalte] = new Bilder(boden);
                }
            }
        }
    }

    // Paint-Methode zeichnet die Bilder passend zur Schicht und dem Objekt an dieser Stelle
    public void paint(Graphics g){
        g.drawRect(0, 0, getWidth(), getHeight());
        for(int zeile = 0; zeile<KARTENHOEHE; zeile++){
            for(int spalte = 0; spalte < KARTENBREITE; spalte++){
                if (hintergrund[zeile][spalte] != null) {
                    g.drawImage(hintergrund[zeile][spalte].getImg(), spalte*BILDGROESSE, zeile*BILDGROESSE,
                            BILDGROESSE, BILDGROESSE, null);
                }
                if(figuren[zeile][spalte] != null){
                    g.drawImage(figuren[zeile][spalte].getImg(), spalte*BILDGROESSE, zeile*BILDGROESSE,
                            BILDGROESSE, BILDGROESSE, null);
                }
            }
        }
    }


}
