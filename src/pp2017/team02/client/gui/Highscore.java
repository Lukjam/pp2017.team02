package pp2017.team02.client.gui;

/**
 * Created by Carina on 13.07.2017.
 */

import pp2017.team02.client.engine.ClientEngine;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

/**
 * zustaendig fuer das Highscore Panel und das Zeichnen neuer Namen
 * beinhaltet Paint-Methode
 *
 * @author Carina Broehl
 */

public class Highscore extends JPanel {

    private ClientEngine cE = new ClientEngine();
    private LinkedList<String[]> highscoreListe ;


    public Highscore(LinkedList<String[]> highscoreListe){

        highscoreListe = cE.getHighscore();

        Image img = null;

        try {
            img = ImageIO.read(new File("img//steuerung.png"));
        }
        catch (IOException e) {
            System.out.println("Highscore konnte nicht geladen werden");
        }

        this.highscoreListe = highscoreListe;
    }


    /**
     *Paint-Methode, zustaendig fuer Zeichnen des Highscore
     * Schriftzug, Namen, Hingrund
     */

    public void paint(Graphics g){

        int hoehe = getHeight();
        int breite = getWidth();
        int bildgroesse = breite/22;

        Image highscoreSchriftzug = null;
        Image hintergrund = null;

        try{
            highscoreSchriftzug = ImageIO.read(new File("img//hintergrund.png"));
            hintergrund = ImageIO.read(new File("img//hintergrundHigh.png"));
        }
        catch(IOException e){

        }

        for(int zeile = 0; zeile < hoehe; zeile++) {
            for (int spalte = 0; spalte < breite; spalte++) {
                g.drawImage(hintergrund, bildgroesse * spalte, bildgroesse * zeile, bildgroesse, bildgroesse,
                        null);
            }
        }

        g.drawImage(highscoreSchriftzug, 0, 0, breite, bildgroesse*7, null);

        int spalte = 0;

        Font font = new Font("Calibri", Font.BOLD, 2*bildgroesse-5);
        g.setFont(font);
        g.setColor(Color.white);

        int max = 0;

        if(highscoreListe.size() > 10){
            max = 10;
        }
        else{
            max = highscoreListe.size();
        }

        for (int i = 0; i < max; i++) {
            g.drawString(i + 1 + "." + highscoreListe.get(i), spalte + bildgroesse,
                    2 * bildgroesse * (i + 1) + bildgroesse * 5);

        }
    }
}



