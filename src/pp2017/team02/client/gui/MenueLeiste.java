package pp2017.team02.client.gui;

/**
 * Created by Carina on 26.06.2017.
 */

/**
 * @author Broehl, Carina, 7310398
 */

// HindiBones verwendet und ergänzt

import pp2017.team02.client.engine.ClientEngine;
import pp2017.team02.shared.Spieler;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class MenueLeiste extends JMenuBar implements ActionListener {

    private static final long serialVersionUID = 1L;

    private JMenu spiel;
    private JMenu anzeige;
    private JMenu hilfe;

    private JMenuItem neuesSpiel;
    private JMenuItem ausloggen;
    private JMenuItem highscore;
    private JMenuItem highscoreschliessen;
    private JMenuItem spielerInfo;
    private JMenuItem beenden;
    private JMenuItem karteaufdecken;
    private JMenuItem karteverdecken;
    private JMenuItem steuerung;

    private SpielFenster spielFenster;
    private ClientEngine cE;
    private LinkedList<String[]> highscoreListe;

    public MenueLeiste(SpielFenster spielFenster, ClientEngine cE){

        this.spielFenster = spielFenster;
        this.cE = cE;

        //Menuepunkte
        spiel = new JMenu("Spiel");
        anzeige = new JMenu("Anzeige");
        hilfe = new JMenu("Hilfe");

        //Unter Menuepunkte
        neuesSpiel = new JMenuItem("Neues Spiel starten");
        ausloggen = new JMenuItem("Ausloggen");
        highscore = new JMenuItem("Highscore anzeigen");
        highscoreschliessen = new JMenuItem("Highscore schliessen");
        spielerInfo = new JMenuItem(("Spielerinfo"));
        beenden = new JMenuItem("Beenden");
        karteaufdecken = new JMenuItem("Karte aufdecken");
        karteverdecken = new JMenuItem("Karte verdecken");
        steuerung = new JMenuItem("Steuerung");

        //ActionListener
        neuesSpiel.addActionListener(this);
        ausloggen.addActionListener(this);
        highscore.addActionListener(this);
        highscoreschliessen.addActionListener(this);
        spielerInfo.addActionListener(this);
        beenden.addActionListener(this);
        karteaufdecken.addActionListener(this);
        karteverdecken.addActionListener(this);
        steuerung.addActionListener(this);
        hilfe.addActionListener(this);

        //Unter Menuepunkte zu Menupunkten hinzufuegen
        spiel.add(neuesSpiel);
        spiel.add(ausloggen);
        spiel.add(beenden);
        anzeige.add(highscore);
        anzeige.add(spielerInfo);
        anzeige.add(karteaufdecken);
        hilfe.add(steuerung);

        //Menuepunkte zu Menueleiste hinzufuegen
        this.add(spiel);
        this.add(anzeige);
        this.add(hilfe);
    }

    //actionPerfomed: was passiert, wenn die einzelnen Menue(unter)punkte gedrueckt werden
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == neuesSpiel) {
            //das Spiel wird von Neuem gestartet
            cE.neuesSpiel();

        }else if(e.getSource() == ausloggen){
            //Der Spieler wird ausgeloggt und das LoginFenster wird wieder geoeffnet
            cE.logout();
            spielFenster.dispose();
            new LoginFenster(cE);

        }else if(e.getSource() == highscore) {
            //Highscore wird angezeigt
            spielFenster.highscoreAnzeigen(highscoreListe);
            anzeige.remove(highscore);
            anzeige.add(highscoreschliessen);

        } else if(e.getSource() == highscoreschliessen) {
            //Highscore wird geschlossen
            anzeige.remove(highscoreschliessen);
            anzeige.add(highscore);
            spielFenster.highscore.setVisible(false);
            spielFenster.spiel.setVisible(true);

        }else if(e.getSource() == spielerInfo) {
            //Spielerinfo wird angezeigt
            new SpielerInfo(cE);

        }else if(e.getSource() == karteaufdecken){
            //Karte ist ganzheitlich sichtbar
            spielFenster.spielFlaeche.nebelAn = false;
            spielFenster.spielFlaeche.repaint();
            anzeige.remove(0);
            anzeige.add(karteverdecken, 0);

        }else if(e.getSource() == karteverdecken){
            //Von der Spielfläche ist nur eine quadratische Fläche um den Spieler sichtbar
            spielFenster.spielFlaeche.nebelAn = true;
            spielFenster.spielFlaeche.repaint();
            anzeige.remove(0);
            anzeige.add(karteaufdecken, 0);

        }else if(e.getSource() == beenden){
            //Spiel wird beendet
            System.exit(0);

        }else if(e.getSource() == steuerung){
            //Anzeige der Steuerung
            Image hintergrund;
            JFrame frame = new JFrame();
            frame.setSize(540, 470);
            frame.setResizable(false);
            frame.setLocationRelativeTo(null);
            try{
                hintergrund = ImageIO.read(new File("img//steuerung.png"));
                frame.setContentPane(new JLabel(new ImageIcon("img//steuerung.png")));
            }
            catch (IOException e1){
                System.err.println("Hintergrundbild konnte nicht geladen werden!");
            }
            frame.setVisible(true);
        }
    }

}


