package pp2017.team02.client.gui;

/**
 * Created by Carina on 26.06.2017.
 */

/**
 * @author Broehl, Carina, 7310398
 */

import pp2017.team02.client.engine.ClientEngine;
import pp2017.team02.shared.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class SpielFlaeche extends JPanel {

    private static final long serialVersionUID = 1L;

    private final int SICHTGROESSE = 13; //sichtbar sind 13x13 Felder
    private final int VERSCHIEBUNG = 6; //alle Felder vom Spieler aus um 6 Felder verschoben

    private Bilder[][] spielfeldhintergrund; //unterste Schicht mit Waenden und Boden
    private Bilder[][] figuren; //2. Schicht auf der Spielfiguren abgebildet werden
    private Bilder[][] schluessels; //Schicht, auf der die Schluessel abgebildet werden
    private Bilder[][] traenke; //Schicht, auf der die Traenke abgebildet werden
    private int[][] monster; //Schicht, auf der die Lebenspunkte der Monster zu zeichnen sind

    private final int KARTENBREITE;
    private final int KARTENHOEHE;
    private final int BILDGROESSE;

    private Image boden;
    private Image drache1;
    private Image drache2;
    private Image drache3;
    private Image heiltrank;
    private Image schluessel;
    private Image spieler;
    private Image wand;
    private Image tuer;
    private Image tueroffen;
    private Image strudel;
    private Image drache4;
    private Image monsterWand;

    public boolean nebelAn = false;
    public int versatzZeile, versatzSpalte;

    private ClientEngine cE;

    public SpielFlaeche(int KARTENBREITE, int KARTENHOEHE, int BILDGROESSE, ClientEngine cEn) {

        this.cE = cEn;
        this.KARTENBREITE = KARTENBREITE;
        this.KARTENHOEHE = KARTENHOEHE;
        this.BILDGROESSE = BILDGROESSE;

        spielfeldhintergrund = new Bilder[KARTENHOEHE][KARTENBREITE];
        figuren = new Bilder[KARTENHOEHE][KARTENBREITE];
        bilderLaden();

    }

    private void bilderLaden(){

        try{
            boden = ImageIO.read(new File("img//bodenA.png"));
            spieler = ImageIO.read(new File("img//princess.png"));
            wand = ImageIO.read(new File("img//wandA.png"));
            tuer = ImageIO.read(new File("img//tuer.png"));
            tueroffen = ImageIO.read(new File("img//tueroffen.png"));
            strudel = ImageIO.read(new File ("img//strudel.png"));
            drache1 = ImageIO.read(new File("img//drache1.png"));
            drache2 = ImageIO.read(new File("img//drache2.png"));
            drache3 = ImageIO.read(new File("img//drache3.png"));// TODO MEHR BILDER LADEN
            drache4 = ImageIO.read(new File("img//dragon_1.png"));
            monsterWand = ImageIO.read(new File("img//goblin_2_walk_003.png"));
        }
        catch (IOException e){
            System.err.println("Ein Bild konnte nicht geladen werden.");
        }
    }

    public void schichtenZuruecksetzen() {

        for(int spalte = 0; spalte < KARTENHOEHE; spalte ++) {
            for (int zeile = 0; zeile < KARTENBREITE; zeile++) {
                spielfeldhintergrund[zeile][spalte] = null;
                figuren[zeile][spalte] = null;
            }
        }
    }

    public void zeichnen(Karte karte, Spieler spielerx) {

        this.schichtenZuruecksetzen();


/*
                for(int spalte = 0; spalte < KARTENHOEHE; spalte ++) {
                    for (int zeile = 0; zeile < KARTENBREITE; zeile++) {
                        switch (karte.getEintrag(spalte, zeile).getTyp()){
                            case SpielfeldElement.WAND:
                                spielfeldhintergrund[zeile][spalte] = new Bilder(wand);
                                break;
                            case SpielfeldElement.BODEN:
                                spielfeldhintergrund[zeile][spalte] = new Bilder(boden);
                                break;
                            case SpielfeldElement.TUER:
                                spielfeldhintergrund[zeile][spalte] = new Bilder(tuer);
                                break;
                            case SpielfeldElement.EINGANG:
                                spielfeldhintergrund[zeile][spalte] = new Bilder(tueroffen);
                                break;
                            case SpielfeldElement.AUSGANG:
                                spielfeldhintergrund[zeile][spalte] = new Bilder(strudel);
                                break;
                            default:
                                spielfeldhintergrund[zeile][spalte] = new Bilder(strudel);//TODO MINIMAP ANPASSEN
                        }
                    }
                }*/
                /*for(int zeile = 0; zeile < KARTENHOEHE; zeile ++){
                    for(int spalte = 0; spalte < KARTENBREITE; spalte ++){
                for(int i=0; i<karte.getMonsterListe().size(); i++){
                    if(karte.getMonsterListe().get(i).getPosition().equals(karte.getEintrag(zeile, spalte))) {

                        if (karte.getMonsterListe().get(i).getClass() == Drache.class) {
                            figuren[KARTENBREITE][KARTENBREITE] = new Bilder(drache2);
                        } else if (karte.getMonsterListe().get(i).getClass() == Statue.class) {
                            figuren[KARTENBREITE][KARTENBREITE] = new Bilder(drache1);
                        }
                    }
                }}}*/
    }



    public void paint(Graphics g){
        this.schichtenZuruecksetzen();
        for(int spalte = 0; spalte < KARTENHOEHE; spalte ++) {
            for (int zeile = 0; zeile < KARTENBREITE; zeile++) {
                switch ((cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getEintrag(spalte,zeile).getTyp())) { //war spalte,zeile

                    case SpielfeldElement.WAND:
                        spielfeldhintergrund[zeile][spalte] = new Bilder(wand);
                        break;
                    case SpielfeldElement.BODEN:
                        spielfeldhintergrund[zeile][spalte] = new Bilder(boden);
                        break;
                    case SpielfeldElement.TUER:
                        spielfeldhintergrund[zeile][spalte] = new Bilder(tuer);
                        break;
                    case SpielfeldElement.EINGANG:
                        spielfeldhintergrund[zeile][spalte] = new Bilder(tueroffen);
                        break;
                    case SpielfeldElement.AUSGANG:
                        spielfeldhintergrund[zeile][spalte] = new Bilder(strudel);
                        break;
                    default:
                        spielfeldhintergrund[zeile][spalte] = new Bilder(wand);//TODO MINIMAP ANPASSEN
                }
            }
        }
        figuren[cE.getSpieler().getyPos()][cE.getSpieler().getxPos()] = new Bilder(spieler);
        g.drawRect(0, 0, getWidth(), (int) (getHeight()*0.6));
        // Spieler zeichene.
        if (cE.getMitSpieler() != null)
        {
            if (!cE.getMitSpieler().isEmpty())
            {
                for (int i = 0; i < cE.getMitSpieler().size(); i++) {
                    figuren[cE.getMitSpieler().get(i).getyPos()][cE.getMitSpieler().get(i).getxPos()] = new Bilder(spieler);
                }
            }}
        //Monster zeichnen
        if (!cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).
                getMonsterListe().isEmpty() || cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getMonsterListe() != null){
            for(int i = 0; i<cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getMonsterListe().size();i++){
                if (cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getClass() == Drache.class) {
                    figuren[cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte())
                            .getMonsterListe().get(i).getyPos()][cE.getSpieler().getAktuellesLevel().getKarten().
                            get(cE.getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getxPos()]
                            = new Bilder(drache4);
                }
                if (cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getClass() == Statue.class) {
                    figuren[cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte())
                            .getMonsterListe().get(i).getyPos()][cE.getSpieler().getAktuellesLevel().getKarten().
                            get(cE.getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getxPos()]
                            = new Bilder(monsterWand);
                }
            }
        }
        for(int zeile = 0; zeile<KARTENHOEHE; zeile++){
            for(int spalte= 0; spalte < KARTENBREITE; spalte++){
                if (spielfeldhintergrund[zeile][spalte] != null) { // war zeile spalte
                    g.drawImage(spielfeldhintergrund[zeile][spalte].getImg(), spalte*BILDGROESSE, zeile*BILDGROESSE,
                            BILDGROESSE, BILDGROESSE, null);
                }
                if(figuren[zeile][spalte] != null){
                    g.drawImage(figuren[zeile][spalte].getImg(), (spalte*BILDGROESSE)-20, (zeile*BILDGROESSE)-20,
                            BILDGROESSE+40, BILDGROESSE+40, null);
                }

                if (nebelAn) {
                    if (zeile <= (int) (this.SICHTGROESSE / 2 - 3) || zeile >= (int) (this.SICHTGROESSE / 2 + 3)
                            || spalte <= (int) (this.SICHTGROESSE / 2 - 3)
                            || spalte >= (int) (this.SICHTGROESSE / 2 + 3))
                    {
                        g.setColor(Color.BLACK);
                        g.fillRect(spalte * BILDGROESSE, zeile * BILDGROESSE, BILDGROESSE, BILDGROESSE);
                    }
                }
            }

        }
                /*for(int zeile = 0; zeile < KARTENHOEHE; zeile ++){
                    for(int spalte = 0; spalte < KARTENBREITE; spalte ++){
                for(int i=0; i< cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getMonsterListe().size(); i++){
                    if(cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getPosition().equals(cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getEintrag(zeile, spalte))) {

                        if (cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getClass() == Monster.class) {
                            figuren[KARTENBREITE][KARTENBREITE] = new Bilder(drache2);
                        } else if (cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getClass() == Statue.class) {
                            figuren[KARTENBREITE][KARTENBREITE] = new Bilder(drache1);
                        }
                    }
                }
                    }
            }*/
    }/*
    public boolean inSichtweite(int x, int y){
        // OBEN
        if (cE.getSpieler().getxPos() < 6 && x -6< cE.getSpieler().getxPos()){
            return true;
        }
        if (cE.getSpieler().getxPos() > 28  x+6 > cE.getSpieler().getxPos()){
            return true;
        }
        return true;*/




}


