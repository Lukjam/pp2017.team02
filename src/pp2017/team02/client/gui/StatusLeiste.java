package pp2017.team02.client.gui;

/**
 *
 * Implementierung der Statusleiste, auf der Spielername, Levelnummer, Heiltraenke und Schluessel angezeigt werden
 *
 * @author Carina Broehl, 7310398
 */


import pp2017.team02.client.engine.ClientEngine;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class StatusLeiste extends JPanel {

    private static final long serialVersionUID = 1L;

    private ClientEngine cE;
    private int zeit;
    private Image schluessel, princess, heiltrank;

    public StatusLeiste(ClientEngine cEn){
        this.cE = cEn;

        try {
            princess = ImageIO.read(new File("img//princess.png"));
            schluessel = ImageIO.read(new File("img//schluessel.png"));
            heiltrank = ImageIO.read(new File("img//heiltrank.png"));
        }
        catch (IOException e) {
            System.err.println("Bilder konnten nicht geladen werden.");
        }
    }

    public void paint(Graphics g) {

        int hoehe = getHeight()+30;
        int breite = getWidth()+100;

        Color c = new Color(221, 196, 154);
        g.setColor(c);
        g.fillRect(0, 0, breite, hoehe);

        int zeilenHoehe = hoehe / 4;
        int abstandLinks = breite / 6;

        int spielerGroesse = (int) (abstandLinks * 0.5);
        int spielerPosX = (int) (abstandLinks * 0.2);
        int spielerPosY = (int) (hoehe - (spielerGroesse * 1.7));

        int schluesselGroesse = (int) (abstandLinks * 0.8);
        int schluesselPosX = (int) (abstandLinks * 5.0);
        int schluesselPosY = hoehe - schluesselGroesse;

        int heiltrankGroesse = (int) (abstandLinks * 0.8);

        Font font = new Font("Calibri", Font.BOLD, zeilenHoehe - 3);
        g.setFont(font);

        //Spielerbild und Name
        g.drawImage(princess, spielerPosX, spielerPosY, spielerGroesse, schluesselGroesse, null);
        g.drawString(cE.getSpieler().getName(), spielerPosX, (int) (spielerPosY+hoehe*0.8));

        //Lebensanzeige
        g.drawString(""+cE.getSpieler().getLeben(), (abstandLinks), (int) (zeilenHoehe*1.5));
        g.setColor(Color.RED);
        g.fillRect((abstandLinks), zeilenHoehe*2, cE.getSpieler().getMaxLeben(), 10);
        g.setColor(Color.GREEN);
        g.fillRect((abstandLinks), zeilenHoehe*2, cE.getSpieler().getLeben(), 10);

        //Level und Zeit
        g.setColor(Color.BLACK);
        //g.drawString("Zeit: " + System.currentTimeMillis(), abstandLinks*3, (int) (zeilenHoehe*1.5));
        g.drawString("Level: " + cE.getSpieler().getAktuellesLevel().getLevelnr(), abstandLinks*3, (int) (zeilenHoehe*3));

        //Traenke und Schluessel
        g.drawString(""+cE.getSpieler().getAnzahlTraenke(), schluesselPosX-heiltrank.getWidth(null),
                (int) (zeilenHoehe*1.5));
        g.drawImage(heiltrank, schluesselPosX-heiltrank.getWidth(null), schluesselPosY, heiltrankGroesse,
                heiltrankGroesse, null);

        if (cE.getSpieler().isHatSchluessel()) {
            g.drawImage(schluessel, schluesselPosX, schluesselPosY, schluesselGroesse, schluesselGroesse, null);
        }

    }
}


