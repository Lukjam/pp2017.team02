package pp2017.team02.client.gui;

/**
 * Created by Carina on 15.07.2017.
 */

import pp2017.team02.client.engine.ClientEngine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * In dieser Klasse wird das Chatfenster zur Verfügung gestellt
 * @author Carina Bröhl, 7310398
 */

public class ChatMain extends JPanel {

    public JFrame frame = new JFrame();
    public JPanel panel1, panelChat;
    public JPanel panel2;
    public JButton senden;
    public JTextField eingabeFeld;
    public JTextArea textFeld;

    public ClientEngine cE;

    public SpielFenster sF;

    private Anordnung a;
    private String benutzername;

    public ChatMain(ClientEngine cE) {

        this.cE = cE;
        panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());

        panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        panel2.setBackground(Color.DARK_GRAY);

        //Button fürs Senden implementieren
        senden = new JButton("senden");
        senden.addActionListener(new SendMessageButtonListener());

        //Feld, in das die Chatnachricht eingegeben wird
        eingabeFeld = new JTextField(30);
        eingabeFeld.requestFocusInWindow();

        //hier werden die Chatnachrichten angezeigt
        textFeld = new JTextArea();
        textFeld.setFont(new Font("Calibri", Font.PLAIN, 15));
        textFeld.setEditable(false);
        textFeld.setLineWrap(true);

        //GridBag für Anordnung
        GridBagConstraints links = new GridBagConstraints();
        links.anchor = GridBagConstraints.LINE_START;
        links.fill = GridBagConstraints.HORIZONTAL;
        links.weightx = 512.0D;
        links.weighty = 1.0D;

        GridBagConstraints rechts = new GridBagConstraints();
        rechts.insets = new Insets(0, 5, 0, 0);
        rechts.anchor = GridBagConstraints.LINE_END;
        rechts.fill = GridBagConstraints.NONE;
        rechts.weightx = 1.0D;
        rechts.weighty = 1.0D;

        //Das Eingabefeld und der Sendebutton werden nebeneinander auf einem JPanel angeordnet
        panel2.add(eingabeFeld, links);
        panel2.add(senden, rechts);

        //Das Textfeld mit einer Scrolleiste über Eingabefeld&Button anordnen
        panel1.add(new JScrollPane(textFeld), BorderLayout.CENTER);
        panel1.add(BorderLayout.SOUTH, panel2);
        panel1.setSize(470, 300);
        panel1.setVisible(true);


        //panel1 auf frame
        frame.add(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(280, 630);
        frame.setUndecorated(true);
        frame.setVisible(true);

        //Benutzername speichern, um diesesn im Textfeld anzeigen lassen zu könenn
        benutzername = cE.getSpieler().getName();
    }

    /**
     * Wird der Sendebutton betätigt, wird der Text aus dem Eingabefeld im Textfeld angezeigt mit dem Benutzername
     * davor und die Nachricht verschickt. Vor jeder Nachricht im Textfeld steht der Benutzername des jeweiligen
     * Spielers, der diese Nachricht versendet hat
     */

    class SendMessageButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (eingabeFeld.getText().length() >= 1) {
                //textFeld.append(benutzername + ": " + eingabeFeld.getText() + "\n");
                cE.sendeChat(eingabeFeld.getText());
                eingabeFeld.setText("");
            }
            eingabeFeld.requestFocusInWindow();
        }
    }
}



