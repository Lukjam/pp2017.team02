package pp2017.team02.client.gui;

/**
 * Created by Carina on 13.07.2017.
 */
/**
 * @author Broehl, Carina, 7310398
 */


import pp2017.team02.client.engine.ClientEngine;

import javax.swing.*;
import java.awt.*;

/**
 * In dieser Klasse wird ein JPanel erzeugt, auf welchem das Chatfenster und die Minimap untereinander dargestellt
 *
 */



public class Anordnung extends JPanel {

    private static final long serialVersionUID = 1L;

    ClientEngine cE;

    public MiniMap miniMap;
    public ChatMain chatMain;

    private final int BILDGROESSE;
    private final int MINIMAPHOEHE;

    public Anordnung(int breite, int hoehe, int MINIMAPHOEHE, int MINIMAPBILDGROESSE, ClientEngine cE) {

        this.cE = cE;
        this.MINIMAPHOEHE = MINIMAPHOEHE;
        this.BILDGROESSE = MINIMAPBILDGROESSE;

        this.setLayout(new BorderLayout());

        miniMap = new MiniMap(35, 35, MINIMAPBILDGROESSE);
        miniMap.setPreferredSize(new Dimension(breite, MINIMAPHOEHE));
        miniMap.setVisible(true);

        chatMain = new ChatMain(this.cE);
        chatMain.setPreferredSize(new Dimension(breite, hoehe - MINIMAPHOEHE));
        chatMain.setVisible(true);

        this.add(miniMap, BorderLayout.NORTH);
        this.add(chatMain, BorderLayout.SOUTH);

    }
}


