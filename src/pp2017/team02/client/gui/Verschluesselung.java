package pp2017.team02.client.gui;

/**
 * Created by Carina on 14.07.2017.
 */

/**
 * Verschluesselungsklasse, welche einen eingebenen String realtiv simpel verschluesseln kann
 *
 * @author Prof Juenger - Universitaet zu Koeln
 *
 */

public class Verschluesselung {
    /**
     * Verschluesselt einen gegebenen String <code>eingabetext</code> durch
     * das ROT-k-Verfahren. Dazu wird jeder Buchstabe in <code>eingabetext</code>
     * um k Zeichen im Alphabet zyklisch nach vorne verschoben.
     *
     * Die Methode wandelt Gross- in Kleinbuchstaben um und erhaelt alle
     * Sonderzeichen.
     *
     * @param eingabetext Ein beliebiger Text, der verschluesselt werden soll.
     * @param k Anzahl Zeichen, um die zyklisch verschoben wird.
     * @return Die ROT-k-Verschluesselung des Eingabetextes, umgewandelt in
     * Kleinbuchstaben.
     *
     * @author Prof Juenger - Universitaet zu Koeln
     */
    public static String rotK(String eingabetext, int k){
        // Wandle die Eingabe in Kleinbuchstaben um.
        String kleingeschrieben = eingabetext.toLowerCase();

        // In diesem Array wird das Ergebnis der Verschluesselung gespeichert.
        char verschluesselt[] = new char[kleingeschrieben.length()];

        // Fuer alle Zeichen im Eingabetext:
        for(int i=0; i < kleingeschrieben.length(); i++){
            // c ist das i-te Zeichen im Text
            char c = kleingeschrieben.charAt(i);

            // Verschluessele nur, wenn c ein Buchstabe ist
            if(c >= 'a' && c <= 'z'){
                // Verschiebe c um 7 Zeichen nach vorne. Wir rechnen
                // modulo 26 um zyklisch zu verschieben.
                c = (char) ((c - 'a' + k) % 26 + 'a');
            }
            verschluesselt[i] = c;
        }

        // Gib das Ergebnis als String zurueck.
        return new String(verschluesselt);
    }

    // Entschluesselt den String durch rekursives Aufrufen der Verschluesselungsmethode
    public static String entschluesseln(String eingabetext, int k){
        return rotK(eingabetext, 26 - k);
    }


}
