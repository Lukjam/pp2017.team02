package pp2017.team02.client.gui;

import pp2017.team02.client.engine.ClientEngine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Dieses Fenster wird geoeffnet, wenn ein neues Konto erstellt wird, damit der Spieler seine 10 zu Beginn
 * erhaltenen Skillpunkte auf die drei Attribute Staerke, Geschicklichkeit und Intelligenz auf zu teilen
 *
 * @author Carina Broehl, 7310398
 */

public class SkillPunkte {

    public static JFrame skill;
    private JPanel punkte;
    private JLabel hintergrund, skillPunkte, staerke, geschicklichtkeit, intelligenz;
    private JTextField staerkeEingabe, geschickEingabe, intellEingabe;
    private Font schrift1, schrift2;
    private JButton bestaetigen;

    private ClientEngine cE;

    public SkillPunkte(ClientEngine cEn) {

        this.cE = cEn;

        skill = new JFrame();
        skill.setUndecorated(true);
        skill.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        skill.setSize(1100, 830);
        skill.setResizable(false);
        skill.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        skill.setLocationRelativeTo(null);

        //Hintergrund fuer Fenster wird gesetzt
        hintergrund = new JLabel(new ImageIcon("img//hintergrund.png"));
        skill.add(hintergrund);
        hintergrund.setLayout(new FlowLayout());

        //Schriftart setzen
        schrift1 = new Font("Calibri", Font.BOLD, 50);
        schrift2 = new Font("Calibri", Font.BOLD, 70);

        //Ueberschrift des Fensters
        skillPunkte = new JLabel("10 Skillpunkte verteilen:");
        skillPunkte.setFont(schrift2);
        skillPunkte.setForeground(Color.BLACK);

        //Feld zur Eingabe der Skillpunkte fuer Staerke mit Beschreibung darüber
        staerke = new JLabel("Staerke:");
        staerke.setLabelFor(staerkeEingabe);
        staerke.setForeground(Color.BLACK);
        staerke.setFont(schrift1);
        staerkeEingabe = new JTextField(2);
        staerkeEingabe.setFont(schrift1);
        staerkeEingabe.setForeground(Color.BLACK);
        staerkeEingabe.setBackground(Color.WHITE);
        staerkeEingabe.setOpaque(true);

        //Feld zur Eingabe der Skillpunkte fuer Geschicklichkeit mit Beschreibung darüber
        geschicklichtkeit = new JLabel("Geschicklichkeit:");
        geschicklichtkeit.setLabelFor(geschickEingabe);
        geschicklichtkeit.setForeground(Color.BLACK);
        geschicklichtkeit.setFont(schrift1);
        geschickEingabe = new JTextField(2);
        geschickEingabe.setFont(schrift1);
        geschickEingabe.setForeground(Color.BLACK);
        geschickEingabe.setBackground(Color.WHITE);
        geschickEingabe.setOpaque(true);

        //Feld zur Eingabe der Skillpunkte fuer Intelligenz mit Beschreibung darüber
        intelligenz = new JLabel("Intelligenz:");
        intelligenz.setLabelFor(intellEingabe);
        intelligenz.setForeground(Color.BLACK);
        intelligenz.setFont(schrift1);
        intellEingabe = new JTextField(2);
        intellEingabe.setFont(schrift1);
        intellEingabe.setForeground(Color.BLACK);
        intellEingabe.setBackground(Color.WHITE);
        intellEingabe.setOpaque(true);

        //Button, um die Eingabe zu bestaetigen
        bestaetigen = new JButton("Bestaetigen");
        bestaetigen.setPreferredSize(new Dimension(200,50));
        bestaetigen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(staerkeEingabe.getText().isEmpty()) staerkeEingabe.setText("0");
                String staerke1 = staerkeEingabe.getText();
                Integer i = Integer.parseInt(staerke1);

                if(geschickEingabe.getText().isEmpty()) geschickEingabe.setText("0");
                String geschicklichkeit1 = geschickEingabe.getText();
                Integer j = Integer.parseInt(geschicklichkeit1);

                if(intellEingabe.getText().isEmpty()) intellEingabe.setText("0");
                String intelligenz1 = intellEingabe.getText();
                Integer k = Integer.parseInt(intelligenz1);

                //als Integer gespeicherte Eingaben an ClientEngine schicken
                boolean tmpa = cE.neuSkillVerteilung(i, k, j );

                if (tmpa){
                    cE.setSkillpunktFertigTaste(true);
                    return;
                }
                else {
                    System.out.println("Verteilung erneut eingeben");
                }

            }
        });

        punkte = new JPanel();
        punkte.setLayout(new GridBagLayout());
        punkte.setOpaque(false);

        GridBagConstraints gridbag3 = new GridBagConstraints();
        gridbag3.insets = new Insets(5,5,5,5);

        //Komponenten positionieren mit GridBag

        gridbag3.gridx = 0;

        gridbag3.gridy++;
        punkte.add(skillPunkte, gridbag3);

        gridbag3.gridy++;
        punkte.add(staerke, gridbag3);

        gridbag3.gridy++;
        punkte.add(staerkeEingabe, gridbag3);

        gridbag3.gridx--;
        gridbag3.gridy++;
        punkte.add(intelligenz, gridbag3);

        gridbag3.gridx++;
        gridbag3.gridy++;
        punkte.add(intellEingabe, gridbag3);

        gridbag3.gridx--;
        gridbag3.gridy++;
        punkte.add(geschicklichtkeit, gridbag3);

        gridbag3.gridx++;
        gridbag3.gridy++;
        punkte.add(geschickEingabe, gridbag3);

        gridbag3.gridy++;
        punkte.add(bestaetigen,gridbag3);

        hintergrund.add(punkte);
        skill.getRootPane().setDefaultButton(bestaetigen);
        skill.setVisible(true);
    }
}
