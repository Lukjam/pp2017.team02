package pp2017.team02.client.gui;

import pp2017.team02.client.engine.ClientEngine;

import java.util.LinkedList;

/**
 * Created by Carina on 22.07.2017.
 */

/**
 * Hier werden je nach Bedarf das Loginfenster, Skillpunktfenster oder Spielfeld aufgerufen
 *
 * @author Carina Broehl, 7310398
 */


public class GuiAll extends Thread{

    private LoginFenster loginFenster;
    private SpielFenster spielFenster;
    private SkillPunkte skillPunkte;
    ClientEngine cE;

    public GuiAll(ClientEngine cE) {
        this.cE = cE;

        run();


/*
        if (cE.getLoginFall() == 2 && cE.isLoginTaste()) {
            loginFenster.start.dispose();
            spielFenster = new SpielFenster(cE);


        } else if (cE.getLoginFall() == 1) {
            System.out.println("Benutzername oder Passowort falsch");

        } else if (cE.getLoginFall() == 3 && cE.isLoginTaste()) {
            System.out.println("Neues Konto erstellt");
            loginFenster.start.dispose();
            skillPunkte = new SkillPunkte(cE);

        }

        if(cE.isSkillpunktFertigTaste() && cE.isLoginTaste()){

            skillPunkte.skill.dispose();
            cE.setLoginTaste(false);
            cE.setSkillpunktFertigTaste(false);
            spielFenster = new SpielFenster(cE);
        }

        if(cE.isLogoutTaste()){

            cE.setLoginTaste(false);
            spielFenster.dispose();
            loginFenster = new LoginFenster(cE);
        }

*/

    }

    public void run() {
        while(true) {
            try {
                if (cE.getLoginFall() == 2 && cE.isLoginTaste()) {
                    cE.setLoginTaste(false);
                    loginFenster.start.dispose();
                    spielFenster = new SpielFenster(cE);


                } else if (cE.getLoginFall() == 1) {

                    System.out.println("Benutzername oder Passowort falsch");

                } else if (cE.getLoginFall() == 3 && cE.isLoginTaste()) {
                    cE.setLoginTaste(false);
                    System.out.println("Neues Konto erstellt");
                    loginFenster.start.dispose();
                    skillPunkte = new SkillPunkte(cE);

                }
                if (cE.isChatAngekommen()){
                    cE.setChatAngekommen(false);
                    while (!cE.getChatListe().isEmpty()){
                        spielFenster.anordnung.chatMain.textFeld.append(cE.getChatListe().pollFirst()+"\n");
                    }
                }

                if (cE.isSkillpunktFertigTaste()) {
                    if (cE.getLoginFall()==2) {
                        cE.setTasteImSpiel(true);
                        cE.setSkillpunktFertigTaste(false);

                        skillPunkte.skill.dispose();
                        cE.setLoginTaste(false);
                        cE.setSkillpunktFertigTaste(false);
                        spielFenster = new SpielFenster(cE);
                        spielFenster.spielFlaeche.zeichnen(cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()), cE.getSpieler());
                        spielFenster.anordnung.miniMap.zeichnen(cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()), cE.getSpieler());
                    }
                }
                if(cE.isRaumWechselTaste()){
                    spielFenster.dispose();
                    cE.setRaumWechselTaste(false);
                    spielFenster = new SpielFenster(cE);
                    spielFenster.spielFlaeche.zeichnen(cE.getSpieler().getAktuellesLevel().getKarten().get(cE.getSpieler().getAktuelleKarte()), cE.getSpieler());
                }

                if (cE.isLogoutTaste() && !cE.isLoginTaste()) {
                    cE.setLoginTaste(false);
                    cE.setTasteImSpiel(false);

                    cE.setLogoutTaste(false);
                    spielFenster.dispose();
                    loginFenster = new LoginFenster(cE);
                }

                if (cE.isLoginTaste() && cE.isLogoutTaste()){
                    cE.setLogoutTaste(false);
                    loginFenster = new LoginFenster(cE);
                }
                if (cE.isTasteImSpiel()){
                    spielFenster.repaint();

                }

                Thread.sleep(50);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
