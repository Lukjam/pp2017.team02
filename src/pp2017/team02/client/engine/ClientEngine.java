package pp2017.team02.client.engine;


import pp2017.team02.shared.*;

import java.util.LinkedList;
import java.util.Random;

/**
 * @author Tiebing, Alexander, 6018254
 */
public class ClientEngine {

    // Hier werden alle vom Server einkommenden Nachrichten gespeichert
    private LinkedList<Object> empfangsListe = new LinkedList<Object>();

    //  Hier werden alle zu versendeten Nachrichten gespeichert. Die COM liest diese Liste dann aus
    private LinkedList<Object> sendeListe = new LinkedList<Object>();

    // Hier werden vom Server ankommende Chatnachrichten gespeichert, die dann an die GUI gegeben werden
    private LinkedList<String> chatListe = new LinkedList<String>();

    // Hier wird die urspruengliche Skillpunktverteilung des Spielers gespeichert, die dann an den Server gegeben wird
    private LinkedList<Integer> skills = new LinkedList<Integer>();

    // Hier wird die Highscoreliste gespeichert
    private LinkedList<String[]> highscore = new LinkedList<String[]>();

    // Hier werden alle anderen Spieler gespeicher, die sich im selben Level und Raum befinden
    private LinkedList<Spieler> mitSpieler = new LinkedList<Spieler>();

    private int clientID;
    private int sessionID = erstelleSessionID();
    private boolean eingeloggt = false;
    private Spieler spieler;
    private Trank heilTrank;
    private Schluessel schluessel;
    private int loginFall;
    private String spielerAusgeloggt;
    private boolean loginTaste = true;
    private boolean skillpunktFertigTaste = false;
    private boolean logoutTaste = true;
    private boolean tasteImSpiel = false;
    private boolean raumWechselTaste = false;
    private boolean chatAngekommen = false;
    private String benutzername;
    private String passwort;


    /**
     * Getter/ Setter
     *
     * @author Tiebing, Alexander, 6018254
     */

    public LinkedList<Object> getEmpfangsListe() {
        return empfangsListe;
    }

    public void setEmpfangsListe(LinkedList<Object> empfangsListe) {
        this.empfangsListe = empfangsListe;
    }

    public LinkedList<Object> getSendeListe() {
        return sendeListe;
    }

    public void setSendeListe(LinkedList<Object> sendeListe) {
        this.sendeListe = sendeListe;
    }

    public LinkedList<String> getChatListe() {
        return chatListe;
    }

    public void setChatListe(LinkedList<String> chatListe) {
        this.chatListe = chatListe;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public int getSessionID() {
        return sessionID;
    }

    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }

    public boolean isEingeloggt() {
        return eingeloggt;
    }

    public void setEingeloggt(boolean eingeloggt) {
        this.eingeloggt = eingeloggt;
    }

    public Trank getHeilTrank() {
        return heilTrank;
    }

    public void setHeilTrank(Trank heilTrank) {
        this.heilTrank = heilTrank;
    }

    public Schluessel getSchluessel() {
        return schluessel;
    }

    public void setSchluessel(Schluessel schluessel) {
        this.schluessel = schluessel;
    }

    public Spieler getSpieler() {
        return spieler;
    }

    public void setSpieler(Spieler spieler) {
        this.spieler = spieler;
    }

    public LinkedList<String[]> getHighscore() {
        return highscore;
    }

    public void setHighscore(LinkedList<String[]> highscore) {
        this.highscore = highscore;
    }

    public LinkedList<Spieler> getMitSpieler() {
        return mitSpieler;
    }

    public void setMitSpieler(LinkedList<Spieler> mitSpieler) {
        this.mitSpieler = mitSpieler;
    }

    public LinkedList<Integer> getSkills() {
        return skills;
    }

    public void setSkills(LinkedList<Integer> skills) {
        this.skills = skills;
    }

    public int getLoginFall() {
        return loginFall;
    }

    public void setLoginFall(int loginFall) {
        this.loginFall = loginFall;
    }

    public String getSpielerAusgeloggt() {
        return spielerAusgeloggt;
    }

    public void setSpielerAusgeloggt(String spielerAusgeloggt) {
        this.spielerAusgeloggt = spielerAusgeloggt;
    }

    public boolean isLoginTaste() {
        return loginTaste;
    }

    public void setLoginTaste(boolean loginTaste) {
        this.loginTaste = loginTaste;
    }

    public boolean isSkillpunktFertigTaste() {
        return skillpunktFertigTaste;
    }

    public void setSkillpunktFertigTaste(boolean skillpunktFertigTaste) {
        this.skillpunktFertigTaste = skillpunktFertigTaste;
    }

    public boolean isLogoutTaste() {
        return logoutTaste;
    }

    public void setLogoutTaste(boolean logoutTaste) {
        this.logoutTaste = logoutTaste;
    }

    public boolean isTasteImSpiel() {
        return tasteImSpiel;
    }

    public void setTasteImSpiel(boolean tasteImSpiel) {
        this.tasteImSpiel = tasteImSpiel;
    }

    public String getBenutzername() {
        return benutzername;
    }

    public void setBenutzername(String benutzername) {
        this.benutzername = benutzername;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public boolean isRaumWechselTaste() {
        return raumWechselTaste;
    }

    public void setRaumWechselTaste(boolean raumWechselTaste) {
        this.raumWechselTaste = raumWechselTaste;
    }

    public boolean isChatAngekommen() {
        return chatAngekommen;
    }

    public void setChatAngekommen(boolean chatAngekommen) {
        this.chatAngekommen = chatAngekommen;
    }


    /**
     * Erzeugt zufaellige 6-stellige Zufallszahl, die fuer den Login benoetigt wird
     *
     * @return sessionID
     * @author Tiebing, Alexander, 6018254
     */
    public int erstelleSessionID() {

        Random rand = new Random();

        return (int) (rand.nextDouble() * 1000000L);
    }


    /**
     * sendeNachricht ruft Methode der COM auf, die dann alle Nachrichten an den Server schickt
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void sendeNachricht(Object msg) {
        if (sendeListe.size() > 5) {
            try {
                sendeListe.wait(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        sendeListe.add(msg);
    }


    /**
     * empfaengt Nachrichten vom Server, speichert sie in empfangsListe und behandelt sie direkt
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void empfangeNachricht(Object msg) {
        getEmpfangsListe().add(msg);
        handleNachricht();

    }


    /**
     * Ueberprueft, ob die Eingabe beim Login regelkonform war und gibt diese Nachricht an den Server weiter, welcher
     * dann prueft, ob die Logindaten stimmen und der Login erfolgreich war
     *
     * @return: true, falls Login erfolgreich
     * false, sonst
     * @author Tiebing, Alexander, 6018254
     */

    public boolean login(String benutzername, String passwort) {
        if (benutzername.length() == 0 || passwort.length() == 0) {
            return false;
        } else {
            LoginNachricht msg = new LoginNachricht(benutzername, passwort, getSessionID());
            sendeNachricht(msg);
            return true;
        }
    }


    /**
     * Wenn der Spieler sich ausloggt, wird diese Information an den Server weitergegeben. Auch die Daten, die fuer
     * die Highscoreliste gebraucht werden, werden uebergeben, sodass der Server pruefen kann, ob der Spieler in
     * die Highscoreliste aufgenommen wird
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void logout() {
        LogoutNachricht log = new LogoutNachricht(getClientID());
        sendeNachricht(log);
    }


    /**
     * Schickt Nachricht an Server, dass ein neues Spiel gestartet werden soll, von einem bereits eingeloggten Spieler
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void neuesSpiel() {
        //TODO Wie wird das gemacht??
    }

    /**
     * Wenn der Spieler den Ausgang des Levels erreicht hat, wird vom Server ein neues Level angefordert
     * Dem Server wird die aktuelle Levelnummer geschickt
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void neuesLevel() {
        if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte())
                .getEintrag(getSpieler().getxPos(), getSpieler().getyPos()).getTyp() == SpielfeldElement.AUSGANG) {

            LevelNachricht msg = new LevelNachricht(getClientID(), getSpieler().getAktuellesLevel().getLevelnr());
            sendeNachricht(msg);
        }

    }


    /**
     * Methode zum Senden von Chatnachrichten, die von der GUI an den Server geleitet werden
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void sendeChat(String text) {

        ChatNachricht msg = new ChatNachricht(getClientID(), text);
        sendeNachricht(msg);

    }


    /**
     * Prueft, ob die aktuelle Spielerposition legitim ist. Auf Felder mit Rand, Wand, Monster oder ausserhalb der Karte
     * kann sich der Spieler nicht bewegen
     *
     * @return true, falls der Spieler auf dem aktuellen Feld stehen darf
     * false, sonst
     * @author Tiebing, Alexander, 6018254
     */
    public boolean Konsistenzcheck(Spieler spieler, Level level, int xPos, int yPos) {
        if (xPos < 0 || xPos >= level.getKarten().get(spieler.getAktuelleKarte()).getKarte().length ||
                yPos < 0 || yPos >= level.getKarten().get(spieler.getAktuelleKarte()).getKarte().length ||
                level.getKarten().get(spieler.getAktuelleKarte()).getEintrag(xPos, yPos).nichtBegehbar()) {

            return false;
        }
        for (int i = 0; i < level.getKarten().get(spieler.getAktuelleKarte()).getMonsterListe().size(); i++) {
            if (level.getKarten().get(spieler.getAktuelleKarte()).getMonsterListe().get(i).getxPos() == xPos &&
                    level.getKarten().get(spieler.getAktuelleKarte()).getMonsterListe().get(i).getyPos() == yPos) {

                return false;
            }
        }
        return true;
    }


    /**
     * Bewegungsmethoden: Pruefen, ob das Feld oben/unten/rechts/links relativ zum Spieler begehbar ist und wenn ja,
     * bewegt er sich dahin und gibt die neue Position an den Server weiter
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void laufeHoch() {

        if (Konsistenzcheck(getSpieler(), getSpieler().getAktuellesLevel(), getSpieler().getxPos(), getSpieler().getyPos() - 1)) {

            getSpieler().setyPos(getSpieler().getyPos() - 1);

            LaufNachricht msg = new LaufNachricht(getClientID(), getSpieler().getxPos(), getSpieler().getyPos() - 1);
            sendeNachricht(msg);
        }
    }

    public void laufeRunter() {

        if (Konsistenzcheck(getSpieler(), getSpieler().getAktuellesLevel(), getSpieler().getxPos(), getSpieler().getyPos() + 1)) {

            getSpieler().setyPos(getSpieler().getyPos() + 1);

            LaufNachricht msg = new LaufNachricht(getClientID(), getSpieler().getxPos(), getSpieler().getyPos() + 1);
            sendeNachricht(msg);
        }
    }

    public void laufeLinks() {

        if (Konsistenzcheck(getSpieler(), getSpieler().getAktuellesLevel(), getSpieler().getxPos() - 1, getSpieler().getyPos())) {

            getSpieler().setxPos(getSpieler().getxPos() - 1);

            LaufNachricht msg = new LaufNachricht(getClientID(), getSpieler().getxPos() - 1, getSpieler().getyPos());
            sendeNachricht(msg);
        }
    }

    public void laufeRechts() {

        if (Konsistenzcheck(getSpieler(), getSpieler().getAktuellesLevel(), getSpieler().getxPos() + 1, getSpieler().getyPos())) {

            getSpieler().setxPos(getSpieler().getxPos() + 1);

            LaufNachricht msg = new LaufNachricht(getClientID(), getSpieler().getxPos() + 1, getSpieler().getyPos());
            sendeNachricht(msg);
        }
    }


    /**
     * Wenn die Taste zum Sammeln von Items gedrueckt wird (z.B LEERTASTE), prueft diese Methode,
     * ob der Spieler auf einem Item steht, und wenn ja, auf welchem Item er steht
     * Dem Server wird gesagt, was aufgenommen wurde, falls etwas aufgenommen wurde
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void sammleItem() {
        for (int i = 0; i < getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getTrankListe().size(); i++) {
            if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getTrankListe().get(i).getXpos()
                    == getSpieler().getxPos() && getSpieler().getyPos()
                    == getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getTrankListe().get(i).getYpos()) {

                setHeilTrank(getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getTrankListe().get(i));
                getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).loescheTrank(i);
                getSpieler().fuegeItemHinzu(getHeilTrank());

                ItemNachricht msg1 = new ItemNachricht(getClientID(), getSpieler().getInventar(), getSpieler().getAktuellesLevel());
                sendeNachricht(msg1);
            }
        }
    }

    /**
     * Platziert einen Trank auf der Karte auf der Position des Spielers. Die Trankliste der zugehoerigen Karte wird
     * aktualisiert und das Inventar sowie das Level werden dem Server geschickt
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void droppeTrank() {
        getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte())
                .addTrank((Trank) getSpieler().droppeItem(getHeilTrank()));

        ItemNachricht msg = new ItemNachricht(getClientID(), getSpieler().getInventar(), getSpieler().getAktuellesLevel());
        sendeNachricht(msg);

    }

    /**
     * Wenn der Spieler  einen Schluessel hat und diesen auf einer geschlossenen Tuer nutzt, dann oeffnet sich diese
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void oeffneTuer() {
        getSpieler().oeffneTuer();

        ItemNachricht msg = new ItemNachricht(getClientID(), getSpieler().isHatSchluessel(), getSpieler().getAktuellesLevel());
        sendeNachricht(msg);

        raumWechsel();
    }

    /**
     * Wenn der Spieler durch eine der beiden Tueren in einen neuen Raum gelangt, dann werden die neue Position des
     * Spielers, sowie die neue und alte Karte dem Server gegeben
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void raumWechsel() {
        int temp = getSpieler().getAktuelleKarte();  // aktuelle Karte wird zwischengespeichert
        getSpieler().raumWechsel();
        setRaumWechselTaste(true);

        SpielerNachricht msg = new SpielerNachricht(getClientID(), getSpieler().getxPos(), getSpieler().getyPos(),
                temp, getSpieler().getAktuelleKarte());

        sendeNachricht(msg);
    }


    /**
     * Wenn der Spieler mind. einen Heiltrank hat und diesen nutzt; dann bekommt er 25 prozent Leben wieder
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void nutzeHeilrank() {
        getSpieler().verwendeTrank();

        SpielerNachricht msg = new SpielerNachricht(getClientID(), getSpieler().getInventar(), getSpieler().getLeben());
        sendeNachricht(msg);
    }


    /**
     * Wenn der Spieler die Angriffstaste drueckt, geht man die Monsterliste durch und es wird geprueft, ob ein Monster
     * in der Naehe ist, und wenn ja, dann wird die monsterID des angegriffenen Monsters dem Server geschickt
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void angriff() {
        for (int i = 0; i < getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().size(); i++) {
            // Pruefe, ob Monster oben bzw. unten vom Spieler ist
            if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getxPos()
                    == getSpieler().getxPos()) {

                if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getyPos()
                        == getSpieler().getyPos() + 1 || getSpieler().getyPos() - 1
                        == getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getyPos()) {

                    KampfNachricht msg1 = new KampfNachricht(getClientID(), getSpieler().getAktuellesLevel().getKarten()
                            .get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getMonsterID(), true);

                    sendeNachricht(msg1);
                }
            }
            // Pruefe, ob Monster links bzw. rechts vom Spieler ist
            if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getyPos()
                    == getSpieler().getyPos()) {

                if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getxPos()
                        == getSpieler().getxPos() + 1 || getSpieler().getxPos() - 1
                        == getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getxPos()) {

                    KampfNachricht msg2 = new KampfNachricht(getClientID(), getSpieler().getAktuellesLevel().getKarten()
                            .get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getMonsterID(), true);

                    sendeNachricht(msg2);
                }
            }
        }

    }

    /**
     * Nachdem ein Konto neu erstellt wurde, werden mit dieser Methode die ersten 10 Skillpunkte verteilt
     *
     * @author Tiebing, Alexander, 6018254
     */
    public boolean neuSkillVerteilung(int staerke, int intelligenz, int geschicklichkeit) {
        if (staerke + intelligenz + geschicklichkeit == 10) {
            getSkills().add(staerke);
            getSkills().add(geschicklichkeit);
            getSkills().add(intelligenz);

            LoginNachricht log = new LoginNachricht(getSessionID(), getBenutzername(), getPasswort(), true, getSkills());

            sendeNachricht(log);

            return true;
        }

        return false;
    }

    /**
     * Ruft die 3 Methoden fuer die Skillpunktverteilung auf. Erst wenn alle neu dazugewonnenen SkillPunkte verteilt
     * wurden, wird dem Server eine SpielerNachricht geschickt.
     *
     * @author Tiebing, Alexander, 6018254
     */
    public boolean skillPktVerteilung(int staerke, int intelligenz, int geschicklichkeit) {
        for (int i = 1; i <= staerke; i++) addStaerke();
        for (int i = 1; i <= intelligenz; i++) addIntelligenz();
        for (int i = 1; i <= geschicklichkeit; i++) addGeschicklichkeit();

        if (getSpieler().getSkillPunkte() == 0) {

            SpielerNachricht msg = new SpielerNachricht(getClientID(), getSpieler().getStaerke(),
                    getSpieler().getGeschicklichkeit(), getSpieler().getIntelligenz());

            sendeNachricht(msg);

            return true;
        }

        return false;
    }

    /**
     * Die Methode ist fuer die Vertelung der Skillpunkte auf Staerke zustaendig
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void addStaerke() {
        if (getSpieler().getSkillPunkte() > 0) {

            getSpieler().setStaerke(getSpieler().getStaerke() + 1);
            getSpieler().setSkillPunkte(getSpieler().getSkillPunkte() - 1);

        }
    }

    /**
     * Die Methode ist fuer die Vertelung der Skillpunkte auf Intelligenz zustaendig
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void addIntelligenz() {
        if (getSpieler().getSkillPunkte() > 0) {

            getSpieler().setIntelligenz(getSpieler().getIntelligenz() + 1);
            getSpieler().setSkillPunkte(getSpieler().getSkillPunkte() - 1);

        }
    }

    /**
     * Die Methode ist fuer die Vertelung der Skillpunkte auf Geschicklichkeit zustaendig
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void addGeschicklichkeit() {
        if (getSpieler().getSkillPunkte() > 0) {

            getSpieler().setGeschicklichkeit(getSpieler().getGeschicklichkeit() + 1);
            getSpieler().setSkillPunkte(getSpieler().getSkillPunkte() - 1);

        }
    }


    /**
     * Entscheidet, wie die Nachrichten, die vom Server eingehen, bearbeitet werden.
     * Prueft die Gegebenheit der Nachricht und verteilt sie dementsprechend
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void handleNachricht() {

        while (!getEmpfangsListe().isEmpty()) {
            Object msg = getEmpfangsListe().poll();

            if (msg.getClass() == LoginNachricht.class) {
                handleLoginNachricht((LoginNachricht) msg);
            } else if (msg.getClass() == LevelNachricht.class) {
                handleLevelNachricht((LevelNachricht) msg);
            } else if (msg.getClass() == ChatNachricht.class) {
                handleChatNachricht((ChatNachricht) msg);
            } else if (msg.getClass() == ItemNachricht.class) {
                handleItemNachricht((ItemNachricht) msg);
            } else if (msg.getClass() == CheatNachricht.class) {
                handleCheatNachricht((CheatNachricht) msg);
            } else if (msg.getClass() == HighscoreNachricht.class) {
                handleHighscoreNachricht((HighscoreNachricht) msg);
            } else if (msg.getClass() == KampfNachricht.class) {
                handleKampfNachricht((KampfNachricht) msg);
            } else if (msg.getClass() == UpdateNachricht.class) {
                handleUpdateNachricht((UpdateNachricht) msg);
            } else if (msg.getClass() == MitspielerNachricht.class) {
                hanldeMitspielerNachricht((MitspielerNachricht) msg);
            }
        }
    }


    /**
     * Wird aufgerufen, wenn der Client eine LoginNachricht mit den Daten vom Server zurueckbekommt. Dies ist der Fall,
     * wenn sich ein Spieler einlggt oder ein neues Konto erstellt hat. Es wird geprueft, um welchen Fall es sich
     * genau handelt, und ob der Login eberhaupt erfolgreich war.
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void handleLoginNachricht(LoginNachricht msg) {
        if (msg.isPasswortFalsch()) setLoginFall(1);

        if (msg.isLoginErfolgreich()) {

            setLoginFall(2);

            setEingeloggt(true);
            setSpieler(msg.getSpieler());
            setClientID(msg.getClientID());

        }

        if (msg.isNeuesKonto()) {

            setLoginFall(3);

            setBenutzername(msg.getBenutzerName());
            setPasswort(msg.getPasswort());
        }
    }


    /**
     * Der Server schickt den Spieler mit dem neuen Level an den Client. Dieser passt dann die aktuelle Karte und Position
     * an, wonach diese Aenderungen wiederum dem Server mitgeteilt werden, zusammen mit der alten Karte
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void handleLevelNachricht(LevelNachricht msg) {

        int temp = getSpieler().getAktuelleKarte();  // aktuelle Karte wird zwischengespeichert
        setSpieler(msg.getSpieler());
        getSpieler().lokalisiereEingang(getSpieler().getAktuellesLevel());  // Karte/Position wird aktualisiert

        SpielerNachricht sp = new SpielerNachricht(getClientID(), getSpieler().getxPos(), getSpieler().getyPos(), temp,
                getSpieler().getAktuelleKarte());

        sendeNachricht(sp);

    }


    /**
     * Fuer vom Server einkommende Chatnachrichten, die an die GUI gegeben werden
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void handleChatNachricht(ChatNachricht msg) {
        addChatListe(msg.getText());
    }


    /**
     * Bearbeitet Nachrichten des Servers bezogen auf Items, nachdem sie zuvor aufgesammelt/gedroppt wurden
     * Updatet das Level und die dazugehoerigen Schluessel und Tranklisten
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void handleItemNachricht(ItemNachricht msg) {
        getSpieler().setAktuellesLevel(msg.getLevel());
    }


    /**
     * Wenn der Spieler den Schluessel-Cheat eingibt, erhaelt er einen Schluessel
     * Wenn der Spieler den Lebens-Cheat eingibt, erhaelt er volles Leben
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void handleCheatNachricht(CheatNachricht msg) {
        if (msg.isSchluessel()) getSpieler().setHatSchluessel(true);

        else getSpieler().setLeben(msg.getLeben());
    }


    /**
     * Der Server schickt dem Client die aktualisierte Highscoreliste
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void handleHighscoreNachricht(HighscoreNachricht msg) {
        setHighscore(msg.getHighscore());
    }


    /**
     * Geht die Monsterliste durch und schaut, welches Monster vom Spieler angegriffen wurde. Das Leben dieses Monsters
     * wird dann angepasst. Ausserdem erhaelt man hier den Schaden, den das Monster dem Spieler zugeteilt hat und die
     * Erfarungspunkte, die der Spieler erhaelt, nachdem er das Monster besiegt hat. Gegebenenfalls wird das Monster
     * aus der Monsterliste entfernt, der Spieler erhaelt einen Schluessel oder ein Item wird gedroppt.
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void handleKampfNachricht(KampfNachricht msg) {
        if (msg.getSchaden() != -1) getSpieler().changeLeben(-msg.getSchaden());

        if (msg.getMonsterLeben() != -1) {

            for (int i = 0; i < getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().size(); i++) {
                if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe()
                        .get(i).getMonsterID() == msg.getMonsterID()) {

                    getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe()
                            .get(i).setLeben(msg.getMonsterLeben());
                }
            }
        }


        if (msg.getErfahrung() != -1) {

            if (msg.isSchluessel()) getSpieler().setHatSchluessel(true);

            getSpieler().plusErfahrung(msg.getErfahrung());

            /*if(msg.getItem().getClass() == Trank.class){
                getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).addTrank((Trank) msg.getItem());
            }*/
            getSpieler().fuegeItemHinzu(msg.getItem());

            for (int i = 0; i < getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().size(); i++) {
                if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getMonsterID()
                        == msg.getMonsterID()) {

                    getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).loescheMonster(i);
                }
            }
        }
    }


    /**
     * Bearbeitet Nachrichten bezogen auf die Monsterbewegungen
     * Server schickt alle Monster im Level des Spielers neu. Die Monster im selben Raum wie der Spieler werden vom Spieler
     * uebernommen
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void handleUpdateNachricht(UpdateNachricht msg) {
        boolean temp = false;

        if (!getMitSpieler().isEmpty()) {

            if (getSpieler().getAktuelleKarte() == msg.getAktuelleKarte()) {


                if(getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().size()
                        == msg.getMonsterListe().size()){

                    for (int i = 0; i < msg.getMonsterListe().size(); i++) {

                        getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe()
                                .get(i).setxPos(msg.getMonsterListe().get(i)[1]);
                        getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe()
                                .get(i).setyPos(msg.getMonsterListe().get(i)[2]);
                        getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe()
                                .get(i).setMonsterID(msg.getMonsterListe().get(i)[0]);
                    }
                }
                while(getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().size()
                        != msg.getMonsterListe().size()){

                    getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe()
                            .removeLast();
                }

            }
        }
        else{
            if(getSpieler().getAktuelleKarte() == msg.getAktuelleKarte()){
                for(int i = 0;i < msg.getMonsterListe().size();i++) {
                    for (int j = 0; j < getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().size(); j++) {
                        if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().get(j).getMonsterID()
                                == msg.getMonsterListe().get(j)[0]) {

                            getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe()
                                    .get(j).setxPos(msg.getMonsterListe().get(i)[1]);
                            getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe()
                                    .get(j).setyPos(msg.getMonsterListe().get(i)[2]);
                        }
                    }
                }
                for (int i = 0; i < getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().size(); i++) {
                    for (int j = 0; j < msg.getMonsterListe().size(); j++) {
                        if (getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).getMonsterListe().get(i).getMonsterID()
                                == msg.getMonsterListe().get(j)[0]) {

                            temp = true;
                        }
                    }
                    if (!temp) {
                        getSpieler().getAktuellesLevel().getKarten().get(getSpieler().getAktuelleKarte()).loescheMonster(i);
                    }

                    temp = false;
                }
            }
        }
    }


    /**
     * Hier werden die Nachrichten bzgl. der Positionen der Mitspieler behandelt. Nur die Spieler im selben Raum
     * werden abgespeichert.
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void hanldeMitspielerNachricht(MitspielerNachricht msg) {
        if (!(msg.getIstimRaum() == null)) {
            if (!getMitSpieler().contains(msg.getIstimRaum())) {
                addMitSpieler(msg.getIstimRaum());
            } else {
                getMitSpieler().get(sucheSpieler(msg.getIstimRaum().getClientID())).setxPos(msg.getIstimRaum().getxPos());
                getMitSpieler().get(sucheSpieler(msg.getIstimRaum().getClientID())).setyPos(msg.getIstimRaum().getyPos());
            }
        }
        if (!(msg.getNichtimRaum() == null)) {
            if (getMitSpieler().contains(msg.getNichtimRaum())) {
                loescheMitSpieler(msg.getNichtimRaum());
            }
        }
    }


    /**
     * Sucht einen Spieler in der Liste der Mitspieler und gibt seinen Index in der Liste wieder
     *
     * @author Tiebing, Alexander, 6018254
     */
    public int sucheSpieler(int id) {
        if (getMitSpieler().isEmpty()) {
            return -1;
        }
        for (int i = 0; i < getMitSpieler().size(); i++)
            if (getMitSpieler().get(i) != null && getMitSpieler().get(i).getClientID() == id) {
                return i;
            }
        return -1;
    }

    /**
     * Gibt das ertse Element der sendeListe wieder und entfernt es aus der Liste
     *
     * @author Tiebing, Alexander, 6018254
     */
    public Object getFirstSendeListe() {
        return sendeListe.pollFirst();
    }

    /**
     * Fuegt Chatnachrichten zur Chatliste hinzu
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void addChatListe(String text) {
        chatListe.add(text);
        setChatAngekommen(true);
    }

    /**
     * Fuegt Spieler zur Mitspielerliste hinzu
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void addMitSpieler(Spieler spieler) {
        mitSpieler.add(spieler);
    }

    /**
     * Entfernt einen Spieler aus der Mitspielerliste
     *
     * @author Tiebing, Alexander, 6018254
     */
    public void loescheMitSpieler(Spieler spieler) {
        mitSpieler.remove(spieler);
    }

}